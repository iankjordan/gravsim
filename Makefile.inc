#    Grav-Sim : http://www.grav-sim.com
#    Copyright (C) 2009 Mark Ridler

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#    UNIX - gcc
#    Copy this file to Makefile.inc to use
# --------------------------------------------------------

CC=g++
#CCFLAGS=-c -ggdb -DPGPLOT -DOMP -fopenmp
CCFLAGS=-c -O3 -DPGPLOT -DOMP -fopenmp
CCOUT=-o 
DEF=-D
INC=-I
INCLUDES=
AR=ar
ARFLAGS=cr
AROUT=
LD=g++
LDFLAGS=
LDOUT=-o 
LIBPATH=
STDLIBS=-lgomp -lpthread -lglut -lGL -lGLU -lstdc++ -lqd -lm -lcpgplot
EXE=
