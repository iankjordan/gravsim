/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "acs.h"
#include "cmdline.h"

using namespace GravSim;

ofstream orbit_file;

void Body::draw_line() const
{
#	pragma omp critical
	orbit_file << id << comma << mass << comma << point_1.time << comma << point_1.pos << comma << point_0.time << comma << point_0.pos << "\n";
}

void commit_hook(const Body * body)
{
	body->draw_line();
}

void error_handler(const char * error, const char * context)
{
	cerr << error << ": " << context << "\n";
	exit(1);
}

void simulate(gengetopt_args_info & args)
{
	serial = (strcmp(args.cpu_arg, "serial") == 0) ? true : false;
	logging = (strcmp(args.logging_arg, "on") == 0) ? true : false;
	detailed_logging = (strcmp(args.detailed_logging_arg, "on") == 0) ? true : false;

	max_batch_size = args.max_batch_size_arg;
	min_integrator_parallel_size = args.min_integrator_parallel_size_arg;
	min_brute_force_parallel_size = args.min_brute_force_parallel_size_arg;
	min_barnes_hut_step_size = args.min_barnes_hut_step_size_arg;
	min_stadel_step_size = args.min_stadel_step_size_arg;
	min_ridler_step_size = args.min_ridler_step_size_arg;
	min_dehnen_step_size = args.min_dehnen_step_size_arg;
	min_dehnen_stadel_step_size = args.min_dehnen_stadel_step_size_arg;
	min_dehnen_ridler_step_size = args.min_dehnen_ridler_step_size_arg;
	min_body_sort_step_size = args.min_body_sort_step_size_arg;

	Integrator * integrator = Integrator::create(args.integrator_arg);

	Interactor * interactor = Interactor::create(args.accelerator_arg, args.interactor_arg);

	Divider * divider = Divider::create(args.accelerator_arg, args.divider_arg,
		interactor, args.theta_arg, args.branch_factor_arg);

	bool constant_timesteps = (strcmp(args.timestep_arg, "constant") == 0);
	bool shared_timesteps = (strcmp(args.timestep_arg, "shared") == 0);
	bool block_timesteps = (strcmp(args.block_timesteps_arg, "yes") == 0);
	bool analyser = (strcmp(args.analyser_arg, "on") == 0);

	Simulator * simulator = new Simulator(integrator, divider,
		constant_timesteps, shared_timesteps, block_timesteps, args.level_arg, args.softening_arg, analyser, args.hill_sphere_factor_arg);

	simulator->load(args.file_arg);

	if(strcmp(args.orbit_arg, "none") != 0)
	{
		Body::commit_hook = commit_hook;
		orbit_file.open(args.orbit_arg);
		if(!orbit_file.is_open())
		{
			Body::error_handler("Cannot open file", args.orbit_arg);
		}
		else
		{
			orbit_file << simulator->get_bodies_size() << ",Particle,Time1,Pos1(x),Pos1(y),Pos1(z),Time2,Pos2(x),Pos2(y),Pos2(z)\n";
		}
	}

	simulator->run(args.sim_time_arg, args.sim_time_arg / args.log_points_arg);

	simulator->save(args.result_arg);

	delete simulator;
	if(interactor != 0) delete interactor;
	if(divider != 0) delete divider;
	delete integrator;
}

void generate(gengetopt_args_info & args)
{
	Model * model = Model::create(args.generate_arg, args.bodies_arg, args.mass_sign_arg);
	model->rotate(args.rotation_rate_arg);
	model->save(args.result_arg);
}

int main(int argc, char** argv)
{
	PROFILE_FUNC();
#ifdef FINESIM
	unsigned int old_cw;
	fpu_fix_start(&old_cw);
#ifdef MPREAL
	mp::mp_init(REAL_DIGITS);
#endif
#endif

	Body::error_handler = error_handler;

	gengetopt_args_info args;
	cmdline_parser(argc, argv, &args);

	if(strcmp(args.generate_arg, "simulation") == 0)
	{
		simulate(args);
	}
	else
	{
		generate(args);
	}

	PROFILER_UPDATE();
	PROFILER_OUTPUT();

#ifdef FINESIM
	fpu_fix_end(&old_cw);
#ifdef MPREAL
	mp::mp_finalize();
#endif
#endif
	return 0;
}
