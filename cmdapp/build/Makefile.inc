ACSLIB=../../../acslib
DEFINES=$(DEF)$(SIMULATOR)
ACSINCLUDE=$(INC)$(ACSLIB)
LIBS=$(ACSLIBTARGET)/acslib.a

main.o : 	../../main.cpp $(ACSLIB)/*.h	    
cmdline.o :	../../cmdline.c 	    

%.o : ../../%.cpp
	$(CC) $(CCFLAGS) $(DEFINES) $(INCLUDES) $(ACSINCLUDE) $(CCOUT)$@ $< 
%.o : ../../%.c
	$(CC) $(CCFLAGS) $(DEFINES) $(INCLUDES) $(ACSINCLUDE) $(CCOUT)$@ $< 

$(TARGET) :	main.o cmdline.o $(LIBS)
	$(LD) *.o $(LDFLAGS) $(LDOUT)$@ $(LIBPATH) $(LIBS) $(STDLIBS)
	-cp $(TARGET) ../../../$(TARGET)

clean:
	-rm -f *.o
	-rm -f *.exe
