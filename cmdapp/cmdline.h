/** @file cmdline.h
 *  @brief The header file for the command line option parser
 *  generated by GNU Gengetopt version 2.22.6
 *  http://www.gnu.org/software/gengetopt.
 *  DO NOT modify this file, since it can be overwritten
 *  @author GNU Gengetopt by Lorenzo Bettini */

#ifndef CMDLINE_H
#define CMDLINE_H

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h> /* for FILE */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef CMDLINE_PARSER_PACKAGE
/** @brief the program name (used for printing errors) */
#define CMDLINE_PARSER_PACKAGE "Grav-Sim"
#endif

#ifndef CMDLINE_PARSER_PACKAGE_NAME
/** @brief the complete program name (used for help and version) */
#define CMDLINE_PARSER_PACKAGE_NAME "Grav-Sim"
#endif

#ifndef CMDLINE_PARSER_VERSION
/** @brief the program version */
#define CMDLINE_PARSER_VERSION "1.06"
#endif

/** @brief Where the command line options are stored */
struct gengetopt_args_info
{
  const char *help_help; /**< @brief Print help and exit help description.  */
  const char *version_help; /**< @brief Print version and exit help description.  */
  char * generate_arg;	/**< @brief Model to generate (default='simulation').  */
  char * generate_orig;	/**< @brief Model to generate original value given at command line.  */
  const char *generate_help; /**< @brief Model to generate help description.  */
  int bodies_arg;	/**< @brief Number of bodies to generate (default='1000').  */
  char * bodies_orig;	/**< @brief Number of bodies to generate original value given at command line.  */
  const char *bodies_help; /**< @brief Number of bodies to generate help description.  */
  char * mass_sign_arg;	/**< @brief Postive or negative mass (default='positive').  */
  char * mass_sign_orig;	/**< @brief Postive or negative mass original value given at command line.  */
  const char *mass_sign_help; /**< @brief Postive or negative mass help description.  */
  float rotation_rate_arg;	/**< @brief Rotation rate about z-axis (default='0.0').  */
  char * rotation_rate_orig;	/**< @brief Rotation rate about z-axis original value given at command line.  */
  const char *rotation_rate_help; /**< @brief Rotation rate about z-axis help description.  */
  char * file_arg;	/**< @brief Model input filename (or stdin) (default='model.csv').  */
  char * file_orig;	/**< @brief Model input filename (or stdin) original value given at command line.  */
  const char *file_help; /**< @brief Model input filename (or stdin) help description.  */
  char * result_arg;	/**< @brief Model output filename (or stdout) (default='result.csv').  */
  char * result_orig;	/**< @brief Model output filename (or stdout) original value given at command line.  */
  const char *result_help; /**< @brief Model output filename (or stdout) help description.  */
  char * orbit_arg;	/**< @brief Orbit output filename (or none) (default='none').  */
  char * orbit_orig;	/**< @brief Orbit output filename (or none) original value given at command line.  */
  const char *orbit_help; /**< @brief Orbit output filename (or none) help description.  */
  char * integrator_arg;	/**< @brief Choice of integrator (default='hermite-6').  */
  char * integrator_orig;	/**< @brief Choice of integrator original value given at command line.  */
  const char *integrator_help; /**< @brief Choice of integrator help description.  */
  char * interactor_arg;	/**< @brief Choice of interactor (default='default').  */
  char * interactor_orig;	/**< @brief Choice of interactor original value given at command line.  */
  const char *interactor_help; /**< @brief Choice of interactor help description.  */
  char * divider_arg;	/**< @brief Choice of space divider (default='default').  */
  char * divider_orig;	/**< @brief Choice of space divider original value given at command line.  */
  const char *divider_help; /**< @brief Choice of space divider help description.  */
  char * accelerator_arg;	/**< @brief Choice of accelerator (default='ridler').  */
  char * accelerator_orig;	/**< @brief Choice of accelerator original value given at command line.  */
  const char *accelerator_help; /**< @brief Choice of accelerator help description.  */
  char * timestep_arg;	/**< @brief Choice of time-step type (default='individual').  */
  char * timestep_orig;	/**< @brief Choice of time-step type original value given at command line.  */
  const char *timestep_help; /**< @brief Choice of time-step type help description.  */
  int level_arg;	/**< @brief Determines the level of simulation (default='1').  */
  char * level_orig;	/**< @brief Determines the level of simulation original value given at command line.  */
  const char *level_help; /**< @brief Determines the level of simulation help description.  */
  float sim_time_arg;	/**< @brief The time at which the simulation ends (default='1.0').  */
  char * sim_time_orig;	/**< @brief The time at which the simulation ends original value given at command line.  */
  const char *sim_time_help; /**< @brief The time at which the simulation ends help description.  */
  int log_points_arg;	/**< @brief The number of log points per sim-time (default='16').  */
  char * log_points_orig;	/**< @brief The number of log points per sim-time original value given at command line.  */
  const char *log_points_help; /**< @brief The number of log points per sim-time help description.  */
  float theta_arg;	/**< @brief Minimum separation before use of the multipole terms. (default='0.75').  */
  char * theta_orig;	/**< @brief Minimum separation before use of the multipole terms. original value given at command line.  */
  const char *theta_help; /**< @brief Minimum separation before use of the multipole terms. help description.  */
  float softening_arg;	/**< @brief Determines the gravity softening parameter (default='0.0').  */
  char * softening_orig;	/**< @brief Determines the gravity softening parameter original value given at command line.  */
  const char *softening_help; /**< @brief Determines the gravity softening parameter help description.  */
  char * block_timesteps_arg;	/**< @brief Whether to restrict times to specific values (default='yes').  */
  char * block_timesteps_orig;	/**< @brief Whether to restrict times to specific values original value given at command line.  */
  const char *block_timesteps_help; /**< @brief Whether to restrict times to specific values help description.  */
  char * cpu_arg;	/**< @brief Serial or parallel pocessing on the CPU via OpenMP (default='parallel').  */
  char * cpu_orig;	/**< @brief Serial or parallel pocessing on the CPU via OpenMP original value given at command line.  */
  const char *cpu_help; /**< @brief Serial or parallel pocessing on the CPU via OpenMP help description.  */
  char * analyser_arg;	/**< @brief Kepler-based analytic regularisation (default='on').  */
  char * analyser_orig;	/**< @brief Kepler-based analytic regularisation original value given at command line.  */
  const char *analyser_help; /**< @brief Kepler-based analytic regularisation help description.  */
  char * logging_arg;	/**< @brief Normal logging output (default='on').  */
  char * logging_orig;	/**< @brief Normal logging output original value given at command line.  */
  const char *logging_help; /**< @brief Normal logging output help description.  */
  char * detailed_logging_arg;	/**< @brief Detailed logging output (default='off').  */
  char * detailed_logging_orig;	/**< @brief Detailed logging output original value given at command line.  */
  const char *detailed_logging_help; /**< @brief Detailed logging output help description.  */
  float hill_sphere_factor_arg;	/**< @brief Determines the analytic range within the hill sphere (default='0.25').  */
  char * hill_sphere_factor_orig;	/**< @brief Determines the analytic range within the hill sphere original value given at command line.  */
  const char *hill_sphere_factor_help; /**< @brief Determines the analytic range within the hill sphere help description.  */
  int branch_factor_arg;	/**< @brief Number of children per node with the ridler accelerator. (default='5').  */
  char * branch_factor_orig;	/**< @brief Number of children per node with the ridler accelerator. original value given at command line.  */
  const char *branch_factor_help; /**< @brief Number of children per node with the ridler accelerator. help description.  */
  int max_batch_size_arg;	/**< @brief Maximum number of bodies processed in each batch. (default='2000').  */
  char * max_batch_size_orig;	/**< @brief Maximum number of bodies processed in each batch. original value given at command line.  */
  const char *max_batch_size_help; /**< @brief Maximum number of bodies processed in each batch. help description.  */
  int min_integrator_parallel_size_arg;	/**< @brief Minimum number of bodies for integrator parallel processing. (default='64').  */
  char * min_integrator_parallel_size_orig;	/**< @brief Minimum number of bodies for integrator parallel processing. original value given at command line.  */
  const char *min_integrator_parallel_size_help; /**< @brief Minimum number of bodies for integrator parallel processing. help description.  */
  int min_brute_force_parallel_size_arg;	/**< @brief Minimum number of bodies for brute-force parallel processing. (default='8').  */
  char * min_brute_force_parallel_size_orig;	/**< @brief Minimum number of bodies for brute-force parallel processing. original value given at command line.  */
  const char *min_brute_force_parallel_size_help; /**< @brief Minimum number of bodies for brute-force parallel processing. help description.  */
  int min_barnes_hut_step_size_arg;	/**< @brief Minimum number of bodies per step for barnes-hut accelerator. (default='16').  */
  char * min_barnes_hut_step_size_orig;	/**< @brief Minimum number of bodies per step for barnes-hut accelerator. original value given at command line.  */
  const char *min_barnes_hut_step_size_help; /**< @brief Minimum number of bodies per step for barnes-hut accelerator. help description.  */
  int min_stadel_step_size_arg;	/**< @brief Minimum number of bodies per step for stadel accelerator. (default='32').  */
  char * min_stadel_step_size_orig;	/**< @brief Minimum number of bodies per step for stadel accelerator. original value given at command line.  */
  const char *min_stadel_step_size_help; /**< @brief Minimum number of bodies per step for stadel accelerator. help description.  */
  int min_ridler_step_size_arg;	/**< @brief Minimum number of bodies per step for ridler accelerator. (default='8').  */
  char * min_ridler_step_size_orig;	/**< @brief Minimum number of bodies per step for ridler accelerator. original value given at command line.  */
  const char *min_ridler_step_size_help; /**< @brief Minimum number of bodies per step for ridler accelerator. help description.  */
  int min_dehnen_step_size_arg;	/**< @brief Minimum number of bodies per step for dehnen accelerator. (default='32').  */
  char * min_dehnen_step_size_orig;	/**< @brief Minimum number of bodies per step for dehnen accelerator. original value given at command line.  */
  const char *min_dehnen_step_size_help; /**< @brief Minimum number of bodies per step for dehnen accelerator. help description.  */
  int min_dehnen_stadel_step_size_arg;	/**< @brief Minimum number of bodies per step for dehnen-stadel accelerator. (default='64').  */
  char * min_dehnen_stadel_step_size_orig;	/**< @brief Minimum number of bodies per step for dehnen-stadel accelerator. original value given at command line.  */
  const char *min_dehnen_stadel_step_size_help; /**< @brief Minimum number of bodies per step for dehnen-stadel accelerator. help description.  */
  int min_dehnen_ridler_step_size_arg;	/**< @brief Minimum number of bodies per step for dehnen-ridler accelerator. (default='256').  */
  char * min_dehnen_ridler_step_size_orig;	/**< @brief Minimum number of bodies per step for dehnen-ridler accelerator. original value given at command line.  */
  const char *min_dehnen_ridler_step_size_help; /**< @brief Minimum number of bodies per step for dehnen-ridler accelerator. help description.  */
  int min_body_sort_step_size_arg;	/**< @brief Minimum number of bodies per step for body-sort rather than body-pointer-sort. (default='512').  */
  char * min_body_sort_step_size_orig;	/**< @brief Minimum number of bodies per step for body-sort rather than body-pointer-sort. original value given at command line.  */
  const char *min_body_sort_step_size_help; /**< @brief Minimum number of bodies per step for body-sort rather than body-pointer-sort. help description.  */
  
  unsigned int help_given ;	/**< @brief Whether help was given.  */
  unsigned int version_given ;	/**< @brief Whether version was given.  */
  unsigned int generate_given ;	/**< @brief Whether generate was given.  */
  unsigned int bodies_given ;	/**< @brief Whether bodies was given.  */
  unsigned int mass_sign_given ;	/**< @brief Whether mass-sign was given.  */
  unsigned int rotation_rate_given ;	/**< @brief Whether rotation-rate was given.  */
  unsigned int file_given ;	/**< @brief Whether file was given.  */
  unsigned int result_given ;	/**< @brief Whether result was given.  */
  unsigned int orbit_given ;	/**< @brief Whether orbit was given.  */
  unsigned int integrator_given ;	/**< @brief Whether integrator was given.  */
  unsigned int interactor_given ;	/**< @brief Whether interactor was given.  */
  unsigned int divider_given ;	/**< @brief Whether divider was given.  */
  unsigned int accelerator_given ;	/**< @brief Whether accelerator was given.  */
  unsigned int timestep_given ;	/**< @brief Whether timestep was given.  */
  unsigned int level_given ;	/**< @brief Whether level was given.  */
  unsigned int sim_time_given ;	/**< @brief Whether sim-time was given.  */
  unsigned int log_points_given ;	/**< @brief Whether log-points was given.  */
  unsigned int theta_given ;	/**< @brief Whether theta was given.  */
  unsigned int softening_given ;	/**< @brief Whether softening was given.  */
  unsigned int block_timesteps_given ;	/**< @brief Whether block-timesteps was given.  */
  unsigned int cpu_given ;	/**< @brief Whether cpu was given.  */
  unsigned int analyser_given ;	/**< @brief Whether analyser was given.  */
  unsigned int logging_given ;	/**< @brief Whether logging was given.  */
  unsigned int detailed_logging_given ;	/**< @brief Whether detailed-logging was given.  */
  unsigned int hill_sphere_factor_given ;	/**< @brief Whether hill-sphere-factor was given.  */
  unsigned int branch_factor_given ;	/**< @brief Whether branch-factor was given.  */
  unsigned int max_batch_size_given ;	/**< @brief Whether max-batch-size was given.  */
  unsigned int min_integrator_parallel_size_given ;	/**< @brief Whether min-integrator-parallel-size was given.  */
  unsigned int min_brute_force_parallel_size_given ;	/**< @brief Whether min-brute-force-parallel-size was given.  */
  unsigned int min_barnes_hut_step_size_given ;	/**< @brief Whether min-barnes-hut-step-size was given.  */
  unsigned int min_stadel_step_size_given ;	/**< @brief Whether min-stadel-step-size was given.  */
  unsigned int min_ridler_step_size_given ;	/**< @brief Whether min-ridler-step-size was given.  */
  unsigned int min_dehnen_step_size_given ;	/**< @brief Whether min-dehnen-step-size was given.  */
  unsigned int min_dehnen_stadel_step_size_given ;	/**< @brief Whether min-dehnen-stadel-step-size was given.  */
  unsigned int min_dehnen_ridler_step_size_given ;	/**< @brief Whether min-dehnen-ridler-step-size was given.  */
  unsigned int min_body_sort_step_size_given ;	/**< @brief Whether min-body-sort-step-size was given.  */

} ;

/** @brief The additional parameters to pass to parser functions */
struct cmdline_parser_params
{
  int override; /**< @brief whether to override possibly already present options (default 0) */
  int initialize; /**< @brief whether to initialize the option structure gengetopt_args_info (default 1) */
  int check_required; /**< @brief whether to check that all required options were provided (default 1) */
  int check_ambiguity; /**< @brief whether to check for options already specified in the option structure gengetopt_args_info (default 0) */
  int print_errors; /**< @brief whether getopt_long should print an error message for a bad option (default 1) */
} ;

/** @brief the purpose string of the program */
extern const char *gengetopt_args_info_purpose;
/** @brief the usage string of the program */
extern const char *gengetopt_args_info_usage;
/** @brief the description string of the program */
extern const char *gengetopt_args_info_description;
/** @brief all the lines making the help output */
extern const char *gengetopt_args_info_help[];

/**
 * The command line parser
 * @param argc the number of command line options
 * @param argv the command line options
 * @param args_info the structure where option information will be stored
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser (int argc, char **argv,
  struct gengetopt_args_info *args_info);

/**
 * The command line parser (version with additional parameters - deprecated)
 * @param argc the number of command line options
 * @param argv the command line options
 * @param args_info the structure where option information will be stored
 * @param override whether to override possibly already present options
 * @param initialize whether to initialize the option structure my_args_info
 * @param check_required whether to check that all required options were provided
 * @return 0 if everything went fine, NON 0 if an error took place
 * @deprecated use cmdline_parser_ext() instead
 */
int cmdline_parser2 (int argc, char **argv,
  struct gengetopt_args_info *args_info,
  int override, int initialize, int check_required);

/**
 * The command line parser (version with additional parameters)
 * @param argc the number of command line options
 * @param argv the command line options
 * @param args_info the structure where option information will be stored
 * @param params additional parameters for the parser
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser_ext (int argc, char **argv,
  struct gengetopt_args_info *args_info,
  struct cmdline_parser_params *params);

/**
 * Save the contents of the option struct into an already open FILE stream.
 * @param outfile the stream where to dump options
 * @param args_info the option struct to dump
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser_dump(FILE *outfile,
  struct gengetopt_args_info *args_info);

/**
 * Save the contents of the option struct into a (text) file.
 * This file can be read by the config file parser (if generated by gengetopt)
 * @param filename the file where to save
 * @param args_info the option struct to save
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser_file_save(const char *filename,
  struct gengetopt_args_info *args_info);

/**
 * Print the help
 */
void cmdline_parser_print_help(void);
/**
 * Print the version
 */
void cmdline_parser_print_version(void);

/**
 * Initializes all the fields a cmdline_parser_params structure 
 * to their default values
 * @param params the structure to initialize
 */
void cmdline_parser_params_init(struct cmdline_parser_params *params);

/**
 * Allocates dynamically a cmdline_parser_params structure and initializes
 * all its fields to their default values
 * @return the created and initialized cmdline_parser_params structure
 */
struct cmdline_parser_params *cmdline_parser_params_create(void);

/**
 * Initializes the passed gengetopt_args_info structure's fields
 * (also set default values for options that have a default)
 * @param args_info the structure to initialize
 */
void cmdline_parser_init (struct gengetopt_args_info *args_info);
/**
 * Deallocates the string fields of the gengetopt_args_info structure
 * (but does not deallocate the structure itself)
 * @param args_info the structure to deallocate
 */
void cmdline_parser_free (struct gengetopt_args_info *args_info);

/**
 * Checks that all the required options were specified
 * @param args_info the structure to check
 * @param prog_name the name of the program that will be used to print
 *   possible errors
 * @return
 */
int cmdline_parser_required (struct gengetopt_args_info *args_info,
  const char *prog_name);

extern const char *cmdline_parser_generate_values[];  /**< @brief Possible values for generate. */
extern const char *cmdline_parser_mass_sign_values[];  /**< @brief Possible values for mass-sign. */
extern const char *cmdline_parser_integrator_values[];  /**< @brief Possible values for integrator. */
extern const char *cmdline_parser_interactor_values[];  /**< @brief Possible values for interactor. */
extern const char *cmdline_parser_divider_values[];  /**< @brief Possible values for divider. */
extern const char *cmdline_parser_accelerator_values[];  /**< @brief Possible values for accelerator. */
extern const char *cmdline_parser_timestep_values[];  /**< @brief Possible values for timestep. */
extern const char *cmdline_parser_block_timesteps_values[];  /**< @brief Possible values for block-timesteps. */
extern const char *cmdline_parser_cpu_values[];  /**< @brief Possible values for cpu. */
extern const char *cmdline_parser_analyser_values[];  /**< @brief Possible values for analyser. */
extern const char *cmdline_parser_logging_values[];  /**< @brief Possible values for logging. */
extern const char *cmdline_parser_detailed_logging_values[];  /**< @brief Possible values for detailed-logging. */


#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* CMDLINE_H */
