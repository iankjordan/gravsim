all:	fastsim gravsim gravview

fastsim:
	$(MAKE) -C acslib  fastsim
	$(MAKE) -C cmdapp  fastsim

gravsim:
	$(MAKE) -C acslib  gravsim
	$(MAKE) -C cmdapp  gravsim

gravview: gravsim
	$(MAKE) -C glutapp gravview

finesim:
	$(MAKE) -C acslib  finesim
	$(MAKE) -C cmdapp  finesim

gravplot: gravsim
	$(MAKE) -C pgapp  gravplot
	
clean:
	$(MAKE) -C acslib  clean
	$(MAKE) -C cmdapp  clean
	$(MAKE) -C glutapp clean
	$(MAKE) -C pgapp   clean

touch:
	-touch Makefile*
	$(MAKE) -C acslib  touch
	$(MAKE) -C cmdapp  touch
	$(MAKE) -C glutapp touch
	$(MAKE) -C pgapp   touch


