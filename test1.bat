rm result1.txt

rem individual, direct

echo 1000, individual, direct, hilbert, >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d hilbert -t individual -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem echo 1000, individual, direct, morton,  >> result1.txt
rem timer > timer.txt
rem gravsim -f model1000.csv -d morton -t individual -n 1 > log.txt
rem timer /s > timer.txt
rem grep seconds timer.txt >> result1.txt

echo 1000, individual, direct, octree,  >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d octree -t individual -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

echo 1000, individual, direct, kdtree,  >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d kdtree -t individual -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem individual, pairwise

echo 1000, individual, pairwise, hilbert, >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d hilbert -t individual -c pairwise -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem echo 1000, individual, pairwise, morton,  >> result1.txt
rem timer > timer.txt
rem gravsim -f model1000.csv -d morton -t individual -c pairwise -n 1 > log.txt
rem timer /s > timer.txt
rem grep seconds timer.txt >> result1.txt

echo 1000, individual, pairwise, octree,  >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d octree -t individual -c pairwise -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

echo 1000, individual, pairwise, kdtree,  >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d kdtree -t individual -c pairwise -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem individual, null

echo 1000, individual, null,     null,    >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d null -t individual -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem shared, direct

echo 1000, shared, direct, hilbert,  >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d hilbert -t shared -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem echo 1000, shared, direct, morton,   >> result1.txt
rem timer > timer.txt
rem gravsim -f model1000.csv -d morton -t shared -n 1 > log.txt
rem timer /s > timer.txt
rem grep seconds timer.txt >> result1.txt

echo 1000, shared, direct, octree,   >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d octree -t shared -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

echo 1000, shared, direct, kdtree,   >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d kdtree -t shared -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem shared, pairwise

echo 1000, shared, pairwise, hilbert,  >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d hilbert -t shared -c pairwise -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem echo 1000, shared, pairwise, morton,   >> result1.txt
rem timer > timer.txt
rem gravsim -f model1000.csv -d morton -t shared -c pairwise -n 1 > log.txt
rem timer /s > timer.txt
rem grep seconds timer.txt >> result1.txt

echo 1000, shared, pairwise, octree,   >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d octree -t shared -c pairwise -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

echo 1000, shared, pairwise, kdtree,   >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d kdtree -t shared -c pairwise -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem shared, null

echo 1000, shared, null,     null,     >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d null -t shared -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem constant, direct

echo 1000, constant, direct, hilbert,  >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d hilbert -t constant -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem echo 1000, constant, direct, morton,   >> result1.txt
rem timer > timer.txt
rem gravsim -f model1000.csv -d morton -t constant -n 1 > log.txt
rem timer /s > timer.txt
rem grep seconds timer.txt >> result1.txt

echo 1000, constant, direct, octree,   >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d octree -t constant -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

echo 1000, constant, direct, kdtree,   >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d kdtree -t constant -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem constant, pairwise

echo 1000, constant, pairwise, hilbert,  >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d hilbert -t constant -c pairwise -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem echo 1000, constant, pairwise, morton,   >> result1.txt
rem timer > timer.txt
rem gravsim -f model1000.csv -d morton -t constant -c pairwise -n 1 > log.txt
rem timer /s > timer.txt
rem grep seconds timer.txt >> result1.txt

echo 1000, constant, pairwise, octree,   >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d octree -t constant -c pairwise -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

echo 1000, constant, pairwise, kdtree,   >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d kdtree -t constant -c pairwise -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

rem constant, null

echo 1000, constant, null,     null,     >> result1.txt
timer > timer.txt
gravsim -f model1000.csv -d null -t constant -n 1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result1.txt

