/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifdef __unix__
#include <time.h> 
#include <sys/time.h>   
#include <sys/resource.h> 
#else
#ifdef WIN32
#define NOMINMAX
#include <windows.h>
#endif
#endif
#include "acs.h"
#include "math.h"
#ifdef PGPLOT
#include "cpgplot.h"
#endif


using namespace GravSim;

Simulator * simulator;

static int orbit_bodies = 0;
static int orbit_count = 0;
static int orbit_mod = 0;
static int orbit_max = 0;
static int orbit_axis = 0;
static Real orbit_threshold = 0.0f;

void Body::draw_line() const
{
	if((orbit_count >= 0) &&
	   (((orbit_count / orbit_bodies) / orbit_mod) < orbit_max) && 
	   (((orbit_count / orbit_bodies) % orbit_mod) == 0))
	{
		float x[2];
		float y[2];
		x[0] = (float) to_double(point_0.pos[0]);
		y[0] = (float) to_double(point_0.pos[1]);
		x[1] = (float) to_double(point_1.pos[0]);
		y[1] = (float) to_double(point_1.pos[1]);
#ifdef PGPLOT
		cpgline(2, x, y);
#endif
	}
}

void commit_hook(const Body * body)
{
	body->draw_line();
	if((body->point_0.pos[orbit_axis] > orbit_threshold) && (body->point_1.pos[orbit_axis] <= orbit_threshold))
	{
		orbit_count++;
		body->draw_line();
	}
}

void orbit_graph(const char * integrator_arg, bool constant_timesteps, Real sim_time, int axis, Real threshold, int level)
{
	orbit_count = -orbit_bodies;
	orbit_axis = axis;
	orbit_threshold = threshold;
	double elapsed = 0.0;

#ifdef PGPLOT
	if (cpgopen("/gif") < 1) exit(1);
	cpgenv(-1.0f, 6.0f, -3.0f, 3.0f, 0, 0);
#endif

	Integrator * integrator = Integrator::create(integrator_arg);

	simulator = new Simulator(integrator, 0, constant_timesteps, false, false, level, 0.0f, false, 0.25f);

	simulator->load("model2.csv");

#ifdef __unix__
    struct timespec start, end; 
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start); 
#else
#ifdef WIN32
	LARGE_INTEGER frequency;
	LARGE_INTEGER start;
	LARGE_INTEGER end;

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&start);
#endif
#endif

	simulator->run(sim_time, sim_time);

#ifdef __unix__
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
	long seconds = end.tv_sec - start.tv_sec; 
	long ns = end.tv_nsec - start.tv_nsec; 

	if (start.tv_nsec > end.tv_nsec) 
	{
		--seconds; 
		ns += 1000000000;
	}
	elapsed = (double)seconds + (double)ns/(double)1000000000;
#else
#ifdef WIN32
	QueryPerformanceCounter(&end);
	elapsed = (double)(end.QuadPart - start.QuadPart) / frequency.QuadPart;
#endif
#endif

	char string[2000];
	const char* variable_arg = (constant_timesteps ? "constant" : "variable");
	sprintf(string, "result_%s_integrator_%s_timesteps_level_%d.csv",
		integrator_arg, variable_arg, level);

	// Save the final position, so that can compare between OS
	simulator->save(string);

	delete simulator;
	delete integrator;

#ifdef PGPLOT
	// Write the time taken in the plot title - will be 0 time if not timer found
	sprintf(string, "%s integrator, %s timesteps, level %d, %.3f seconds",
		integrator_arg, variable_arg, level, elapsed);

	cpglab("x", "y", string);
	cpgclos();

	sprintf(string, "cp pgplot.gif orbit%s%s%d.gif", integrator_arg, variable_arg, level);
	int res=system(string); // Avoid warning of ignoring return value
#endif

	// Write execution time to output
	sprintf(string, "Simulation Time %s integrator, %s timesteps, level %d, %.3f seconds",
		integrator_arg, variable_arg, level, elapsed);
	cout << string << "\n";
}

int main()
{
	Body::commit_hook = commit_hook;

	clock_t time0 = clock();

	orbit_bodies = 2;
	orbit_mod = 20;
	orbit_max = 10;

	Real orbits = (orbit_mod - 0.5) * orbit_max;
	Real sim_time = 28.75 * orbits * 1.33;

	orbit_graph("proto-hermite", true, sim_time, 1, 0.0f, 1);
	orbit_graph("proto-hermite", true, sim_time, 1, 0.0f, 2);
	orbit_graph("proto-hermite", true, sim_time, 1, 0.0f, 3);
	orbit_graph("proto-hermite", false, sim_time, 1, 0.0f, 1);
	orbit_graph("proto-hermite", false, sim_time, 1, 0.0f, 2);
	orbit_graph("proto-hermite", false, sim_time, 1, 0.0f, 3);

	orbit_graph("leap-frog", true, sim_time, 1, 0.0f, 1);
	orbit_graph("leap-frog", true, sim_time, 1, 0.0f, 2);
	orbit_graph("leap-frog", true, sim_time, 1, 0.0f, 3);
	orbit_graph("leap-frog", false, sim_time, 1, 0.0f, 1);
	orbit_graph("leap-frog", false, sim_time, 1, 0.0f, 2);
	orbit_graph("leap-frog", false, sim_time, 1, 0.0f, 3);

#ifdef MULTISTEP
	orbit_graph("multi-step", true, sim_time, 1, 0.0f, 1);
	orbit_graph("multi-step", true, sim_time, 1, 0.0f, 2);
	orbit_graph("multi-step", true, sim_time, 1, 0.0f, 3);
	orbit_graph("multi-step", false, sim_time, 1, 0.0f, 1);
	orbit_graph("multi-step", false, sim_time, 1, 0.0f, 2);
	orbit_graph("multi-step", false, sim_time, 1, 0.0f, 3);
#endif

	orbit_graph("runge-kutta-2n", true, sim_time, 1, 0.0f, 1);
	orbit_graph("runge-kutta-2n", true, sim_time, 1, 0.0f, 2);
	orbit_graph("runge-kutta-2n", true, sim_time, 1, 0.0f, 3);
	orbit_graph("runge-kutta-2n", false, sim_time, 1, 0.0f, 1);
	orbit_graph("runge-kutta-2n", false, sim_time, 1, 0.0f, 2);
	orbit_graph("runge-kutta-2n", false, sim_time, 1, 0.0f, 3);

	//orbit_graph("runge-kutta-2n-fsal", true, sim_time, 1, 0.0f, 1);
	//orbit_graph("runge-kutta-2n-fsal", true, sim_time, 1, 0.0f, 2);
	//orbit_graph("runge-kutta-2n-fsal", true, sim_time, 1, 0.0f, 3);
	//orbit_graph("runge-kutta-2n-fsal", false, sim_time, 1, 0.0f, 1);
	//orbit_graph("runge-kutta-2n-fsal", false, sim_time, 1, 0.0f, 2);
	//orbit_graph("runge-kutta-2n-fsal", false, sim_time, 1, 0.0f, 3);

	orbit_graph("runge-kutta-3", true, sim_time, 0, 0.0f, 1);
	orbit_graph("runge-kutta-3", true, sim_time, 0, 0.0f, 2);
	orbit_graph("runge-kutta-3", true, sim_time, 0, 0.0f, 3);
	orbit_graph("runge-kutta-3", false, sim_time, 0, 0.0f, 1);
	orbit_graph("runge-kutta-3", false, sim_time, 0, 0.0f, 2);
	orbit_graph("runge-kutta-3", false, sim_time, 0, 0.0f, 3);

	orbit_graph("runge-kutta-4", true, sim_time, 1, 0.0f, 1);
	orbit_graph("runge-kutta-4", true, sim_time, 1, 0.0f, 2);
	orbit_graph("runge-kutta-4", true, sim_time, 1, 0.0f, 3);
	orbit_graph("runge-kutta-4", false, sim_time, 1, 0.0f, 1);
	orbit_graph("runge-kutta-4", false, sim_time, 1, 0.0f, 2);
	orbit_graph("runge-kutta-4", false, sim_time, 1, 0.0f, 3);

	orbit_graph("runge-kutta-4n", true, sim_time, 1, 0.0f, 1);
	orbit_graph("runge-kutta-4n", true, sim_time, 1, 0.0f, 2);
	orbit_graph("runge-kutta-4n", true, sim_time, 1, 0.0f, 3);
	orbit_graph("runge-kutta-4n", false, sim_time, 1, 0.0f, 1);
	orbit_graph("runge-kutta-4n", false, sim_time, 1, 0.0f, 2);
	orbit_graph("runge-kutta-4n", false, sim_time, 1, 0.0f, 3);

	orbit_graph("chin-chen", true, sim_time, 1, 0.0f, 1);
	orbit_graph("chin-chen", true, sim_time, 1, 0.0f, 2);
	orbit_graph("chin-chen", true, sim_time, 1, 0.0f, 3);
	orbit_graph("chin-chen", false, sim_time, 1, 0.0f, 1);
	orbit_graph("chin-chen", false, sim_time, 1, 0.0f, 2);
	orbit_graph("chin-chen", false, sim_time, 1, 0.0f, 3);

	orbit_graph("chin-chen-optimised", true, sim_time, 1, 0.0f, 1);
	orbit_graph("chin-chen-optimised", true, sim_time, 1, 0.0f, 2);
	orbit_graph("chin-chen-optimised", true, sim_time, 1, 0.0f, 3);
	orbit_graph("chin-chen-optimised", false, sim_time, 1, 0.0f, 1);
	orbit_graph("chin-chen-optimised", false, sim_time, 1, 0.0f, 2);
	orbit_graph("chin-chen-optimised", false, sim_time, 1, 0.0f, 3);

	orbit_graph("hermite", true, sim_time, 1, 0.0f, 1);
	orbit_graph("hermite", true, sim_time, 1, 0.0f, 2);
	orbit_graph("hermite", true, sim_time, 1, 0.0f, 3);
	orbit_graph("hermite", false, sim_time, 1, 0.0f, 1);
	orbit_graph("hermite", false, sim_time, 1, 0.0f, 2);
	orbit_graph("hermite", false, sim_time, 1, 0.0f, 3);

	orbit_graph("hermite-6", true, sim_time, 1, 0.0f, 1);
	orbit_graph("hermite-6", true, sim_time, 1, 0.0f, 2);
	orbit_graph("hermite-6", true, sim_time, 1, 0.0f, 3);
	orbit_graph("hermite-6", false, sim_time, 1, 0.0f, 1);
	orbit_graph("hermite-6", false, sim_time, 1, 0.0f, 2);
	orbit_graph("hermite-6", false, sim_time, 1, 0.0f, 3);

	orbit_graph("hermite-8", true, sim_time, 1, 0.0f, 1);
	orbit_graph("hermite-8", true, sim_time, 1, 0.0f, 2);
	orbit_graph("hermite-8", true, sim_time, 1, 0.0f, 3);
	orbit_graph("hermite-8", false, sim_time, 1, 0.0f, 1);
	orbit_graph("hermite-8", false, sim_time, 1, 0.0f, 2);
	orbit_graph("hermite-8", false, sim_time, 1, 0.0f, 3);

	orbit_graph("forward-euler", true, sim_time, 1, 0.0f, 1);
	orbit_graph("forward-euler", true, sim_time, 1, 0.0f, 2);
	orbit_graph("forward-euler", true, sim_time, 1, 0.0f, 3);
	orbit_graph("forward-euler", false, sim_time, 1, 0.0f, 1);
	orbit_graph("forward-euler", false, sim_time, 1, 0.0f, 2);
	orbit_graph("forward-euler", false, sim_time, 1, 0.0f, 3);

	orbit_graph("forward-plus", true, sim_time, 1, 0.0f, 1);
	orbit_graph("forward-plus", true, sim_time, 1, 0.0f, 2);
	orbit_graph("forward-plus", true, sim_time, 1, 0.0f, 3);
	orbit_graph("forward-plus", false, sim_time, 1, 0.0f, 1);
	orbit_graph("forward-plus", false, sim_time, 1, 0.0f, 2);
	orbit_graph("forward-plus", false, sim_time, 1, 0.0f, 3);

	cout << "Total Execution Time = " << ((double)(clock() - time0)) / CLOCKS_PER_SEC << " seconds\n";

	return 0;
}

