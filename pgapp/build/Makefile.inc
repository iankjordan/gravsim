ACSLIB=../../../acslib
DEFINES=$(DEF)$(SIMULATOR)
ACSINCLUDE=$(INC)$(ACSLIB)
LIBS=$(ACSLIBTARGET)/acslib.a

main.o : 	../../main.cpp $(ACSLIB)/*.h	    

%.o : ../../%.cpp
	$(CC) $(CCFLAGS) $(DEFINES) $(INCLUDES) $(ACSINCLUDE) $(CCOUT)$@ $< 

$(TARGET) :	main.o $(LIBS)
	$(LD) $(LDFLAGS) $(LDOUT)$@ *.o $(LIBPATH) $(LIBS) $(STDLIBS)
	-cp $(TARGET) ../../../$(TARGET)

clean:
	-rm -f *.o
	-rm -f *.exe
