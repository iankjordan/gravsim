rm result2.txt

rem individual, direct

echo 10000, individual, direct, hilbert, >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d hilbert -t individual -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result2.txt

rem echo 10000, individual, direct, morton,  >> result2.txt
rem timer > timer.txt
rem gravsim -f model10000.csv -d morton -t individual -n 1 -m 0.1 > log.txt
rem timer /s > timer.txt
rem grep seconds timer.txt >> result2.txt

echo 10000, individual, direct, octree,  >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d octree -t individual -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result2.txt

echo 10000, individual, direct, kdtree,  >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d kdtree -t individual -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result2.txt

rem individual, pairwise

echo 10000, individual, pairwise, hilbert, >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d hilbert -t individual -c pairwise -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result2.txt

rem echo 10000, individual, pairwise, morton,  >> result2.txt
rem timer > timer.txt
rem gravsim -f model10000.csv -d morton -t individual -c pairwise -n 1 -m 0.1 > log.txt
rem timer /s > timer.txt
rem grep seconds timer.txt >> result2.txt

echo 10000, individual, pairwise, octree,  >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d octree -t individual -c pairwise -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result2.txt

echo 10000, individual, pairwise, kdtree,  >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d kdtree -t individual -c pairwise -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result2.txt

rem individual, null

echo 10000, individual,  null,             >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d null -t individual -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result2.txt

rem shared, direct

echo 10000, shared, direct, hilbert, >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d hilbert -t shared -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result2.txt

rem echo 10000, shared, direct, morton,  >> result2.txt
rem timer > timer.txt
rem gravsim -f model10000.csv -d morton -t shared -n 1 -m 0.1 > log.txt
rem timer /s > timer.txt
rem grep seconds timer.txt >> result2.txt

echo 10000, shared, direct, octree,  >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d octree -t shared -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result2.txt

echo 10000, shared, direct, kdtree,  >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d kdtree -t shared -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result2.txt

rem shared, pairwise

echo 10000, shared, pairwise, hilbert, >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d hilbert -t shared -c pairwise -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result2.txt

rem echo 10000, shared, pairwise, morton,  >> result2.txt
rem timer > timer.txt
rem gravsim -f model10000.csv -d morton -t shared -c pairwise -n 1 -m 0.1 > log.txt
rem timer /s > timer.txt
rem grep seconds timer.txt >> result2.txt

echo 10000, shared, pairwise, octree,  >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d octree -t shared -c pairwise -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result2.txt

echo 10000, shared, pairwise, kdtree,  >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d kdtree -t shared -c pairwise -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result2.txt

rem shared, null

echo 10000, shared, null,              >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d null -t shared -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result2.txt

rem constant, direct

echo 10000, constant, direct, hilbert, >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d hilbert -t constant -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result2.txt

rem echo 10000, constant, direct, morton,  >> result2.txt
rem timer > timer.txt
rem gravsim -f model10000.csv -d morton -t constant -n 1 -m 0.1 > log.txt
rem timer /s > timer.txt
rem grep minutes timer.txt >> result2.txt

echo 10000, constant, direct, octree,  >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d octree -t constant -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result2.txt

echo 10000, constant, direct, kdtree,  >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d kdtree -t constant -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result2.txt

rem constant, pairwise

echo 10000, constant, pairwise, hilbert, >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d hilbert -t constant -c pairwise -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result2.txt

rem echo 10000, constant, pairwise, morton,  >> result2.txt
rem timer > timer.txt
rem gravsim -f model10000.csv -d morton -t constant -c pairwise -n 1 -m 0.1 > log.txt
rem timer /s > timer.txt
rem grep minutes timer.txt >> result2.txt

echo 10000, constant, pairwise, octree,  >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d octree -t constant -c pairwise -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result2.txt

echo 10000, constant, pairwise, kdtree,  >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d kdtree -t constant -c pairwise -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result2.txt

rem constant, null

echo 10000, constant, null,              >> result2.txt
timer > timer.txt
gravsim -f model10000.csv -d null -t constant -n 1 -m 0.1 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result2.txt
