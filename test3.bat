rm result3.txt

rem individual, direct

echo 100000, individual, direct, hilbert, >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d hilbert -t individual -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result3.txt

rem echo 100000, individual, direct, morton,  >> result3.txt
rem timer > timer.txt
rem gravsim -f model100000.csv -d morton -t individual -n 1 -m 0.01 > log.txt
rem timer /s > timer.txt
rem grep seconds timer.txt >> result3.txt

echo 100000, individual, direct, octree,  >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d octree -t individual -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result3.txt

echo 100000, individual, direct, kdtree,  >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d kdtree -t individual -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result3.txt

rem individual, pairwise

echo 100000, individual, pairwise, hilbert, >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d hilbert -t individual -c pairwise -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result3.txt

rem echo 100000, individual, pairwise, morton,  >> result3.txt
rem timer > timer.txt
rem gravsim -f model100000.csv -d morton -t individual -c pairwise -n 1 -m 0.01 > log.txt
rem timer /s > timer.txt
rem grep seconds timer.txt >> result3.txt

echo 100000, individual, pairwise, octree,  >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d octree -t individual -c pairwise -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result3.txt

echo 100000, individual, pairwise, kdtree,  >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d kdtree -t individual -c pairwise -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep seconds timer.txt >> result3.txt

rem individual, null

echo 100000, individual,  null,     >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d null -t individual -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result3.txt

rem shared, direct

echo 100000, shared, direct, hilbert, >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d hilbert -t shared -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result3.txt

rem echo 100000, shared, direct, morton,  >> result3.txt
rem timer > timer.txt
rem gravsim -f model100000.csv -d morton -t shared -n 1 -m 0.01 > log.txt
rem timer /s > timer.txt
rem grep minutes timer.txt >> result3.txt

echo 100000, shared, direct, octree,  >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d octree -t shared -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result3.txt

echo 100000, shared, direct, kdtree,  >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d kdtree -t shared -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result3.txt

rem shared, pairwise

echo 100000, shared, pairwise, hilbert, >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d hilbert -t shared -c pairwise -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result3.txt

rem echo 100000, shared, pairwise, morton,  >> result3.txt
rem timer > timer.txt
rem gravsim -f model100000.csv -d morton -t shared -c pairwise -n 1 -m 0.01 > log.txt
rem timer /s > timer.txt
rem grep minutes timer.txt >> result3.txt

echo 100000, shared, pairwise, octree,  >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d octree -t shared -c pairwise -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result3.txt

echo 100000, shared, pairwise, kdtree,  >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d kdtree -t shared -c pairwise -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result3.txt

rem shared, null

echo 100000, shared, null,              >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d null -t shared -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep hours timer.txt >> result3.txt

rem constant, direct

echo 100000, hilbert, direct, constant, >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d hilbert -t constant -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result3.txt

rem echo 100000, morton, direct, constant,  >> result3.txt
rem timer > timer.txt
rem gravsim -f model100000.csv -d morton -t constant -n 1 -m 0.01 > log.txt
rem timer /s > timer.txt
rem grep minutes timer.txt >> result3.txt

echo 100000, octree, direct,  constant, >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d octree -t constant -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result3.txt

echo 100000, kdtree, direct,  constant, >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d kdtree -t constant -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result3.txt

rem constant, pairwise

echo 100000, constant, pairwise, hilbert, >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d hilbert -t constant -c pairwise -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result3.txt

rem echo 100000, constant, pairwise, morton,  >> result3.txt
rem timer > timer.txt
rem gravsim -f model100000.csv -d morton -t constant -c pairwise -n 1 -m 0.01 > log.txt
rem timer /s > timer.txt
rem grep minutes timer.txt >> result3.txt

echo 100000, constant, pairwise, octree,  >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d octree -t constant -c pairwise -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result3.txt

echo 100000, constant, pairwise, kdtree,  >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d kdtree -t constant -c pairwise -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep minutes timer.txt >> result3.txt

rem constant, null

echo 100000, constant, null,              >> result3.txt
timer > timer.txt
gravsim -f model100000.csv -d null -t constant -n 1 -m 0.01 > log.txt
timer /s > timer.txt
grep hours timer.txt >> result3.txt
