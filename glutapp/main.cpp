/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "acs.h"
#include "cmdline.h"

#include <GL/glut.h>

using namespace GravSim;

int bodies_size;
ifstream orbit;
Model * model = 0;
Integrator * integrator = 0;
Interactor * interactor = 0;
Divider * divider = 0;
Simulator * simulator = 0;
Real args_sim_time_arg = 0.0f;
int args_log_points_arg = 0;

double pointSize(const Integer & id)
{
	double point_size = 1.0f;
	if(id % 17 == 0)
	{
		point_size = 2.0f;
	}
	else if(id % 149 == 0)
	{
		point_size = 3.0f;
	}
	return point_size;
}

void starColour(const Integer & id, const Real & mass)
{
	if(mass > 0.0f)
	{
		int mod = id % 11;
		if(mod == 0)
		{
			glColor3f(1.0f,1.0f,0.0f); // yellow
		}
		else if(mod == 1)
		{
			glColor3f(0.5f,0.5f,1.0f); // blue
		}
		else if(mod == 2)
		{
			glColor3f(1.0f,0.5f,0.5f); // red
		}
		else if(mod == 3)
		{
			glColor3f(1.0f,1.0f,0.5f); // bright yellow
		}
		else if(mod == 4)
		{
			glColor3f(0.75f,0.75f,1.0f); // bright blue
		}
		else if(mod == 5)
		{
			glColor3f(1.0f,0.75f,0.75f); // bright red
		}
		else
		{
			glColor3f(1.0f,1.0f,1.0f); // white
		}
	}
	else
	{
		glColor3f(0.375f,0.0f,0.375f); // dark purple
	}
}

void drawStar(const Integer & id, const Real & mass, const Vector & pos)
{
	double point_size = pointSize(id);
	starColour(id, mass);
	glPointSize(point_size);
	glBegin(GL_POINTS);
	glVertex3f(to_double(pos[0]), to_double(pos[1]), to_double(pos[2]));
	glEnd();
	if((mass > 0.0f) && (point_size > 1.0f))
	{
		glColor3f(1.0f,1.0f,1.0f);
		glPointSize(point_size - 1.0f);
		glBegin(GL_POINTS);
		glVertex3f(to_double(pos[0]), to_double(pos[1]), to_double(pos[2]));
		glEnd();
	}
}

void hideStar(const Integer & id, const Real & mass, const Vector & pos)
{
	glPointSize(pointSize(id));
	glColor3f(0.0f,0.0f,0.0f);
	glBegin(GL_POINTS);
	glVertex3f(to_double(pos[0]), to_double(pos[1]), to_double(pos[2]));
	glEnd();
}

void renderScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	if(model != 0)
	{
		model->render();
		if(simulator != 0)
		{
			simulator->run(args_sim_time_arg, args_sim_time_arg / args_log_points_arg);
		}
	}
	else
	{
		int count = 0;
		int refresh = min(bodies_size, 1024);
		while(!orbit.eof())
		{
			Integer id;
			Real mass;
			Real time1;
			Vector pos1;
			Real time2;
			Vector pos2;
			orbit >> id >> comma >> mass >> comma >> time1 >> comma >> pos1 >> comma >> time2 >> comma >> pos2 >> comma;
			hideStar(id, mass, pos1);
			drawStar(id, mass, pos2);
			count++;
			if(count % refresh == 0) glFlush();
		}
		glFlush();
	}
}

void Model::render()
{
	glBegin(GL_POINTS);
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		body->draw_point();
	}
	glEnd();
	glFlush();
}

void Body::draw_point() const
{
	drawStar(id, mass, pos);
}

void Body::draw_line() const
{
	hideStar(id, mass, point_1.pos);
	drawStar(id, mass, point_0.pos);
}

void body_commit_hook(const Body * body)
{
	body->draw_line();
}

void simulator_commit_hook(const Simulator * simulator)
{
	glFlush();
}

void error_handler(const char * error, const char * context)
{
	cerr << error << ": " << context << "\n";
	exit(1);
}

int main(int argc, char** argv)
{
#ifdef FINESIM
	unsigned int old_cw;
	fpu_fix_start(&old_cw);
#ifdef MPREAL
	mp::mp_init(REAL_DIGITS);
#endif
#endif

	Body::error_handler = error_handler;
	Body::commit_hook = body_commit_hook;
	Simulator::commit_hook = simulator_commit_hook;

	gengetopt_args_info args;
	cmdline_parser(argc, argv, &args);

	char header[256];
	orbit.open(args.file_arg);
	if(!orbit.is_open())
	{
		Body::error_handler("Cannot open file", args.file_arg);
	}
	orbit >> bodies_size >> comma;
	orbit.getline(header, 256);
	if(strcmp(header, "Particle,Time1,Pos1(x),Pos1(y),Pos1(z),Time2,Pos2(x),Pos2(y),Pos2(z)") != 0)
	{
		orbit.close();
		model = new Model();
		model->load(args.file_arg);
		bodies_size = model->get_bodies_size();

		min_integrator_parallel_size = 1000000; // GLUT doesn't seem to like OMP
		logging = (strcmp(args.logging_arg, "on") == 0) ? true : false;
		detailed_logging = (strcmp(args.detailed_logging_arg, "on") == 0) ? true : false;

		integrator = Integrator::create(args.integrator_arg);

		interactor = Interactor::create(args.accelerator_arg, args.interactor_arg);

		divider = Divider::create(args.accelerator_arg, args.divider_arg, interactor, args.theta_arg, 5);

		bool constant_timesteps = (strcmp(args.timestep_arg, "constant") == 0);
		bool shared_timesteps = (strcmp(args.timestep_arg, "shared") == 0);
		bool block_timesteps = true;
		bool analyser = (strcmp(args.analyser_arg, "on") == 0);

		simulator = new Simulator(integrator, divider,
			constant_timesteps, shared_timesteps, block_timesteps, args.level_arg, args.softening_arg, analyser, args.hill_sphere_factor_arg);

		simulator->load(args.file_arg);

		args_sim_time_arg = args.sim_time_arg;
		args_log_points_arg = args.log_points_arg;
	}
	else
	{
		cout << "Loading " << args.file_arg << "\n";
	}

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowPosition(40,60);
	glutInitWindowSize(800,800);
	GLint window = glutCreateWindow("Grav-Sim Viewer");
//    glutWMCloseFunc(window);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
	glScalef(0.9f,0.9f,0.9f);
	glutDisplayFunc(renderScene);
	glutMainLoop();

#ifdef FINESIM
	fpu_fix_end(&old_cw);
#ifdef MPREAL
	mp::mp_finalize();
#endif
#endif

	delete simulator;
	if(divider != 0) delete divider;
	if(interactor != 0) delete interactor;
	delete integrator;

	return 0;
}
