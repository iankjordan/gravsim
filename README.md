# Grav-Sim #

A clone of the contents of gravsim-1.11.zip downloaded from http://www.grav-sim.com/download.html

[The Original README File from gravsim-1.11.zip](README_org)

The copyright of this software remains with the original author (Mark Ridler).

A copy of the Terms from the grav-sim website:

*The Grav-Sim software is free and available for public download.
It is distributed under the terms of the GNU General Public License or GPL.
You may distribute and copy the software for free. You may also modify it and distribute the modifications as long as you make it clear exactly which bits have been modified. The copyright and no-warranty notices must remain intact as part of the distribution.
The Grav-Sim author Mark Ridler retains his copyright on the code.
Grav-Sim acknowledges the copyright of Piet Hut and Jun Makino in the parts of the code which were ported to C++ from the Ruby-based ACS code.
Grav-Sim also acknowledges the copyright of Volker Springel on the Hilbert routines which were ported to C++ from the ANSI C-based GADGET2 code.*

## Changes made to original software ##
* Fixed some compilation and execution issues for Linux builds
* Fixed some execution issues in supplied Windows binaries
* Build for mingw (without pgplot)
* Build for MSVC 2019 (without pgplot or qd)
### Issues addressed ###
#### Compilation under Linux ####
* Forward references in templates
* Updated `Makefile.inc` for Unix
* Error in conversion from ‘float’ to non-scalar type compiling `FastSim`
* Incorrect signature for `Simulator` when building `GravPlot`
#### Issues repoducible in the Windows Version
* Out of range error with `qd` (`acos` with value >1.0)
* NaN errors for integrators derived from `RungeKutta3`
#### Miscellaneous ####
* Move the logging of individual points to option `detailed-logging` in `GravPlot`
* Add measure of CPU time in `GravPlot` gif titles
* Log measure of CPU time in `GravPlot` 
* Option to compile GravPlot without pgplot
* Support for MS Visual Studio 2019
## Required Libraries ##
### Ubuntu 18.04 ###
With installation instructions
#### gcc and g++ ####
`sudo apt install gcc`

`sudo apt install make`

`sudo apt install g++`

#### Gengetopt ####
`sudo apt install gengetopt`
#### Openmp ####
`sudo apt install openmp`

`sudo apt-get install libomp-dev`
#### Opengl and FreeGlut ####
Required by `GravView`

`sudo apt-get install libglu1-mesa-dev freeglut freeglut3-dev`

`sudo apt install libglu`

`sudo apt install mesa-common-dev mesa-utils`
#### qd ####
Download **QD for Unix based Systems** from https://www.davidhbailey.com/dhbsoftware/

Extract from the zip file

Follow the configure and make instructions in the `README` file that is part of the zip contents

#### pgplot ####
Required by `GravPlot`

`sudo apt install pgplot5`
#### Gifview ####
To view images created by `GravPlot`

`sudo apt install gifsicle`

### MSYS2 MinGW 64-Bit  ###
Most packages required can be found at https://packages.msys2.org/search

`qd` was built and installed using the instructions above

`pgplot` was not used
### Microsoft Visual Studio 2019 ###
FreeGlut was downloaded from https://sourceforge.net/projects/freeglut/

Follow the instructions to build. CMake is required https://cmake.org/download/

Build and copy the include, Lib and DLL files to a known location
## Build Instructions ##
### Linux ###
Copy `Makefile.inc.unix-gcc` to `Makefile.inc`

`make clean`

`make all`

`make finesim`

`make gravplot`

### MSYS2 MinGW 64-Bit  ###
Copy `Makefile.inc.windows-gcc` to `Makefile.inc`

`make clean`

`make all`

`make finesim`

`make gravplot`

**Note:** Existing `*.exe` files may need to be deleted after `make clean` for `make` to successfully rebuild
 
### Microsoft Visual Studio 2019 ###
Open the `gravsim` solution in MS VS 19

Set the `glutapp` `VC++ Directories` `Include Directories` and `Library Directories` properties to 
point to the directories used to store the `Freeglut` includes and libraries

The option to build 32-bit binaries has been removed. Only 64-bit binaries are supported

The option to build `FineSim` has been removed (no `qd` support)

`GravPlot` ('pgapp') is built without pgplot support

**Note:** The `glutapp` requires access to the
`Freeglut` dll. (The most simple solution is to copy the dll in the same directory as 
the `glutapp` executable)
## ORIGINAL README ##
    Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

Grav-Sim 1.04 is experimental software and currently under development and testing.

To compile it you will need:

- A C++ compiler (Intel C++, Microsoft Visual C++ and MinGW gcc are known to work on Windows)
- gcc should work on Linux and others, but hasn't been tested so good luck
- gengetopt (see http://www.gnu.org/software/gengetopt/)
- OpenGL/GLUT (see http://www.opengl.org/resources/libraries/glut/)

Optional:

- qd library if you want to compile FineSim (see http://crd.lbl.gov/~dhbailey/mpdist/)
- shiny profiler if you want to look at performance (see http://sourceforge.net/projects/shinyprofiler/)

Choose the appropriate Makefile.inc file from:

- Makefile.inc.unix-gcc
- Makefile.inc.windows-gcc
- Makefile.inc.windows-icl
- Makefile.inc.windows-msvc
- or create one from scratch

and copy it to Makefile.inc

You may want to edit the INCLUDES, LIBPATH and STDLIBS lines to include qd and shiny

Then just type:

> make

or maybe:

> make finesim

etc

You may want to edit the all: dependency in Makefile to include finesim

You should end up with the following executable files:

- FastSim
- GravSim
- GravView
- FineSim (optional)
- GravPlot (optional)

Also included is a Microsoft Visual C++ project gravsim.sln

For starters, try:

> GravView -f model1000.csv

which will animate the 1000-body model for a time duration of 1.0.

Then try:

> FastSim -f model1000.csv -r result1000.csv -o orbit1000.csv

which will do the calculation in command-line mode and store the results in an orbit and model file.

Finally:

> GravView -f orbit1000.csv

Changes For Version 1.01:

- body.h line 47 now reads: virtual void commit(); 
- ridlertree.cpp line 97 now reads: #if (DERIVATIVE >= 4) 
- ridlertree.cpp line 99 now reads: #if (DERIVATIVE >= 5)
- logpoint variable included in accelerator.h
- max_batch_size and min_batch_size moved from basic.h to command-line arguments
- #include <string.h> added to basic.h for unix builds
- #include <GL/glut.h> capitalised for unix builds
- -lglut included in Makefile.inc.unix-gcc
- .exe extension removed from Makefiles for unix builds
- gravplot included in the release

Changes For Version 1.02:

- GravView now animates on-the-fly
- GravView stars drawn with a random mix of colours and sizes
- GravView updates now done with a glFlush using Accelerator::commit_hook

Changes For Version 1.03:

- OpenMP parallel implementation
- Improved logging with time, next-time, step-size and batch-size 
- New min-parallel-batch-size parameter
- gravview plays back orbit files in colour

Changes For Version 1.04:

- Divider plug-in interface
- Null divider is equivalent to BruteForce
- Hilbert reworked (from RidlerTree) to fit the new divider interface
- Morton introduced by taking advantage of existing code
- Octree developed from scratch
- KDTree developed from scratch
- Interactor plug-in interface
- Direct interactor reworked (from RidlerTree) to fit the new interactor interface
- Pairwise interactor started but incomplete
- Integrators reworked for compatibility with the interactors
- Gravity field is a separate member in Body (fixes a bug with the higher-order integrators)
- RungeKutta2n, 3, 4 and 4n all perform substantially better as a result
- Timestamps added to orbit files

Changes For Version 1.05:

- Pairwise interactor completed
- Revised list of accelerators now available:
	brute-force
	barnes-hut
	stadel
	ridler
	dehnen
	dehnen-stadel
	dehnen-ridler
- Based on the following list of space dividers:
	null
	octree
	kdtree
	hilbert
	morton
- And the following list of interactors:
	direct
	pairwise
- Various code changes to accomodate the above
- Adaptive serial / parallel version of PredictorCorrector::step
- Initial tests performed on a million-body Plummer model

Changes For Version 1.06:

- Parallelised brute-force algorithm
- Tidied up step methods in integrator.cpp

Changes For Version 1.1:

- Analyser command-line switch
- Kepler-based Analyser implemented with Elliptic orbit prediction
- Hyperbolic orbit detection is in the code but commented out
- Parabolic and Linear orbits are stubs
- Accelerator renamed to Simulator
- Mass / PointMass renamed to Particle
- Particle inherits from WorldPoint
- Body and Composite inherit from Particle
- WorldLine changed to past points only
- Number of source code files reduced by merging the smaller ones
- Fixed the FineSim bug with commas in model files
- Logging / Detailed-Logging command-line switches
- Serial / Parallel command-line switch
- Min-body-sort-step-size command-line switch
- Standard tests moved to double precision in serial mode
- Model generation options -g, -b and --mass-sign
- Tests performed with negative mass particles


