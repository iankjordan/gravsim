/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "acs.h"

namespace GravSim {

Interactor * Interactor::create(const char * accelerator_arg, const char * interactor_arg)
{
	Interactor * interactor;
	if(strcmp(interactor_arg, "default") == 0)
	{
		if(strcmp(accelerator_arg, "brute-force") == 0)
		{
			interactor = 0;
		}
		else if((strcmp(accelerator_arg, "barnes-hut") == 0) ||
		        (strcmp(accelerator_arg, "stadel") == 0) ||
		        (strcmp(accelerator_arg, "ridler") == 0))
		{
			interactor = new DirectInteractor();
		}
		else if((strcmp(accelerator_arg, "dehnen") == 0) ||
		        (strcmp(accelerator_arg, "dehnen-stadel") == 0) ||
		        (strcmp(accelerator_arg, "dehnen-ridler") == 0))
		{
			interactor = new PairwiseInteractor();
		}
		else
		{
			cerr << "Simulator '" << accelerator_arg << "' not recognised\n";
			exit(1);
		}
	}
	else if(strcmp(interactor_arg, "null") == 0)
	{
		interactor = 0;
	}
	else if(strcmp(interactor_arg, "direct") == 0)
	{
		interactor = new DirectInteractor();
	}
	else if(strcmp(interactor_arg, "pairwise") == 0)
	{
		interactor = new PairwiseInteractor();
	}
	else
	{
		cerr << "Interactor '" << interactor_arg << "' not recognised\n";
		exit(1);
	}

	return interactor;
}

Interactor::Interactor()
{
	simulator = 0;
	divider = 0;
}

Interactor::~Interactor()
{
}

void Interactor::startup(Simulator * a, Divider * d)
{
	simulator = a;
	divider = d;
	bodies = simulator->get_bodies();
	bodies_size = simulator->get_bodies_size();
	step_size = simulator->get_step_size();
	root = divider->get_root();
}

Body ** DirectInteractor::active_buffer()
{
	Body ** buffer = new Body *[step_size];
	int buffer_size = 0;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		if(body->is_active)
		{
			buffer[buffer_size++] = body;
		}
	}
	assert(buffer_size == step_size);
	return buffer;
}

DirectInteractor::DirectInteractor() : Interactor()
{
}

void DirectInteractor::calc_gravity(const GravArgs & args)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 50%)

	if(detailed_logging) cout << setw(10) << "Batch" << ", \t" << setw(10) << "Batch-Size" << "\n" << flush;

	Body ** buffer = active_buffer();
	int BS = calc_batch_size(step_size);
	int NT = batch_threads(step_size, BS);
#	pragma omp parallel num_threads(NT)
	parallel_batch_for(b1, 0, step_size, BS)
	{
		int batch_size = min(step_size - b1, BS);
		Body ** batch = buffer + b1;

		if(detailed_logging) {
#		pragma omp critical
		cout << setw(10) << b1 << ", \t" << setw(10) << batch_size << "\n" << flush; }

		root->calc_gravity_direct(batch, batch_size, args);

		for(int b2 = 0; b2 < batch_size; b2++)
		{
			Body * body2 = batch[b2];
			assert(body2->is_active);
			body2->apply_gravity(args);
		}
	}
	delete buffer;
}

const char * DirectInteractor::name() const
{
	return "direct";
}

void PairwiseInteractor::apply_gravity(const GravArgs & args)
{
	if(detailed_logging) cout << "PairwiseInteractor::apply_gravity()\n" << flush;

	int working_size = simulator->get_working_size();
	if(step_size < (working_size / 4)) // Hierarchical execution
	{
		root->apply_gravity(args);
	}
	else // Linear execution
	{
		simulator->apply_gravity(args);
	}
}

PairwiseInteractor::PairwiseInteractor() : Interactor()
{
}

void PairwiseInteractor::calc_gravity(const GravArgs & args)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 50%)

	root->calc_gravity_pairwise((Particle *) 0, args); // Self-interaction
	apply_gravity(args);
}

const char * PairwiseInteractor::name() const
{
	return "pairwise";
}

} // namespace GravSim
