/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "acs.h"

namespace GravSim {

Divider * Divider::create(const char * accelerator_arg, const char * divider_arg, Interactor * interactor,
                          const Real & theta, int branch_factor)
{
	if(logging) cout << "------------------------------Initialising-------------------------------------\n\n" << flush;

	Divider * divider;
	if(interactor == 0)
	{
		divider = 0;
	}
	else if(strcmp(divider_arg, "default") == 0)
	{
		if(strcmp(accelerator_arg, "brute-force") == 0)
		{
			divider = 0;
		}
		else if((strcmp(accelerator_arg, "barnes-hut") == 0) ||
		        (strcmp(accelerator_arg, "dehnen") == 0))
		{
			divider = new Octree(interactor, theta);
			divider->direct_or_pairwise("barnes-hut", "dehnen");
		}
		else if((strcmp(accelerator_arg, "stadel") == 0) ||
		        (strcmp(accelerator_arg, "dehnen-stadel") == 0))
		{
			divider = new KDTree(interactor, theta);
			divider->direct_or_pairwise("stadel", "dehnen-stadel");
		}
		else if((strcmp(accelerator_arg, "ridler") == 0) ||
		        (strcmp(accelerator_arg, "dehnen-ridler") == 0))
		{
			divider = new CurveTree(interactor, theta, CurveHilbert, branch_factor);
			divider->direct_or_pairwise("ridler", "dehnen-ridler");
		}
		else
		{
			cerr << "Simulator '" << accelerator_arg << "' not recognised\n";
			exit(1);
		}
	}
	else if(strcmp(divider_arg, "null") == 0)
	{
		divider = 0;
	}
	else if(strcmp(divider_arg, "octree") == 0)
	{
		divider = new Octree(interactor, theta);
		divider->direct_or_pairwise("barnes-hut", "dehnen");
	}
	else if(strcmp(divider_arg, "kdtree") == 0)
	{
		divider = new KDTree(interactor, theta);
		divider->direct_or_pairwise("stadel", "dehnen-stadel");
	}
	else if(strcmp(divider_arg, "hilbert") == 0)
	{
		divider = new CurveTree(interactor, theta, CurveHilbert, branch_factor);
		divider->direct_or_pairwise("ridler", "dehnen-ridler");
	}
	else if(strcmp(divider_arg, "morton") == 0)
	{
		divider = new CurveTree(interactor, theta, CurveMorton, branch_factor);
		divider->direct_or_pairwise("ridler", "dehnen-ridler");
	}
	else
	{
		cerr << "Divider '" << divider_arg << "' not recognised\n";
		exit(1);
	}
	if(divider != 0) divider->configure_min_step_size();

	return divider;
}

void Divider::direct_or_pairwise(const char * name1, const char * name2)
{
	if(strcmp(interactor->name(), "direct") == 0)
	{
		accelerator_name = name1;
	}
	else if(strcmp(interactor->name(), "pairwise") == 0)
	{
		accelerator_name = name2;
	}
}

void Divider::configure_min_step_size()
{
	if(strcmp(accelerator_name, "barnes-hut") == 0)
	{
		min_step_size = min_barnes_hut_step_size;
	}
	else if(strcmp(accelerator_name, "stadel") == 0)
	{
		min_step_size = min_stadel_step_size;
	}
	else if(strcmp(accelerator_name, "ridler") == 0)
	{
		min_step_size = min_ridler_step_size;
	}
	else if(strcmp(accelerator_name, "dehnen") == 0)
	{
		min_step_size = min_dehnen_step_size;
	}
	else if(strcmp(accelerator_name, "dehnen-stadel") == 0)
	{
		min_step_size = min_dehnen_stadel_step_size;
	}
	else if(strcmp(accelerator_name, "dehnen-ridler") == 0)
	{
		min_step_size = min_dehnen_ridler_step_size;
	}
}

Divider::Divider(Interactor * i, const Real & th)
{
	simulator = 0;
	interactor = i;
	theta = th;
	min_step_size = 0;
}

Divider::~Divider()
{
}

const char * Divider::interactor_name() const
{
	return interactor->name();
}

void Tree::log_config()
{
	if(logging) {
	cout << "--------Divider Configuration-----------\n";
	cout << "Divider = " << name() << "\n";
	cout << "Theta  = " << theta << "\n";
	cout << "Min Step Size = " <<  min_step_size << "\n";
	cout << "Branch Factor = " << branch_factor << "\n\n";

	cout << "--------Interactor Configuration--------\n";
	cout << "Interactor = " << interactor->name() << "\n\n" << flush; }
}

bool Tree::worth_using() const
{
	Real scale_factor = ((Real) min_step_size) / 3.0f;
	return (simulator->get_step_size() >= min_step_size) &&
	       (simulator->get_step_size() >= (scale_factor * log10((Real) bodies_size)));
}

void Tree::clear_ground()
{
	if(root != 0) cut_down_tree();
}

void Tree::establish_tree()
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 3%)
	if(detailed_logging) cout << "Tree::wait_for_tree()\n" << flush;

	Real time = simulator->get_time();
	if(tree_time != time)
	{
		clear_ground();
		plant_tree();
		grow_tree();

		if(detailed_logging) {
		cout << "Tree Depth = " << root->depth() << "\n";
		cout << "Branch Count = " << next_branch << "\n";
		cout << "Branch Reserve = " << branches_size << "\n" << flush; }

		interactor->startup(simulator, this);
		tree_time = time;
	}
	else
	{
		plant_tree(); // Already grown ... assumes sort order is reproducible
	}
	assert(root != 0);
	assert(tree_time != Infinity);
}

void Tree::plant_tree()
{
}

void Tree::dig_up_tree()
{
}

void Tree::cut_down_tree()
{
	assert(root != 0);
	delete branches;
	branches = 0;
	branches_size = 0;
	next_branch = 0;
	root = 0;
	tree_time = Infinity;
}

Tree::Tree(Interactor * i, const Real & th, int bf) : Divider(i, th)
{
	assert(bf <= MAX_BRANCH_FACTOR);

	root = 0;
	branches = 0;
	branches_size = 0;
	next_branch = 0;
	branch_factor = bf;
	tree_time = Infinity;
}

Tree::~Tree()
{
	clear_ground();
}

void Tree::startup(Simulator * a)
{
	clear_ground();
	simulator = a;
	bodies_size = simulator->get_bodies_size();
	bodies = simulator->get_bodies();
}

Composite * Tree::get_root()
{
	return root;
}

void Tree::calc_gravity(const GravArgs & args)
{
	DETAILED_PROFILE_FUNC();
	if(detailed_logging) cout << "Tree::calc_gravity()\n" << flush;

	establish_tree();
	interactor->calc_gravity(args);
	dig_up_tree();
}

void Octree::grow_tree()
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 3%)
	assert(root == 0);
	assert(MAX_BRANCH_FACTOR >= 8);

	if(detailed_logging) cout << "Growing Octree ..." << flush;

	Vector minpos;
	Vector maxpos;
	simulator->position_limits(minpos, maxpos);

	if(detailed_logging) cout << "\nPass 1 (Gather Leaves)" << flush;

	vector<Body *> leaves;
	for(int b = 0; b < bodies_size; b++)
	{
		leaves.push_back(&bodies[b]);
	}

	if(detailed_logging) cout << "\nPass 2 (Assemble Branches)" << flush;

	branches_size = 0;
	int twigs_size = (bodies_size + 7) / 8;
	while(twigs_size > 1)
	{
		branches_size += twigs_size;
		twigs_size = (twigs_size + 7) / 8;
	}
	branches_size += 1;
	branches_size *= 10; // Reserve safety factor
	branches = new Composite[branches_size];

	root = grow_branch(minpos, maxpos, leaves);

	if(detailed_logging) cout << "\n" << flush;
}

Composite * Octree::grow_branch(const Vector & minpos, const Vector & maxpos, vector<Body *> leaves)
{
	if(leaves.empty()) return 0;
	assert(next_branch < branches_size);
	Composite * branch = &branches[next_branch++];
	branch->is_active = false;
	int leaves_size = leaves.size();
	if(leaves_size <= 8)
	{
		for(int n = 0; n < leaves_size; n++)
		{
			Body * body = leaves[n];
			branch->components[n] = body;
			branch->is_active |= body->is_active;
		}
		branch->components_size = leaves_size;
	}
	else
	{
		Vector middle = (maxpos + minpos) / 2.0f;
		vector<Body *> left_bottom_back;
		vector<Body *> left_bottom_front;
		vector<Body *> left_top_back;
		vector<Body *> left_top_front;
		vector<Body *> right_bottom_back;
		vector<Body *> right_bottom_front;
		vector<Body *> right_top_back;
		vector<Body *> right_top_front;
		foreach(vector<Body *>, l, leaves)
		{
			Body * body = *l;
			if(body->pos[0] <= middle[0]) // left
			{
				if(body->pos[1] <= middle[1]) // bottom
				{
					if(body->pos[2] <= middle[2]) // back
					{
						left_bottom_back.push_back(body);
					}
					else // front
					{
						left_bottom_front.push_back(body);
					}
				}
				else // top
				{
					if(body->pos[2] <= middle[2]) // back
					{
						left_top_back.push_back(body);
					}
					else // front
					{
						left_top_front.push_back(body);
					}
				}
			}
			else // right
			{
				if(body->pos[1] <= middle[1]) // bottom
				{
					if(body->pos[2] <= middle[2]) // back
					{
						right_bottom_back.push_back(body);
					}
					else // front
					{
						right_bottom_front.push_back(body);
					}
				}
				else // top
				{
					if(body->pos[2] <= middle[2]) // back
					{
						right_top_back.push_back(body);
					}
					else // front
					{
						right_top_front.push_back(body);
					}
				}
			}
		}
		Vector top; top(1)   = (maxpos[1] - minpos[1]) / 2.0f;
		Vector front; front(2) = (maxpos[2] - minpos[2]) / 2.0f;
		Composite * branches[8];
//#		pragma omp parallel sections if(!omp_in_parallel())
		{
//#			pragma omp section
			branches[0] = grow_branch(minpos,               middle,               left_bottom_back);
//#			pragma omp section
			branches[1] = grow_branch(minpos + front,       middle + front,       left_bottom_front);
//#			pragma omp section
			branches[2] = grow_branch(minpos + top,         middle + top,         left_top_back);
//#			pragma omp section
			branches[3] = grow_branch(minpos + top + front, middle + top + front, left_top_front);
//#			pragma omp section
			branches[4] = grow_branch(middle - top - front, maxpos - top - front, right_bottom_back);
//#			pragma omp section
			branches[5] = grow_branch(middle - top,         maxpos - top,         right_bottom_front);
//#			pragma omp section
			branches[6] = grow_branch(middle - front,       maxpos - front,       right_top_back);
//#			pragma omp section
			branches[7] = grow_branch(middle,               maxpos,               right_top_front);
		}
		int n = 0;
		for(int b = 0; b < 8; b++)
		{
			Particle * component = branches[b];
			if(component != 0)
			{
				branch->components[n++] = component;
				branch->is_active |= component->is_active;
			}
		}
		branch->components_size = n;
	}
	branch->calc_multipole(theta);
	return branch;
}

Octree::Octree(Interactor * i, const Real & th)
 : Tree(i, th, 8)
{
	assert(MAX_BRANCH_FACTOR >= 8);
}

const char * Octree::name() const
{
	return "octree";
}

void KDTree::grow_tree()
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 3%)
	assert(root == 0);
	assert(MAX_BRANCH_FACTOR >= 8);

	if(detailed_logging) cout << "Growing KDTree ..." << flush;

	Vector minpos;
	Vector maxpos;
	simulator->position_limits(minpos, maxpos);

	if(detailed_logging) cout << "\nPass 1 (Gather Leaves)" << flush;

	vector<Body *> leaves;
	for(int b = 0; b < bodies_size; b++)
	{
		leaves.push_back(&bodies[b]);
	}

	if(detailed_logging) cout << "\nPass 2 (Assemble Branches)" << flush;

	branches_size = 0;
	int twigs_size = (bodies_size + 1) / 2;
	while(twigs_size > 1)
	{
		branches_size += twigs_size;
		twigs_size = (twigs_size + 1) / 2;
	}
	branches_size += 1;
	branches_size *= 20; // Reserve safety factor
	branches = new Composite[branches_size];

	root = grow_branch(minpos, maxpos, leaves, X);

	if(detailed_logging) cout << "\n" << flush;
}

Composite * KDTree::grow_branch(const Vector & minpos, const Vector & maxpos, vector<Body *> leaves, KDPlane plane)
{
	if(leaves.empty()) return 0;
	assert(next_branch < branches_size);
	Composite * branch = &branches[next_branch++];
	branch->is_active = false;
	int leaves_size = leaves.size();
	if(leaves_size <= 2)
	{
		for(int n = 0; n < leaves_size; n++)
		{
			Body * body = leaves[n];
			branch->components[n] = body;
			branch->is_active |= body->is_active;
		}
		branch->components_size = leaves_size;
	}
	else
	{
		Composite * branches[2];
		if(plane == X)
		{
			Real middle = (minpos[0] + maxpos[0]) / 2.0f;
			vector<Body *> left;
			vector<Body *> right;
			foreach(vector<Body *>, l, leaves)
			{
				Body * body = *l;
				if(body->pos[0] <= middle) // left
				{
					left.push_back(body);
				}
				else // right
				{
					right.push_back(body);
				}
			}
//#			pragma omp parallel sections if(!omp_in_parallel())
			{
//#				pragma omp section
				branches[0] = grow_branch(minpos, Vector(middle, maxpos[1], maxpos[2]), left, Y);
//#				pragma omp section
				branches[1] = grow_branch(Vector(middle, minpos[1], minpos[2]), maxpos, right, Y);
			}
		}
		else if(plane == Y)
		{
			Real middle = (minpos[1] + maxpos[1]) / 2.0f;
			vector<Body *> bottom;
			vector<Body *> top;
			foreach(vector<Body *>, l, leaves)
			{
				Body * body = *l;
				if(body->pos[1] <= middle) // bottom
				{
					bottom.push_back(body);
				}
				else // top
				{
					top.push_back(body);
				}
			}
//#			pragma omp parallel sections if(!omp_in_parallel())
			{
//#				pragma omp section
				branches[0] = grow_branch(minpos, Vector(maxpos[0], middle, maxpos[2]), bottom, Z);
//#				pragma omp section
				branches[1] = grow_branch(Vector(minpos[0], middle, minpos[2]), maxpos, top, Z);
			}
		}
		else // if(plane == Z)
		{
			Real middle = (minpos[2] + maxpos[2]) / 2.0f;
			vector<Body *> back;
			vector<Body *> front;
			foreach(vector<Body *>, l, leaves)
			{
				Body * body = *l;
				if(body->pos[2] <= middle) // back
				{
					back.push_back(body);
				}
				else // front
				{
					front.push_back(body);
				}
			}
//#			pragma omp parallel sections if(!omp_in_parallel())
			{
//#				pragma omp section
				branches[0] = grow_branch(minpos, Vector(maxpos[0], maxpos[1], middle), back, X);
//#				pragma omp section
				branches[1] = grow_branch(Vector(minpos[0], minpos[1], middle), maxpos, front, X);
			}
		}
		int n = 0;
		for(int b = 0; b < 2; b++)
		{
			Particle * component = branches[b];
			if(component != 0)
			{
				branch->components[n++] = component;
				branch->is_active |= component->is_active;
			}
		}
		branch->components_size = n;
	}
	branch->calc_multipole(theta);
	return branch;
}

KDTree::KDTree(Interactor * i, const Real & th)
 : Tree(i, th, 2)
{
	assert(MAX_BRANCH_FACTOR >= 2);
}

const char * KDTree::name() const
{
	return "kdtree";
}

void CurveTree::plant_tree()
{
	if(curve == CurveHilbert)
	{
		if(detailed_logging) cout << "CurveTree::plant_tree() : Sort Hilbert" << flush;

		if(simulator->get_step_size() > min_body_sort_step_size)
		{
			simulator->sort_bodies_hilbert();
		}
		else
		{
			body_pointers = simulator->sort_body_pointers_hilbert();
		}
	}
	else // if(curve == Morton)
	{
		if(detailed_logging) cout << "CurveTree::plant_tree() : Sort Morton" << flush;

		if(simulator->get_step_size() > min_body_sort_step_size)
		{
			simulator->sort_bodies_morton();
		}
		else
		{
			body_pointers = simulator->sort_body_pointers_morton();
		}
	}
	if(detailed_logging) cout << "\n" << flush;
}

void CurveTree::grow_tree()
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 3%)
	assert(root == 0);
	assert(branch_factor <= MAX_BRANCH_FACTOR);

	if(detailed_logging) cout << "CurveTree::grow_tree()\n" << flush;

	if(detailed_logging) cout << "Pass 1 : Assemble Twigs" << flush;

	branches_size = 0;
	int twigs_size = (bodies_size + branch_factor - 1) / branch_factor;
	while(twigs_size > 1)
	{
		branches_size += twigs_size;
		twigs_size = (twigs_size + branch_factor - 1) / branch_factor;
	}
	branches_size += 1;
	twigs_size = (bodies_size + branch_factor - 1) / branch_factor;
	branches = new Composite[branches_size];
	Composite ** twigs = new Composite *[twigs_size];
	next_branch = 0;
	int NT = batch_threads(bodies_size, branch_factor);
#	pragma omp parallel num_threads(NT)
	parallel_batch_for(b1, 0, bodies_size, branch_factor)
	{
		int b = b1 / branch_factor;
		Composite * twig = &branches[next_branch + b];
		twigs[b] = twig;
		for(int b2 = b1; b2 < min(b1 + branch_factor, bodies_size); b2++)
		{
			Body * body = (body_pointers != 0) ? body_pointers[b2] : &bodies[b2];
			twig->add(body);
		}
		twig->calc_multipole(theta);
	}

	if(detailed_logging) cout << "\nPass 2 : Assemble Branches" << flush;

	next_branch += twigs_size;
	while(twigs_size > 1)
	{
		int twigs2_size = (twigs_size + branch_factor - 1) / branch_factor;
		Composite ** twigs2 = new Composite *[twigs2_size];
		for(int t1 = 0; t1 < twigs_size; t1 += branch_factor)
		{
			int b = t1 / branch_factor;
			Composite * branch = &branches[next_branch + b];
			twigs2[b] = branch;
			for(int t2 = t1; t2 < min(t1 + branch_factor, twigs_size); t2++)
			{
				branch->add(twigs[t2]);
			}
			branch->calc_multipole(theta);
		}
		delete twigs;
		twigs = twigs2;
		twigs_size = twigs2_size;
		next_branch += twigs_size;
	}
	if(twigs_size > 0)
	{
		assert(twigs_size == 1);
		root = twigs[0];
		delete twigs;
	}

	if(body_pointers != 0)
	{
		delete body_pointers;
		body_pointers = 0;
	}

	if(detailed_logging) cout << "\n" << flush;
}

void CurveTree::dig_up_tree()
{
	if(detailed_logging) cout << "CurveTree::dig_up_tree()\n" << flush;

//	simulator->sort_bodies_is_active(); // Experimental option - can restore original sort order
}

CurveTree::CurveTree(Interactor * i, const Real & th, SpaceFillingCurve cv, int bf)
 : Tree(i, th, bf)
{
	curve = cv;
	body_pointers = 0;
}

const char * CurveTree::name() const
{
	return (curve == CurveHilbert) ? "hilbert" : "morton";
}

} // namespace GravSim
