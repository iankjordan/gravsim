/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRAVSIM_BASIC_H
#define GRAVSIM_BASIC_H

extern bool serial;
extern bool logging;
extern bool detailed_logging;

extern int max_batch_size;
extern int min_integrator_parallel_size;
extern int min_brute_force_parallel_size;
extern int min_barnes_hut_step_size;
extern int min_stadel_step_size;
extern int min_ridler_step_size;
extern int min_dehnen_step_size;
extern int min_dehnen_stadel_step_size;
extern int min_dehnen_ridler_step_size;
extern int min_body_sort_step_size;

extern int num_procs;
extern int max_threads;
extern int num_threads;

#define min(x, y) (((x) > (y)) ? (y) : (x))
#define max(x, y) (((x) < (y)) ? (y) : (x))

inline int parallel_threads(int R)
{
	return min(max_threads, R);
}

inline int parallel_range(int R, int NT)
{
	return (R + NT - 1) / NT;
}

inline int parallel_start(int R, int TN, int NT)
{
	return TN * parallel_range(R, NT);
}

inline int parallel_finish(int R, int TN, int NT)
{
	return min((TN + 1) * parallel_range(R, NT), R);
}

#define serial_for(I, S, F) \
	for(int I = S; I < F; I++)

#define parallel_for(I, S, F) \
	for(int TN = omp_get_thread_num(), NT = omp_get_num_threads(), I = parallel_start(F - S, TN, NT) + S; I < parallel_finish(F - S, TN, NT) + S; I++)

inline int calc_batch_size(int R)
{
	int BS = min(max_batch_size, (R + max_threads - 1) / max_threads);
	return min(BS, R);
}

inline int batch_total(int R, int BS)
{
	return ((R + BS - 1) / BS);
}

inline int batch_threads(int R)
{
	return min(max_threads, batch_total(R, calc_batch_size(R)));
}

inline int batch_threads(int R, int BS)
{
	return min(max_threads, batch_total(R, BS));
}

inline int batch_range(int R, int NT, int BS)
{
	return (batch_total(R, BS) + NT - 1) / NT * BS;
}

inline int batch_start(int R, int TN, int NT, int BS)
{
	return (TN * batch_range(R, NT, BS));
}

inline int batch_finish(int R, int TN, int NT, int BS)
{
	return min((TN + 1) * batch_range(R, NT, BS), R);
}

#define serial_batch_for(I, S, F, BS) \
	for(int I = batch_start(F - S, 0, 1, BS) + S; I < batch_finish(F - S, 0, 1, BS) + S; I += BS)

#define parallel_batch_for(I, S, F, BS) \
	for(int TN = omp_get_thread_num(), NT = omp_get_num_threads(), I = batch_start(F - S, TN, NT, BS) + S; I < batch_finish(F - S, TN, NT, BS) + S; I += BS)

inline int calc_pairwise_size(int I, int R)
{
	return min(I, R - I);
}

inline int pairwise_cycle(int PS)
{
	return PS * 2;
}

inline int pairwise_max_threads(int R, int PS)
{
	int PC = pairwise_cycle(PS);
	return ((R + PC - 1) / PC) * PS;
}

inline int pairwise_threads(int R, int PS)
{
	return min(max_threads, pairwise_max_threads(R, PS));
}

inline int pairwise_increment(int I, int PS, int NT)
{
	for(int T = 0; T < NT; T++)
	{
		I += ((I + 1) % PS == 0) ? PS + 1 : 1;
	}
	return I;
}

#define parallel_pairwise_for(I, S, F, PS) \
 for(int TN = omp_get_thread_num(), NT = omp_get_num_threads(), I = pairwise_increment(0, PS, TN); I < F; I = pairwise_increment(I, PS, NT))

#define parallel_pairwise_for2(I, S, F, PS) \
 for(int TN = omp_get_thread_num(), NT = omp_get_num_threads(), I = pairwise_increment(0, PS, TN) + PS; I < F; I = pairwise_increment(I, PS, NT))

// QSORT appears to be up to 6x quicker than STL_SORT

#define QSORT

#ifdef QSORT
#define quick_sort(A, B, C, D) qsort((A), (B), (C), (D))
#else
#define quick_sort(A, B, C, D) sort((A), (A) + (B), (D))
#endif

// DERIVATIVE controls the range of integrators available and how much memory is required.
// Possible Values:
//   2 ... Leap-Frog, Runge-Kutta (2n, 2n-fsal)
//   3 ... Hermite, Chin-Chen (Optimised), Runge-Kutta (3, 4, 4n) 
//   4 ... Hermite-6
//   5 ... Hermite-8
//

#ifdef FASTSIM
#define DERIVATIVE 4
#endif

#ifdef GRAVSIM
#define DERIVATIVE 5
#endif

#ifdef FINESIM
#define DERIVATIVE 5
#endif

// EXPANSION controls the number of derivatives used in the Taylor expansion
// for local field approximation within a Composite.

#define EXPANSION 1

// MULTISTEP controls whether the idiosyncratic MultiStep integrator is available. This
// requires a full WorldLine point to be kept and needs a potentially arbitrary
// amount of memory for large, long simulations.
//
// FIXME: MULTISTEP currently does not produce orbit graphs and needs debugging 

//#define MULTISTEP

// CONSTANT_TUNING_FACTOR adjusts the overall number of steps corresponding to
// level 1 for constant timesteps.
//
// Increase the tuning factor for an increase in speed and a decrease in quality.
//
// Default: 5.6f

#define CONSTANT_TUNING_FACTOR 5.6f

// VARIABLE_TUNING_FACTOR adjusts the overall number of steps corresponding to
// level 1 for variable timesteps.
//
// Increase the tuning factor for an increase in speed and decrease in quality.
//
// Default: 18.0f

#define VARIABLE_TUNING_FACTOR 18.0f

// MAX_NEIGHBOURS sets the maximum number of neighbours used in orbit analysis.
//
// MAX_NEIGHBOURS = 1 Minimum setting for numerical integration only (analyser "off")
// MAX_NEIGHBOURS = 2 Minimum setting for orbit analysis (analyser "on")

#define MAX_NEIGHBOURS 2

// MAX_BRANCH_FACTOR sets the maximum number of children per Composite body. This affects the
// shape of trees which may be constructed using the Composite struct as the bodies.
//
// branch_factor = 2  KDTree (simple, but too deep for use in N-Body calculations)
// branch_factor = 5  CurveTree (works well for the 1000-Body, 10000-body and 100000-body Plummer models)
// branch_factor = 8  Octree (neat, but too shallow for optimum performance)

#define MAX_BRANCH_FACTOR 8

// THETA sets the minimum distance from a branch body (as a ratio of
// its radius) before the multipole terms can be used.
//
// Possible values of min_theta_multipole depend on the target level of precision and probably
// need to be set in sympathy with the dt_param parameter.
//
// As a rough guide, for a binary tree (branch_factor=2) you might see:
//
// Value     PE Error
// ------------------
// 1.0f      8.1E-4
// 1.1f      3.7E-4
// 1.4f      3.0E-4
// 2.0f      1.4E-4
// 2.8f      4.5E-5
// 4.0f      1.8E-5
// 5.6f      4.8E-6
// 8.0f      8.0E-7
//
// THETA is invalid below value 1.0f.
// However, due to the difference between predicted and corrected positions, it
// can cause objects to escape the self-gravity avoidance test if set lower than
// about 1.1f.
// Configure to Infinity to revert to brute force.

// THETA now implemented as a command-line option


// Timings involving 0.3 billion vector operations (+, -, * or /) on an Intel Pentium
// 4 CPU at 3Ghz were:
//
// Define			Type			Precision	Intel C++	MS VC++		MinGW gcc
// -----------------------------------------------------------------------------
// FLOAT			float			7 digits	N/A			N/A			N/A
// DOUBLE			double			15 digits	5.8s		6.4s		14s
// LONGDOUBLE		long double		20 digits	N/A			N/A			25s
// DOUBLEDOUBLE		doubledouble	30 digits	44s			50s			60s	
// DDREAL			dd_real			30 digits				78s			N/A	
// QDREAL			qd_real			60 digits				510s		N/A	
// MPREAL			mp_real			90 digits				2480s		N/A	
//
// Comparisions were done with the gcc -O flag, since the -O2
// and -O3 flags produced different results and took slightly longer.
//
// On a Windows PC, Intel C++ produces the fastest code overall. It can be used in
// place of MS VC++ for anywhere between 10% and 500% improvement.
//
// With the Intel C++ compiler , you can use the /Qpc32 compiler flag to set the FPU
// precision down to 32 bits (gives roughly 10% improvement). Note the FLOAT type can 
// actually be slower than DOUBLE, at least on the Pentium 4 I'm using. I guess this
// means you should only use FLOAT if memory is the limiting factor.
//
// MS VC++ produces faster code than gcc via MinGW, particularly for DOUBLE. gcc
// supposedly supports LONGDOUBLE whereas MS VC++ doesn't. However, it seems to be
// no more precise than double precision in practice - giving random errors in the
// lower bits.
//
// They all compile OK with the 106-bit doubledouble library. However the results
// differ in the lower bits compared to dd_real, qd_real and mp_real.
//
// Intel C++ and MS VC++ work with the QD library (dd_real and qd_real) and the
// ARPREC multiple precision library (mp_real). However, I can't really see the point
// in going any higher than dd_real, and even then only if you're using BruteForce
// with a high order integrator, individual timesteps and small dt_param.
//
// Haven't yet tried any of this on Linux, but on the strength of the above results,
// I'd expect the Intel Compiler to give significantly better results than gcc.

#ifdef FLOAT
typedef float Real;
#define REAL_TYPE "float"
#define REAL_DIGITS 7
#define Real(x) (float) atof(x)
#define to_int(x) (int)(x)
#define to_double(x) (double)(x)
#define PI 3.1415926f
#ifdef __GNUC__
inline Real abs(const Real & x) { return (Real) abs((int) x); }
#endif
inline Real sqr(const Real & x) { return x * x; }
#define Infinity 1.0E6f
#define Reasonable 1.0E3f
#endif

#ifdef DOUBLE
typedef double Real;
#define REAL_TYPE "double"
#define REAL_DIGITS 15
#define Real(x) atof(x)
#define to_int(x) (int)(x)
#define to_double(x) (double)(x)
#define PI 3.1415926536
#ifdef __GNUC__
inline Real abs(const Real & x) { return (Real) abs((int) x); }
#endif
inline Real sqr(const Real & x) { return x * x; }
#define Infinity 1.0E13f
#define Reasonable 1.0E6f
#endif

#ifdef LONGDOUBLE
// No better than DOUBLE in practice ... gcc pretends to support it but doesn't
typedef long double Real;
#define REAL_TYPE "long double"
#define REAL_DIGITS 20
#define Real(x) atof(x)
#define to_int(x) (int)(x)
#define to_double(x) (double)(x)
#define PI 3.1415926536
#ifdef __GNUC__
inline Real abs(const Real & x) { return (Real) abs((int) x); }
#endif
inline Real sqr(const Real & x) { return x * x; }
#define Infinity 1.0E19f
#define Reasonable 1.0E9f
#endif

#ifdef DOUBLEDOUBLE
// No better than DOUBLE in practice ... use DDREAL instead
#include <doubledouble/doubledouble.h>
typedef doubledouble Real;
#define REAL_TYPE "doubledouble"
#define REAL_DIGITS 30
#define PI doubledouble::Pi
inline Real abs(Real x) { return (Real) abs((int) x); }
#define Infinity 1.0E28f
#define Reasonable 1.0E14f
#endif

#ifdef DDREAL
typedef dd_real Real;
#define REAL_TYPE "dd_real"
#define REAL_DIGITS 30
#define PI dd_real::_pi
#define Infinity 1.0E28f
#define Reasonable 1.0E14f
#endif

#ifdef QDREAL
typedef qd_real Real;
#define REAL_TYPE "qd_real"
#define REAL_DIGITS 60
#define PI qd_real::_pi
#define Infinity 1.0E58f
#define Reasonable 1.0E29f
#endif

#ifdef MPREAL
typedef mp_real Real;
#define REAL_TYPE "mp_real"
#define REAL_DIGITS 90
// You could go higher than 90, but this is already getting silly :-)
#define PI mp_real::_pi
#define Infinity 1.0E88f
#define Reasonable 1.0E44f
#endif

#define PI2 (2.0f * PI)
#define Epsilon (1.0f / Infinity)

inline Real cub(const Real & x) { return sqr(x) * x; }
inline Real frth(const Real & x) { return sqr(sqr(x)); }
inline Real ffth(const Real & x) { return frth(x) * x; }

#if (defined(FLOAT) || defined(DOUBLE))
inline Real acosh(Real x)
{
	return log(x + sqrt(x - 1) * sqrt(x + 1));
}

inline Real atanh(Real x)
{
	return 0.5f * log((1.0f + x) / (1.0f - x));
}
#endif

typedef long long Integer;

inline Real eps(const Real & x, const Real & y, const Real & f = 1.0f) { return (max(max(fabs(x), fabs(y)), 1.0f) * f * Epsilon); }
inline bool eq(const Real & x, const Real & y, const Real & f = 1.0f) { return (fabs(x - y) <= eps(x, y, f)) ? true : false; }
inline bool neq(const Real & x, const Real & y, const Real & f = 1.0f) { return (fabs(x - y) <= eps(x, y, f)) ? false : true; }
inline bool gt(const Real & x, const Real & y, const Real & f = 1.0f) { return ((x - y) > eps(x, y, f)) ? true : false; }
inline bool gte(const Real & x, const Real & y, const Real & f = 1.0f) { return ((x - y) >= -eps(x, y, f)) ? true : false; }
inline bool lt(const Real & x, const Real & y, const Real & f = 1.0f) { return ((x - y) < -eps(x, y, f)) ? true : false; }
inline bool lte(const Real & x, const Real & y, const Real & f = 1.0f) { return ((x - y) <= eps(x, y, f)) ? true : false; }

#define foreach(type, i, group) for(type::iterator i = (group).begin(); i != (group).end(); i++)
#define foreach_const(type, i, group) for(type::const_iterator i = (group).begin(); i != (group).end(); i++)
#define reveach(type, i, group) for(type::reverse_iterator i = (group).rbegin(); i != (group).rend(); i++)
#define reveach_const(type, i, group) for(type::const_reverse_iterator i = (group).rbegin(); i != (group).rend(); i++)

using namespace std;

struct WorldPoint;
struct WorldLine;
struct Particle;
struct Body;
class Integrator;
class PredictorCorrector;
class Model;
class Quadrupole;
struct Expansion;
struct Composite;
class Interactor;
class Divider;
class Simulator;

class Char
{
private:

	char ch;

public:

	Char(char c) { ch = c; }
	bool peek(istream & is) { char pk = is.peek(); return pk == ch; }
	void maybe(istream & is) { char pk = is.peek(); if(pk == ch) is >> ch; }
	friend ostream & operator << (ostream & os, const Char & cH) { return os << cH.ch; }
	friend istream & operator >> (istream & is, Char & cH) { cH.maybe(is); return is; }
};

extern Char comma;
extern Char lbracket;
extern Char rbracket;

class Vector
{
private:

	Real vec[3];

public:

	Vector(const Real & x = 0.0f, const Real & y = 0.0f, const Real & z = 0.0f) { vec[0] = x; vec[1] = y; vec[2] = z; }
	void operator = (const Real & r) { vec[0] = r; vec[1] = r; vec[2] = r; }
	Vector operator - () const { return Vector(-vec[0], -vec[1], -vec[2]); }
	Vector operator + (const Vector & v) const { return Vector(vec[0] + v[0], vec[1] + v[1], vec[2] + v[2]); }
	Vector operator - (const Vector & v) const { return Vector(vec[0] - v[0], vec[1] - v[1], vec[2] - v[2]); }
	Vector operator * (const Real & r) const { return Vector(vec[0] * r, vec[1] * r, vec[2] * r); }
	Vector operator / (const Real & r) const  { return Vector(vec[0] / r, vec[1] / r, vec[2] / r); }
	void operator += (const Vector & v) { vec[0] += v[0]; vec[1] += v[1]; vec[2] += v[2]; }
	void operator -= (const Vector & v) { vec[0] -= v[0]; vec[1] -= v[1]; vec[2] -= v[2]; }
	void operator *= (const Real & r) { vec[0] *= r; vec[1] *= r; vec[2] *= r; }
	void operator /= (const Real & r) { vec[0] /= r; vec[1] /= r; vec[2] /= r; }
	bool operator == (const Vector & v) { return (vec[0] == v[0]) && (vec[1] == v[1]) && (vec[2] == v[2]); }
	Real operator [] (int i) const { return vec[i]; }
	Real & operator () (int i) { return vec[i]; }
	friend Vector operator * (const Real & r, const Vector & v) { return v * r; }
	friend Real dot(const Vector & a, const Vector & b) { return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]; }
	friend Vector cross(const Vector & a, const Vector & b) { return Vector(a[1] * b[2] - a[2] * b[1], a[2] * b[0] - a[0] * b[2], a[0]* b[1] - a[1] * b[0]); }
	friend Real sqr(const Vector & v) { return dot(v, v); }
	friend Real mag(const Vector & v) { return sqrt(sqr(v)); }
	friend Vector norm(const Vector & v) { return v / mag(v); }
	friend bool eq(const Vector & x, const Vector & y, const Real & f = 1.0f) { return (eq(x[0], y[0], f) && eq(x[1], y[1], f) && eq(x[2], y[2], f)); }
	friend bool neq(const Vector & x, const Vector & y, const Real & f = 1.0f) { return (neq(x[0], y[0], f) || neq(x[1], y[1], f) || neq(x[2], y[2], f)); }
	friend ostream & operator << (ostream & os, const Vector & v) { return os << v[0] << comma << v[1] << comma << v[2]; }
	friend istream & operator >> (istream & is, Vector & v) { return is >> v(0) >> comma >> v(1) >> comma >> v(2); }
};

extern Vector zero_vector;

struct PosVel
{
	Vector pos;
	Vector vel;
};

struct AccEtc
{
	Vector acc;
#if (DERIVATIVE >= 3)
	Vector jerk;
#if (DERIVATIVE >= 4)
	Vector snap;
#if (DERIVATIVE >= 5)
	Vector crackle;
#if (DERIVATIVE >= 6)
	Vector pop;
#if (DERIVATIVE >= 7)
	Vector nanda;
#endif
#endif
#endif
#endif
#endif

	void zero();
	void apply(const AccEtc & field);
	void add(const AccEtc & field);
};

struct Point : public PosVel, public AccEtc
{
	void operator = (const Real & r);
	Point operator + (const Point & p) const;
	Point operator - (const Point & p) const;
	Point operator * (const Real & r) const;
	Point operator / (const Real & r) const;
	friend ostream & operator << (ostream & os, const Point & p);
};

struct WorldPoint : public Point
{
	Real time;

	WorldPoint(const Real & t = 0.0f);
	WorldPoint(const Point & p, const Real & t = 0.0f);
	WorldPoint(const WorldPoint & wp);
	WorldPoint(const Vector & p, const Real & t = 0.0f);
	WorldPoint(const Vector & p, const Vector & v, const Real & t = 0.0f);
};

inline ostream & operator << (ostream & os, const WorldPoint & wp)
{
	return os << wp.pos << " " << wp.vel << " ";
}

inline istream & operator >> (istream & is, WorldPoint & wp)
{
	return is >> wp.pos >> wp.vel;
}

class ReplaceBuf : public streambuf
{
    char readBuf_;
    streambuf* pExternBuf_;

    int_type underflow()
    {
        if(gptr() == &readBuf_) return traits_type::to_int_type(readBuf_);

        int_type nextChar = pExternBuf_->sbumpc();
        if(nextChar == EOF) return traits_type::eof();
        readBuf_ = traits_type::to_char_type(nextChar);

        if(readBuf_ == ',') readBuf_ = ' ';

        setg(&readBuf_, &readBuf_, &readBuf_ + 1);
        return traits_type::to_int_type(readBuf_);
    }

public:
    ReplaceBuf(streambuf* pExternBuf) : pExternBuf_(pExternBuf)
    {
        setg(0, 0, 0);
    }
};


#endif
