/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRAVSIM_ANALYSER_H
#define GRAVSIM_ANALYSER_H

#include "basic.h"

class Orbit
{
protected:

	unsigned body_id;    // body->id
	unsigned partner_id; // partner->id
	int ref_count;       // reference count (0 => deletion)
	Real tm;             // Total mass
	WorldPoint cm_0;     // Existing centre of mass
	WorldPoint cm_;      // Next centre of mass

public:

	Real expiry_time_sq; // Expiry time

	Orbit(const Body * body, const Particle * partner);
	void update(const Body * body);
	bool includes(const Body * body) const;
	void free();
	virtual bool valid() const = 0;
	virtual void predict(Body * body, Integrator * integrator) = 0;
	virtual void correct(Body * body, PredictorCorrector * integrator) = 0;
};

struct OrbitalElements
{
	Real gm;           // Gravitational mass (for orbit calculation)
	Real gm2;          // Gravitational mass (for potential calculation)
	Vector r_;         // Relative position
	Vector v_;         // Relative velocity
	Real u;            // Specific orbital energy
	Vector h_; Real h; // Angular momentum (vector and scalar)
	Vector e_; Real e; // Eccentricity (vector and scalar)
	Vector a_; Real a; // Semi-major axis (vector and scalar)
	Vector b_; Real b; // Semi-minor axis (vector and scalar)
	Real Ta;           // True anomaly
	Real Ea;           // Eccentric anomaly
	Real Ma;           // Mean anomaly
	Real Ot;           // Orbital time

	void stage1(const Real & arg_gm, const Real & arg_gm2, const Vector & arg_r_, const Vector & arg_v_,
	            const Real & arg_u, const Vector & arg_h_, const Vector & arg_e_, const Real & arg_Ta);
	void stage2(const Vector & arg_a_, const Real & arg_a, const Vector & arg_b_, const Real & arg_b,
		        const Real & arg_Ea, const Real & arg_Ma, const Real & arg_Ot);
	void log_elements() const;
};

class KeplerOrbit : public Orbit
{
protected:

	OrbitalElements orbit1; // Body orbit relative to cm,dv
	OrbitalElements orbit2; // Partner orbit relative to cm,dv

	static Real reduced_mass(const Real & pm, const Real & tm);
	static Real reduced_mass2(const Real & pm, const Real & tm);
	static Vector specific_angular_momentum_vector(const Vector & r, const Vector & v);
	static Real specific_orbital_energy(const Real & gm, const Vector & r, const Vector & v);
	static Vector eccentricity_vector(const Real & gm, const Vector & r, const Vector & v);
	static Vector semi_major_axis_vector(const Vector & e_, const Real & a);
	static Vector semi_minor_axis_vector(const Vector & e_, const Vector & h_, const Real & b);
	static Real true_anomaly(const Vector & r, const Vector & v, const Vector & e_);
	static Real orbital_time(const Real & gm, const Real & a, const Real & Ma);
	static Real orbital_period(const Real & gm, const Real & a);
	static Real Ma_from_Ot(const Real & gm, const Real & a, const Real & Ot);

public:

	static KeplerOrbit * create(const Body * body, const Particle * partner);

	KeplerOrbit(const Body * body, const Particle * partner);
	void stage1(OrbitalElements & orbit, const Real & gm, const Real & gm2, const Vector & r, const Vector & v);
};

class EllipticOrbit : public KeplerOrbit
{
protected:

	static Real semi_major_axis(const Real & gm, const Real & u);
	static Real semi_minor_axis(const Real & e, const Real & a);
	static Real eccentric_anomaly(const Real & e, const Real & Ta);
	static Real mean_anomaly(const Real & e, const Real & Ea);
	static Vector pericentre(const Real & e, const Vector & a_);
	static Vector apocentre(const Real & e, const Vector & a_);
	static Vector pos_from_Ea(const Real & e, const Vector & a_, const Vector & b_, const Real & Ea);
	static Vector vel_from_Ea(const Real & e, const Vector & a_, const Vector & b_, const Real & Ea, const Real & h);
	static Real Ea_from_Ma(const Real & e, const Real & Ma);

	void predict(WorldPoint & wp, OrbitalElements & orbit);
	void stage2(OrbitalElements & orbit);
	void test(OrbitalElements & orbit);

public:

	EllipticOrbit(const Body * body, const Particle * partner);
	bool valid() const;
	void predict(Body * body, Integrator * integrator);
	void correct(Body * body, PredictorCorrector * integrator);
};

class HyperbolicOrbit : public KeplerOrbit
{
protected:

	static Real semi_major_axis(const Real & gm, const Real & u);
	static Real semi_minor_axis(const Real & e, const Real & a);
	static Real true_anomaly_max(const Real & e);
	static Real true_anomaly_min(const Real & e);
	static Real eccentric_anomaly(const Real & e, const Real & Ta);
	static Real mean_anomaly(const Real & e, const Real & Ea);
	static Vector pericentre(const Real & e, const Vector & a_);
	static Vector pos_from_Ea(const Real & e, const Vector & a_, const Vector & b_, const Real & Ea);
	static Vector vel_from_Ea(const Real & e, const Vector & a_, const Vector & b_, const Real & Ea, const Real & h);
	static Real Ea_from_Ma(const Real & e, const Real & Ma);
	static bool within_analytic_range(const Real & e, const Real & Ta);
	static void log_range(const Real & e);

	void predict(WorldPoint & wp, OrbitalElements & orbit);
	void stage2(OrbitalElements & orbit);
	void test(OrbitalElements & orbit);

public:

	HyperbolicOrbit(const Body * body, const Particle * partner);
	bool valid() const;
	void predict(Body * body, Integrator * integrator);
	void correct(Body * body, PredictorCorrector * integrator);
};

class ParabolicOrbit : public KeplerOrbit
{
protected:

	static Vector pericentre(const Real & hh_gm, const Vector & x_);
	static Vector pos_from_Ta(const Vector & x_, const Vector & y_, const Real & Ta, const Real & hh_gm);
	static Vector vel_from_Ta(const Vector & x_, const Vector & y_, const Real & Ta, const Real & gm_h);

	void stage2(OrbitalElements & orbit);
	void test(OrbitalElements & orbit);

public:

	ParabolicOrbit(const Body * body, const Particle * partner);
	bool valid() const;
	void predict(Body * body, Integrator * integrator);
	void correct(Body * body, PredictorCorrector * integrator);
};

class LinearOrbit : public KeplerOrbit
{
public:

	LinearOrbit(const Body * body, const Particle * partner);
	bool valid() const;
	void predict(Body * body, Integrator * integrator);
	void correct(Body * body, PredictorCorrector * integrator);
};

class Analyser
{
protected:

	Real hill_sphere_factor_sq;

	Real hill_sphere_radius(const Body * body, const Particle * partner) const;
	bool within_hill_sphere(const Body * body, const Particle * partner) const;

public:

	Analyser(const Real & hill_sphere_factor);
	Orbit * analytic_orbit(const Body * body, const Particle * partner);
};

#endif
