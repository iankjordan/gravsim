/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRAVSIM_DIVIDER_H
#define GRAVSIM_DIVIDER_H

#include "basic.h"

class Divider
{
protected:

	const char * accelerator_name;
	Simulator * simulator;
	Interactor * interactor;
	Real theta;
	int min_step_size;
	int bodies_size;
	Body * bodies;

	void direct_or_pairwise(const char * name1, const char * name2);
	void configure_min_step_size();

public:

	static Divider * create(const char * accelerator_arg, const char * divider_arg, Interactor * interactor,
		const Real & theta, int branch_factor);

	const char * get_accelerator_name() const { return accelerator_name; }

	const char * interactor_name() const;

	virtual void log_config() = 0;
	virtual bool worth_using() const = 0;

	Divider(Interactor * interactor, const Real & theta);
	virtual ~Divider();
	virtual void startup(Simulator * simulator) = 0;
	virtual Composite * get_root() = 0;
	virtual void calc_gravity(const GravArgs & args) = 0;
	virtual const char * name() const = 0;
};

class Tree : public Divider
{
protected:

	Composite * root;
	Composite * branches;
	int branches_size;
	int next_branch;
	int branch_factor;
	Real tree_time;

	virtual void clear_ground();
	virtual void establish_tree();
	virtual void plant_tree();
	virtual void grow_tree() = 0;
	virtual void dig_up_tree();
	virtual void cut_down_tree();

public:

	virtual void log_config();
	virtual bool worth_using() const;

	Tree(Interactor * interactor, const Real & theta, int branch_factor);
	virtual ~Tree();
	virtual void startup(Simulator * simulator);
	virtual Composite * get_root();
	virtual void calc_gravity(const GravArgs & args);
};

class Octree : public Tree
{
private:

	void grow_tree();
	Composite * grow_branch(const Vector & maxpos, const Vector & minpos, vector<Body *> leaves);

public:

	Octree(Interactor * interactor, const Real & theta);
	const char * name() const;
};

enum KDPlane
{
	X, Y, Z
};

class KDTree : public Tree
{
private:

	void grow_tree();
	Composite * grow_branch(const Vector & maxpos, const Vector & minpos, vector<Body *> leaves, KDPlane plane);

public:

	KDTree(Interactor * interactor, const Real & theta);
	const char * name() const;
};

enum SpaceFillingCurve
{
	CurveHilbert,
	CurveMorton
};

class CurveTree : public Tree
{
private:

	SpaceFillingCurve curve;
	Body ** body_pointers;

	void plant_tree();
	void grow_tree();
	void dig_up_tree();

public:

	CurveTree(Interactor * interactor, const Real & theta, SpaceFillingCurve curve, int branch_factor);
	const char * name() const;
};

#endif
