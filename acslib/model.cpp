/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler
	Copyright (C) 2007 Piet Hut
	Copyright (C) 2007 Jun Makino

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "acs.h"

namespace GravSim {

Model * Model::create(const char * generate_arg, int bodies_arg, const char * mass_sign_arg)
{
	if(logging) cout <<"------------------------------Generating-------------------------------------\n\n" << flush;

	Model * model = 0;
	ParticleSign mass_sign;
	if(strcmp(mass_sign_arg, "positive") == 0)
	{
		mass_sign = Positive;
	}
	else if(strcmp(mass_sign_arg, "negative") == 0)
	{
		mass_sign = Negative;
	}
	else if(strcmp(mass_sign_arg, "mixture") == 0)
	{
		mass_sign = Mixture;
	}
	else
	{
		cerr << "Particle-Sign '" << mass_sign_arg << "' not recognised\n";
		exit(1);
	}
	if(strcmp(generate_arg, "plummer") == 0)
	{
		model = new Plummer(bodies_arg, mass_sign);
	}
	else if(strcmp(generate_arg, "uniform") == 0)
	{
		model = new Uniform(bodies_arg, mass_sign);
	}
	else
	{
		cerr << "Model '" << generate_arg << "' not recognised\n";
		exit(1);
	}

	return model;
}


void Model::log_stats()
{
	if(logging) {
	cout << "----Stats-------------------------------\n";
	Real tm = total_mass();
	cout << "Total Mass = " << tm << "\n";
	if(neq(tm, 0.0f))
	{
		cout << "Centre Of Mass =\n" << centre_of_mass() << "\n";
		cout << "Drift Velocity =\n" << drift_velocity() << "\n";
	}
	cout << "Maximum Distance = " << maximum_distance() << "\n";
	cout << "Half-Mass Distance = " << half_mass_distance() << "\n";
	cout << "Root-Mean-Square Distance = " << root_mean_square_distance() << "\n";
	cout << "Maximum Speed = " << maximum_speed() << "\n";
	cout << "Half-Mass Speed = " << half_mass_speed() << "\n";
	cout << "Root-Mean-Square Speed = " << root_mean_square_speed() << "\n";
	cout << "Crossing Time = " << crossing_time() << "\n\n" << flush; }
}

#ifdef QSORT

static int compareBodyId(const void * arg1, const void * arg2)
{
	const Body * body1 = (const Body *) arg1;
	const Body * body2 = (const Body *) arg2;
	return (body1->id > body2->id) ? 1 : (body1->id < body2->id) ? -1 : 0;
}

static int compareBodyTime(const void * arg1, const void * arg2)
{
	const Body * body1 = (const Body *) arg1;
	const Body * body2 = (const Body *) arg2;
	return (body1->time > body2->time) ? 1 : (body1->time < body2->time) ? -1 : 0;
}

static int compareBodyNextTime(const void * arg1, const void * arg2)
{
	const Body * body1 = (const Body *) arg1;
	const Body * body2 = (const Body *) arg2;
	return (body1->next_time > body2->next_time) ? 1 : (body1->next_time < body2->next_time) ? -1 : 0;
}

static int compareBodyIsActive(const void * arg1, const void * arg2)
{
	const Body * body1 = (const Body *) arg1;
	const Body * body2 = (const Body *) arg2;
	return (body1->is_active < body2->is_active) ? 1 : (body1->is_active > body2->is_active) ? -1 : 0;
}

static int compareBodyKey(const void * arg1, const void * arg2)
{
	const Body * body1 = (const Body *) arg1;
	const Body * body2 = (const Body *) arg2;
	return (body1->key > body2->key) ? 1 : (body1->key < body2->key) ? -1 : 0;
}

static int compareBodyKeyMass(const void * arg1, const void * arg2)
{
	const Body * body1 = (const Body *) arg1;
	const Body * body2 = (const Body *) arg2;
	return (body1->key > body2->key) ? 1 : (body1->key < body2->key) ? -1 :
	       (body1->mass > body2->mass) ? 1 : (body1->mass < body2->mass) ? -1 : 0;
}

static int compareBodyPointerKeyMass(const void * arg1, const void * arg2)
{
	const Body * body1 = *((const Body **) arg1);
	const Body * body2 = *((const Body **) arg2);
	return (body1->key > body2->key) ? 1 : (body1->key < body2->key) ? -1 :
	       (body1->mass > body2->mass) ? 1 : (body1->mass < body2->mass) ? -1 : 0;
}

#else

static bool compareBodyId(Body body1, Body body2)
{
	return (body1.id < body2->id) ? true : false;
}

static bool compareBodyTime(Body body1, Body body2)
{
	return (body1.time < body2.time) ? true : false;
}

static bool compareBodyNextTime(Body body1, Body body2)
{
	return (body1.next_time < body2.next_time) ? true : false;
}

static bool compareBodyIsActive(Body body1, Body body2)
{
	return (body1.is_active > body2->is_active) ? true : false;
}

static bool compareBodyKey(Body body1, Body body2)
{
	return (body1.key < body2.key) ? true : false;
}

static bool compareBodyKeyMass(Body body1, Body body2)
{
	return (body1.key < body2.key) ? true : (body1.mass < body2.mass) ? true : false;
}

static bool compareBodyPointerKeyMass(Body * body1, Body * body2)
{
	return (body1->key < body2->key) ? true : (body1->mass < body2->mass) ? true : false;
}

#endif

// Hilbert routines borrowed from Springel's GADGET2 code

static int quadrants[24][2][2][2] = {
  /* rotx=0, roty=0-3 */
  {{{0, 7}, {1, 6}}, {{3, 4}, {2, 5}}},
  {{{7, 4}, {6, 5}}, {{0, 3}, {1, 2}}},
  {{{4, 3}, {5, 2}}, {{7, 0}, {6, 1}}},
  {{{3, 0}, {2, 1}}, {{4, 7}, {5, 6}}},
  /* rotx=1, roty=0-3 */
  {{{1, 0}, {6, 7}}, {{2, 3}, {5, 4}}},
  {{{0, 3}, {7, 4}}, {{1, 2}, {6, 5}}},
  {{{3, 2}, {4, 5}}, {{0, 1}, {7, 6}}},
  {{{2, 1}, {5, 6}}, {{3, 0}, {4, 7}}},
  /* rotx=2, roty=0-3 */
  {{{6, 1}, {7, 0}}, {{5, 2}, {4, 3}}},
  {{{1, 2}, {0, 3}}, {{6, 5}, {7, 4}}},
  {{{2, 5}, {3, 4}}, {{1, 6}, {0, 7}}},
  {{{5, 6}, {4, 7}}, {{2, 1}, {3, 0}}},
  /* rotx=3, roty=0-3 */
  {{{7, 6}, {0, 1}}, {{4, 5}, {3, 2}}},
  {{{6, 5}, {1, 2}}, {{7, 4}, {0, 3}}},
  {{{5, 4}, {2, 3}}, {{6, 7}, {1, 0}}},
  {{{4, 7}, {3, 0}}, {{5, 6}, {2, 1}}},
  /* rotx=4, roty=0-3 */
  {{{6, 7}, {5, 4}}, {{1, 0}, {2, 3}}},
  {{{7, 0}, {4, 3}}, {{6, 1}, {5, 2}}},
  {{{0, 1}, {3, 2}}, {{7, 6}, {4, 5}}},
  {{{1, 6}, {2, 5}}, {{0, 7}, {3, 4}}},
  /* rotx=5, roty=0-3 */
  {{{2, 3}, {1, 0}}, {{5, 4}, {6, 7}}},
  {{{3, 4}, {0, 7}}, {{2, 5}, {1, 6}}},
  {{{4, 5}, {7, 6}}, {{3, 2}, {0, 1}}},
  {{{5, 2}, {6, 1}}, {{4, 3}, {7, 0}}}
};

static int rotxmap_table[24] = { 4, 5, 6, 7, 8, 9, 10, 11,
  12, 13, 14, 15, 0, 1, 2, 3, 17, 18, 19, 16, 23, 20, 21, 22
};

static int rotymap_table[24] = { 1, 2, 3, 0, 16, 17, 18, 19,
  11, 8, 9, 10, 22, 23, 20, 21, 14, 15, 12, 13, 4, 5, 6, 7
};

static int rotx_table[8] = { 3, 0, 0, 2, 2, 0, 0, 1 };
static int roty_table[8] = { 0, 1, 1, 2, 2, 3, 3, 0 };

static int sense_table[8] = { -1, -1, -1, +1, +1, -1, -1, -1 };

static int flag_quadrants_inverse = 1;
static char quadrants_inverse_x[24][8];
static char quadrants_inverse_y[24][8];
static char quadrants_inverse_z[24][8];

Integer Model::hilbert(int x, int y, int z, int bits)
{
	int i, quad, bitx, bity, bitz;
	int mask, rotation, rotx, roty, sense;
	Integer key;

	mask = 1 << (bits - 1);
	key = 0;
	rotation = 0;
	sense = 1;

	for(i = 0; i < bits; i++, mask >>= 1)
    {
		bitx = (x & mask) ? 1 : 0;
		bity = (y & mask) ? 1 : 0;
		bitz = (z & mask) ? 1 : 0;

		quad = quadrants[rotation][bitx][bity][bitz];

		key <<= 3;
		key += (sense == 1) ? (quad) : (7 - quad);

		rotx = rotx_table[quad];
		roty = roty_table[quad];
		sense *= sense_table[quad];

		while(rotx > 0)
		{
			rotation = rotxmap_table[rotation];
			rotx--;
		}

		while(roty > 0)
		{
			rotation = rotymap_table[rotation];
			roty--;
		}
	}
	return key;
}

// Morton routines borrowed from Makino's NBody code

Integer Model::morton(int ix, int keybits)
{
    Integer result = 0;
    int i, j;
    for(i = j = 0; i < keybits; i ++, j += 3)
	{
		if((ix >> i) & 1)
		{
			result |= ((Integer) 1) << j;
		}
    }
    return result;
}
    
Integer Model::hilbert_key(const Vector & pos, const Real & scale, const Real & offset, int keybits)
{
	int x = to_int(pos[0] * scale + offset);
	int y = to_int(pos[1] * scale + offset);
	int z = to_int(pos[2] * scale + offset);
	return hilbert(x, y, z, keybits);
}

Integer Model::morton_key(const Vector & pos, const Real & scale, const Real & offset, int keybits)
{
	Integer key = 0;
    for(int i = 0; i < 3; i ++)
	{
		int ix = to_int(pos[i] * scale + offset);
		key |= (morton(ix, keybits) << (2-i));
    }
	return key;
}

void Model::distance_keys()
{
	int keybits = sizeof(Integer) * 8 - 1;
	Real offset = pow(2.0f, keybits);
    Real scale = offset / maximum_distance();
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		body->key = (Integer) to_double(sqrt(sqr(body->pos)) * scale);
	}
}

void Model::speed_keys()
{
	int keybits = sizeof(Integer) * 8 - 1;
	Real offset = pow(2.0f, keybits);
    Real scale = offset / maximum_speed();
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		body->key = (Integer) to_double(sqrt(sqr(body->vel)) * scale);
	}
}

void Model::hilbert_keys()
{
	int keybits = sizeof(Integer) * 8 / 3;
	Real offset = pow(2.0f, keybits);
    Real scale = offset / maximum_distance();
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		body->key = hilbert_key(body->pos, scale, offset, keybits);
	}
}

void Model::morton_keys()
{
	int keybits = sizeof(Integer) * 8 / 3;
	Real offset = pow(2.0f, keybits);
    Real scale = offset / maximum_distance();
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		body->key = morton_key(body->pos, scale, offset, keybits);
	}
}

Body ** Model::new_body_pointers() const
{
	Body ** body_pointers = new Body *[bodies_size];
	for(int b = 0; b < bodies_size; b++)
	{
		body_pointers[b] = &bodies[b];
	}
	return body_pointers;
}

void Model::sort_bodies_id()
{
	DETAILED_PROFILE_FUNC();

	if(sort_order != SortId)
	{
		quick_sort(bodies, bodies_size, sizeof(Body), compareBodyId);
		sort_order = SortId;
	}
}

void Model::sort_bodies_time()
{
	DETAILED_PROFILE_FUNC();

	if(sort_order != SortTime)
	{
		quick_sort(bodies, bodies_size, sizeof(Body), compareBodyTime);
		sort_order = SortTime;
	}
}

void Model::sort_bodies_next_time()
{
	DETAILED_PROFILE_FUNC();

	if(sort_order != SortNextTime)
	{
		quick_sort(bodies, bodies_size, sizeof(Body), compareBodyNextTime);
		sort_order = SortNextTime;
	}
}

void Model::sort_bodies_is_active()
{
	DETAILED_PROFILE_FUNC();

	if(sort_order != SortIsActive)
	{
		quick_sort(bodies, bodies_size, sizeof(Body), compareBodyIsActive);
		sort_order = SortIsActive;
	}
}

void Model::sort_bodies_distance()
{
	DETAILED_PROFILE_FUNC();

	if(sort_order != SortDistance)
	{
		distance_keys();
		quick_sort(bodies, bodies_size, sizeof(Body), compareBodyKey);
		sort_order = SortDistance;
	}
}

void Model::sort_bodies_speed()
{
	DETAILED_PROFILE_FUNC();

	if(sort_order != SortSpeed)
	{
		speed_keys();
		quick_sort(bodies, bodies_size, sizeof(Body), compareBodyKey);
		sort_order = SortSpeed;
	}
}

void Model::sort_bodies_hilbert()
{
	DETAILED_PROFILE_FUNC();

	if(sort_order != SortHilbert)
	{
		hilbert_keys();
		quick_sort(bodies, bodies_size, sizeof(Body), compareBodyKeyMass);
		sort_order = SortHilbert;
	}
}

void Model::sort_bodies_morton()
{
	DETAILED_PROFILE_FUNC();

	if(sort_order != SortMorton)
	{
		morton_keys();
		quick_sort(bodies, bodies_size, sizeof(Body), compareBodyKeyMass);
		sort_order = SortMorton;
	}
}

Body ** Model::sort_body_pointers_hilbert()
{
	DETAILED_PROFILE_FUNC();

	Body ** body_pointers = new_body_pointers();
	hilbert_keys();
	quick_sort(body_pointers, bodies_size, sizeof(Body *), compareBodyPointerKeyMass);
	return body_pointers;
}

Body ** Model::sort_body_pointers_morton()
{
	DETAILED_PROFILE_FUNC();

	Body ** body_pointers = new_body_pointers();
	morton_keys();
	quick_sort(body_pointers, bodies_size, sizeof(Body *), compareBodyPointerKeyMass);
	return body_pointers;
}

void Model::adjust_center()
{
	Vector pos_com, vel_com;
	Real mass = 0.0f;
	for(int b1 = 0; b1 < bodies_size; b1++)
	{
		Body * body = &bodies[b1];
		pos_com += body->pos*body->mass;
		vel_com += body->vel*body->mass;
		mass += body->mass;
	}
	pos_com /= mass;
	vel_com /= mass;
	for(int b2 = 0; b2 < bodies_size; b2++)
	{
		Body * body = &bodies[b2];
		body->pos -= pos_com;
		body->vel -= vel_com;
	}
}

void Model::rotate(Real rate)
{
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		Real distance = mag(body->pos);
		Real factor = rate * distance;
		Vector dv(-body->pos[1] * factor, body->pos[0] * factor, 0);
		body->vel += dv;
	}
}

Model::Model()
{
	bodies = 0;
	bodies_size = 0;
}

Model::~Model()
{
	clear();
}

void Model::clear()
{
	if(bodies != 0) delete[] bodies;
}

void Model::load(istream & is)
{
	clear();
	char header[256];
	is >> bodies_size; is.getline(header, 256);
	bodies = new Body[bodies_size];
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		is >> body->id >> comma >> body->mass >> comma >> body->pos >> comma >> body->vel >> comma; 
		body->validate();
	}
}

void Model::load(const char * filename)
{
	if(strcmp(filename, "stdin") == 0)
	{
		load(cin);
	}
	else
	{
		if(logging) cout <<"Loading " << filename << " ... " << flush;
		ifstream file(filename);
		if(!file.is_open())
		{
			Body::error_handler("Cannot open file", filename);
		}
		ReplaceBuf filter(file.rdbuf());
		istream file_filter(&filter);
		load(file_filter);
		if(logging) cout <<"\n\n" << flush;
	}
}

void Model::load_binary(const char * filename)
{
	if(logging) cout <<"Loading Binary " << filename << " ... " << flush;

	clear();
	ifstream file(filename, ifstream::binary);
	file.read((char *) &bodies_size, 4);
	bodies = new Body[bodies_size];
	int data_size = sizeof(Integer) + sizeof(Real) + sizeof(PosVel);
	int buffer_size = bodies_size * data_size;
	char * buffer = new char[buffer_size];
	file.read(buffer, buffer_size);

	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		char * data = (char *) body;
		for(int d = 0; d < data_size; d++)
		{
			data[d] = buffer[d * bodies_size + b]; // More compressible (particularly by lzop)
		}
	}

	if(logging) cout <<"\n\n" << flush;
}

void Model::save(ostream & os)
{
	os << bodies_size << ",Particle,Pos(x),Pos(y),Pos(z),Vel(x),Vel(y),Vel(z)\n";
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		os << body->id << comma << body->mass << comma << body->pos << comma << body->vel << "\n";
	}
}

void Model::save(const char * filename)
{
	sort_bodies_id();

	if(strcmp(filename, "stdout") == 0)
	{
		save(cout);
	}
	else
	{
		if(logging) cout << "Saving " << filename << " ... " << flush;

		ofstream file(filename);
		if(!file.is_open())
		{
			Body::error_handler("Cannot open file", filename);
		}
		file.precision(REAL_DIGITS);
		save(file);

		if(logging) cout <<"\n\n" << flush;
	}
}

void Model::save_binary(const char * filename)
{
	sort_bodies_id();

	if(logging) cout <<"Saving Binary " << filename << " ... " << flush;

	ofstream file(filename, ofstream::binary);
	file.write((char *) &bodies_size, 4);
	int data_size = sizeof(Integer) + sizeof(Real) + sizeof(PosVel);
	int buffer_size = bodies_size * data_size;
	char * buffer = new char[buffer_size];

	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		char * data = (char *) body;
		for(int d = 0; d < data_size; d++)
		{
			buffer[d * bodies_size + b] = data[d]; // More compressible (particularly by lzop)
		}
	}
	file.write(buffer, buffer_size);

	if(logging) cout <<"\n\n" << flush;
}

Real Model::total_mass()
{
	DETAILED_PROFILE_FUNC();

	Real mass = 0.0f;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		mass += body->mass;
	}
	return mass;
}

Vector Model::centre_of_mass()
{
	DETAILED_PROFILE_FUNC();

	Vector pos_com;
	Real mass = 0.0f;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		pos_com += body->pos * body->mass;
		mass += body->mass;
	}
	return pos_com / mass;
}

Vector Model::drift_velocity()
{
	DETAILED_PROFILE_FUNC();

	Vector vel_com;
	Real mass = 0.0f;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		vel_com += body->vel * body->mass;
		mass += body->mass;
	}
	return vel_com / mass;
}

void Model::position_limits(Vector & minpos, Vector & maxpos)
{
	DETAILED_PROFILE_FUNC();

	minpos = Infinity;
	maxpos = -Infinity;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		if(body->pos[0] > maxpos[0]) maxpos(0) = body->pos[0];
		if(body->pos[1] > maxpos[1]) maxpos(1) = body->pos[1];
		if(body->pos[2] > maxpos[2]) maxpos(2) = body->pos[2];
		if(body->pos[0] < minpos[0]) minpos(0) = body->pos[0];
		if(body->pos[1] < minpos[1]) minpos(1) = body->pos[1];
		if(body->pos[2] < minpos[2]) minpos(2) = body->pos[2];
	}
}

Real Model::maximum_distance()
{
	DETAILED_PROFILE_FUNC();

	Real max_distance_sq = 0.0f;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		Real distance_sq = sqr(body->pos);
	    if(distance_sq > max_distance_sq) max_distance_sq = distance_sq;
	}
	return sqrt(max_distance_sq);
}

Real Model::half_mass_distance()
{
	DETAILED_PROFILE_FUNC();

	sort_bodies_distance();
	Real half_mass = total_mass() / 2.0f;
	Real mass = 0.0f;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		mass += body->mass;
		if(mass >= half_mass)
		{
			return sqrt(sqr(body->pos));
		}
	}
	return 0.0f;
}

Real Model::root_mean_square_distance()
{
	DETAILED_PROFILE_FUNC();

	Real total_distance_sq = 0.0f;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		total_distance_sq += sqr(body->pos);
	}
	return sqrt(total_distance_sq / bodies_size);
}

Real Model::maximum_speed()
{
	DETAILED_PROFILE_FUNC();

	Real max_speed_sq = 0.0f;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		Real speed_sq = sqr(body->vel);
	    if(speed_sq > max_speed_sq) max_speed_sq = speed_sq;
	}
	return sqrt(max_speed_sq);
}

Real Model::half_mass_speed()
{
	DETAILED_PROFILE_FUNC();

	sort_bodies_speed();
	Real half_mass = total_mass() / 2.0f;
	Real mass = 0.0f;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		mass += body->mass;
		if(mass >= half_mass)
		{
			return sqrt(sqr(body->vel));
		}
	}
	return 0.0f;
}

Real Model::root_mean_square_speed()
{
	DETAILED_PROFILE_FUNC();

	Real total_speed_sq = 0.0f;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		total_speed_sq += sqr(body->vel);
	}
	return sqrt(total_speed_sq / bodies_size);
}

Real Model::crossing_time()
{
	DETAILED_PROFILE_FUNC();

	return 2.0f * root_mean_square_distance() / (root_mean_square_speed() + Epsilon);
}

ostream & operator << (ostream & os, Model & model)
{
	model.save(os);
	return os;
}

istream & operator >> (istream & is, Model & model)
{
	model.load(is);
	return is;
}

Real Model::frand(const Real & low, const Real & high)
{
	assert(high > low);
	return low + (high - low) * ((Real) rand()) / ((Real) RAND_MAX + 1);
}

Vector Model::spherical(const Real & r)
{
	Real theta = acos(frand(-1.0f, 1.0f));
	Real phi = frand(0.0f, 2.0f * PI);
	return Vector(r * sin(theta) * cos(phi), r * sin(theta) * sin(phi), r * cos(theta));
}

Real Model::assign(const Real & mass, ParticleSign mass_sign)
{
	if(mass_sign == Positive)
	{
		return mass;
	}
	else if(mass_sign == Negative)
	{
		return -mass;
	}
	else // if(mass_sign == Mixture)
	{
		static Real flip_sign = 1.0f;
		flip_sign *= -1.0f;
		return mass * flip_sign;
	}
}

Plummer::Plummer(int number, ParticleSign mass_sign, int seed) : Model()
{
	assert(bodies_size == 0);
	if(seed) srand(seed);
	bodies = new Body[number];
	bodies_size = number;
	Real mass = (Real) 1.0 / (Real) number;
	Real point1 = (Real) 1.0f / (Real) 10.0f;
	Real scalefactor = 16.0f / (3.0f * PI);
	Real cumulative_mass_min = 0.0f;
	Real cumulative_mass_max = mass;
	for(int b = 0; b < bodies_size; b++)
	{
		Real cumulative_mass = frand(cumulative_mass_min, cumulative_mass_max);
		cumulative_mass_min = cumulative_mass_max;
		cumulative_mass_max += mass;
		Real x = 0.0f;
		Real y = point1;
		while(y > sqr(x) * pow(((Real) 1.0f - sqr(x)), (Real) 3.5f))
		{
			x = frand(0.0f, 1.0f);
			y = frand(0.0f, point1);
		}
		Real distance = 1.0f / sqrt(pow(cumulative_mass, (Real) -2.0f / (Real) 3.0f) - 1.0f);
		Real velocity = x * sqrt((Real) 2.0f) * pow((Real) (1.0f + sqr(distance)), (Real) -0.25f);
		Body * body = &bodies[b];
		body->pos = spherical(distance) / scalefactor;
		body->vel = spherical(velocity) * sqrt(scalefactor);
		body->mass = assign(mass, mass_sign);
	}
	if(mass_sign != Mixture) adjust_center();
}

Uniform::Uniform(int number, ParticleSign mass_sign, int seed) : Model()
{
	assert(bodies_size == 0);
	if(seed) srand(seed);
	bodies = new Body[number];
	bodies_size = number;
	Real mass = (Real) 1.0 / (Real) number;
	for(int b = 0; b < bodies_size; b++)
	{
		Real x = frand(-1.0f, 1.0f);
		Real y = frand(-1.0f, 1.0f);
		Real z = frand(-1.0f, 1.0f);
		while(sqr(x) + sqr(y) + sqr(z) > 1.0f)
		{
			x = frand(-1.0f, 1.0f);
			y = frand(-1.0f, 1.0f);
			z = frand(-1.0f, 1.0f);
		}
		Body * body = &bodies[b];
		body->pos = Vector(x, y, z);
		body->vel = 0.0f;
		body->mass = assign(mass, mass_sign);
	}
	if(mass_sign != Mixture) adjust_center();
}

} // namespace GravSim
