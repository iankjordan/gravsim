/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "acs.h"

namespace GravSim {

bool serial = true;
bool logging = true;
bool detailed_logging = false;

int max_batch_size = 2000;
int min_integrator_parallel_size = 64;
int min_brute_force_parallel_size = 8;
int min_barnes_hut_step_size = 16;
int min_stadel_step_size = 32;
int min_ridler_step_size = 8;
int min_dehnen_step_size = 32;
int min_dehnen_stadel_step_size = 64;
int min_dehnen_ridler_step_size = 256;
int min_body_sort_step_size = 512;

int num_procs = omp_get_num_procs();
int max_threads = omp_get_max_threads();
int num_threads = omp_get_num_threads();

Char comma(',');
Char lbracket('(');
Char rbracket(')');

Vector zero_vector(0.0f);

void Point::operator = (const Real & r)
{
	pos = r;
	vel = r;
	acc = r;
#if (DERIVATIVE >= 3)
	jerk = r;
#if (DERIVATIVE >= 4)
	snap = r;
#if (DERIVATIVE >= 5)
	crackle = r;
#if (DERIVATIVE >= 6)
	pop = r;
#if (DERIVATIVE >= 7)
	nanda = r;
#endif
#endif
#endif
#endif
#endif
}

Point Point::operator + (const Point & p) const
{
	Point result;
	result.pos = pos + p.pos;
	result.vel = vel + p.vel;
	result.acc = acc + p.acc;
#if (DERIVATIVE >= 3)
	result.jerk = jerk + p.jerk;
#if (DERIVATIVE >= 4)
	result.snap = snap + p.snap;
#if (DERIVATIVE >= 5)
	result.crackle = crackle + p.crackle;
#if (DERIVATIVE >= 6)
	result.pop = pop + p.pop;
#if (DERIVATIVE >= 7)
	result.nanda = nanda + p.nanda;
#endif
#endif
#endif
#endif
#endif
	return result;
}

Point Point::operator - (const Point & p) const
{
	Point result;
	result.pos = pos - p.pos;
	result.vel = vel - p.vel;
	result.acc = acc - p.acc;
#if (DERIVATIVE >= 3)
	result.jerk = jerk - p.jerk;
#if (DERIVATIVE >= 4)
	result.snap = snap - p.snap;
#if (DERIVATIVE >= 5)
	result.crackle = crackle - p.crackle;
#if (DERIVATIVE >= 6)
	result.pop = pop - p.pop;
#if (DERIVATIVE >= 7)
	result.nanda = nanda - p.nanda;
#endif
#endif
#endif
#endif
#endif
	return result;
}

Point Point::operator * (const Real & r) const
{
	Point result;
	result.pos = pos * r;
	result.vel = vel * r;
	result.acc = acc * r;
#if (DERIVATIVE >= 3)
	result.jerk = jerk * r;
#if (DERIVATIVE >= 4)
	result.snap = snap * r;
#if (DERIVATIVE >= 5)
	result.crackle = crackle * r;
#if (DERIVATIVE >= 6)
	result.pop = pop * r;
#if (DERIVATIVE >= 7)
	result.nanda = nanda * r;
#endif
#endif
#endif
#endif
#endif
	return result;
}

Point Point::operator / (const Real & r) const
{
	Point result;
	result.pos = pos / r;
	result.vel = vel / r;
	result.acc = acc / r;
#if (DERIVATIVE >= 3)
	result.jerk = jerk / r;
#if (DERIVATIVE >= 4)
	result.snap = snap / r;
#if (DERIVATIVE >= 5)
	result.crackle = crackle / r;
#if (DERIVATIVE >= 6)
	result.pop = pop / r;
#if (DERIVATIVE >= 7)
	result.nanda = nanda / r;
#endif
#endif
#endif
#endif
#endif
	return result;
}

ostream & operator << (ostream & os, const Point & p)
{
	os << "pos = " << p.pos << "\n";
	os << "vel = " << p.vel << "\n";
	os << "acc = " << p.acc << "\n";
#if (DERIVATIVE >= 3)
	os << "jerk = " << p.jerk << "\n";
#if (DERIVATIVE >= 4)
	os << "snap = " << p.snap << "\n";
#if (DERIVATIVE >= 5)
	os << "crackle = " << p.crackle << "\n";
#if (DERIVATIVE >= 6)
	os << "pop = " << p.pop << "\n";
#if (DERIVATIVE >= 7)
	os << "nanda = " << p.nanda << "\n";
#endif
#endif
#endif
#endif
#endif
	return os;
}

void AccEtc::zero()
{
	acc = 0.0f;
#if (DERIVATIVE >= 3)
	jerk = 0.0f;
#if (DERIVATIVE >= 4)
	snap = 0.0f;
#if (DERIVATIVE >= 5)
	crackle = 0.0f;
#if (DERIVATIVE >= 6)
	pop = 0.0f;
#if (DERIVATIVE >= 7)
	nanda = 0.0f;
#endif
#endif
#endif
#endif
#endif
}

void AccEtc::apply(const AccEtc & field)
{
	acc = field.acc;
#if (DERIVATIVE >= 3)
	jerk = field.jerk;
#if (DERIVATIVE >= 4)
	snap = field.snap;
#if (DERIVATIVE >= 5)
	crackle = field.crackle;
#if (DERIVATIVE >= 6)
	pop = field.pop;
#if (DERIVATIVE >= 7)
	nanda = field.nanda;
#endif
#endif
#endif
#endif
#endif
}

void AccEtc::add(const AccEtc & field)
{
	acc += field.acc;
#if (DERIVATIVE >= 3)
	jerk += field.jerk;
#if (DERIVATIVE >= 4)
	snap += field.snap;
#if (DERIVATIVE >= 5)
	crackle += field.crackle;
#if (DERIVATIVE >= 6)
	pop += field.pop;
#if (DERIVATIVE >= 7)
	nanda += field.nanda;
#endif
#endif
#endif
#endif
#endif
}

WorldPoint::WorldPoint(const Real & t)
{
	pos = 0.0f;
	vel = 0.0f;
	acc = 0.0f;
#if (DERIVATIVE >= 3)
	jerk = 0.0f;
#if (DERIVATIVE >= 4)
	snap = 0.0f;
#if (DERIVATIVE >= 5)
	crackle = 0.0f;
#if (DERIVATIVE >= 6)
	pop = 0.0f;
#if (DERIVATIVE >= 7)
	nanda = 0.0f;
#endif
#endif
#endif
#endif
#endif
	time = t;
}

WorldPoint::WorldPoint(const Point & p, const Real & t)
{
	*((Point *) this) = p;
	time = t;
}

WorldPoint::WorldPoint(const WorldPoint & wp)
{
	*this = wp;
}

WorldPoint::WorldPoint(const Vector & p, const Real & t)
{
	pos = p;
	vel = 0.0f;
	acc = 0.0f;
#if (DERIVATIVE >= 3)
	jerk = 0.0f;
#if (DERIVATIVE >= 4)
	snap = 0.0f;
#if (DERIVATIVE >= 5)
	crackle = 0.0f;
#if (DERIVATIVE >= 6)
	pop = 0.0f;
#if (DERIVATIVE >= 7)
	nanda = 0.0f;
#endif
#endif
#endif
#endif
#endif
	time = t;
}

WorldPoint::WorldPoint(const Vector & p, const Vector & v, const Real & t)
{
	pos = p;
	vel = v;
	acc = 0.0f;
#if (DERIVATIVE >= 3)
	jerk = 0.0f;
#if (DERIVATIVE >= 4)
	snap = 0.0f;
#if (DERIVATIVE >= 5)
	crackle = 0.0f;
#if (DERIVATIVE >= 6)
	pop = 0.0f;
#if (DERIVATIVE >= 7)
	nanda = 0.0f;
#endif
#endif
#endif
#endif
#endif
	time = t;
}

} // namespace GravSim
