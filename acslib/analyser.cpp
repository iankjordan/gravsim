/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "acs.h"

namespace GravSim {

Orbit::Orbit(const Body * body, const Particle * partner)
{
	body_id = body->id;
	partner_id = partner->id;
	ref_count = 2;
	tm = body->mass + partner->mass;
	Point & body_cm = *((Point *) body);
	Point & partner_cm = *((Point *) partner);
	cm_0 = WorldPoint((body_cm * body->mass + partner_cm * partner->mass) / tm, body->time);
	cm_ = WorldPoint(Infinity);
	expiry_time_sq = Infinity;
}

bool Orbit::includes(const Body * body) const
{
	return ((body->id == body_id) || (body->id == partner_id));
}

void Orbit::update(const Body * body)
{
	assert(includes(body));
	Point & body_cm = *((Point *) body);
	cm_ = WorldPoint(cm_ + body_cm * (body->mass / tm), cm_.time);
}

void Orbit::free()
{
	ref_count--;
	if(ref_count == 0) delete this;
}

void OrbitalElements::stage1(const Real & arg_gm, const Real & arg_gm2, const Vector & arg_r_, const Vector & arg_v_,
                             const Real & arg_u, const Vector & arg_h_, const Vector & arg_e_, const Real & arg_Ta)
{
	gm = arg_gm;
	gm2 = arg_gm2;
	r_ = arg_r_;
	v_ = arg_v_;
	u = arg_u;
	h_ = arg_h_; h = mag(h_);
	e_ = arg_e_; e = mag(e_);
	Ta = arg_Ta;
}

void OrbitalElements::stage2(const Vector & arg_a_, const Real & arg_a, const Vector & arg_b_, const Real & arg_b,
                             const Real & arg_Ea, const Real & arg_Ma, const Real & arg_Ot)
{
	a_ = arg_a_; a = arg_a;
	b_ = arg_b_; b = arg_b;
	Ea = arg_Ea;
	Ma = arg_Ma;
	Ot = arg_Ot;
}

void OrbitalElements::log_elements() const
{
	if(detailed_logging) {
	cout << "gm = " << gm << "\n";
	cout << "gm2 = " << gm2 << "\n";
	cout << "r_ = " << r_ << "\n";
	cout << "v_ = " << v_ << "\n";
	cout << "u = " << u << "\n";
	cout << "h_ = " << h_ << ", h = " << h << "\n";
	cout << "e_ = " << e_ << ", e = " << e << "\n";
	cout << "a_ = " << a_ << ", a = " << a << "\n";
	cout << "b_ = " << b_ << ", b = " << b << "\n";
	cout << "Ta = " << Ta << "\n";
	cout << "Ea = " << Ea << "\n";
	cout << "Ma = " << Ma << "\n";
	cout << "Ot = " << Ot << "\n" << flush; }
}

Real KeplerOrbit::reduced_mass(const Real & pm, const Real & tm)
{
	return cub(pm) / sqr(tm);
}

Real KeplerOrbit::reduced_mass2(const Real & pm, const Real & tm)
{
	return sqr(pm) / tm;
}

Vector KeplerOrbit::specific_angular_momentum_vector(const Vector & r, const Vector & v)
{
	return cross(r, v);
}

Real KeplerOrbit::specific_orbital_energy(const Real & gm, const Vector & r, const Vector & v)
{
	return (sqr(v) / 2.0f) - (gm / mag(r));
}

Vector KeplerOrbit::eccentricity_vector(const Real & gm, const Vector & r, const Vector & v)
{
	return ((sqr(v) * r - dot(r, v) * v) / gm) - norm(r);
}

Vector KeplerOrbit::semi_major_axis_vector(const Vector & e_, const Real & a)
{
	return a * norm(e_);
}

Vector KeplerOrbit::semi_minor_axis_vector(const Vector & e_, const Vector & h_, const Real & b)
{
	return b * norm(cross(h_, e_));
}

Real KeplerOrbit::true_anomaly(const Vector & r, const Vector & v, const Vector & e_)
{
	Real cos_Ta = dot(e_, r) / (mag(e_) * mag(r));
	Real Ta = (fabs(cos_Ta) < 1) ? acos(cos_Ta) : 0.0;
	if(Ta > PI) Ta -= PI2;
	if(dot(r, v) < 0.0f) Ta = - Ta;
	return Ta;
}

Real KeplerOrbit::orbital_time(const Real & gm, const Real & a, const Real & Ma)
{
	return a * sqrt(a / gm) * Ma;
}

Real KeplerOrbit::orbital_period(const Real & gm, const Real & a)
{
	return orbital_time(gm, a, PI2);
}

Real KeplerOrbit::Ma_from_Ot(const Real & gm, const Real & a, const Real & Ot)
{
	return Ot / (a * sqrt(a / gm));
}

KeplerOrbit * KeplerOrbit::create(const Body * body, const Particle * partner)
{
	Real gm = body->mass + partner->mass;
	Vector r_ = body->pos - partner->pos;
	Vector v_ = body->vel - partner->vel;
	Real u = specific_orbital_energy(gm, r_, v_);
	Vector e_ = eccentricity_vector(gm, r_, v_);
	Real e = mag(e_);
	KeplerOrbit * result;
	if(eq(e, 1.0f, Reasonable))
	{
		if(eq(u, 0.0f, Reasonable))
		{
			if(logging) cout << "Would be Parabolic\n";
			result = 0; //new ParabolicOrbit(body, partner);
		}
		else
		{
			if(logging) cout << "Would be Linear\n";
			result = 0; //new LinearOrbit(body, partner);
		}
	}
	else if(e < 1.0f)
	{
		result = new EllipticOrbit(body, partner);
	}
	else
	{
		result = 0; //new HyperbolicOrbit(body, partner);
	}
	if((result != 0) && (!result->valid()))
	{
		delete result;
		result = 0;
	}
	return result;
}

KeplerOrbit::KeplerOrbit(const Body * body, const Particle * partner) : Orbit(body, partner)
{
	stage1(orbit1, reduced_mass(partner->mass, tm), reduced_mass2(partner->mass, tm), body->pos - cm_0.pos, body->vel - cm_0.vel);
	stage1(orbit2, reduced_mass(body->mass, tm), reduced_mass2(body->mass, tm), partner->pos - cm_0.pos, partner->vel - cm_0.vel);
}

void KeplerOrbit::stage1(OrbitalElements & orbit, const Real & gm, const Real & gm2, const Vector & r, const Vector & v)
{
	Real u = specific_orbital_energy(gm, r, v);
	Vector h_ = specific_angular_momentum_vector(r, v);
	Vector e_ = eccentricity_vector(gm, r, v);
	Real Ta = true_anomaly(r, v, e_);

	orbit.stage1(gm, gm2, r, v, u, h_, e_, Ta);
}

Real EllipticOrbit::semi_major_axis(const Real & gm, const Real & u)
{
	return -gm / (2.0f * u);
}

Real EllipticOrbit::semi_minor_axis(const Real & e, const Real & a)
{
	return a * sqrt(1 - sqr(e));
}

Real EllipticOrbit::eccentric_anomaly(const Real & e, const Real & Ta)
{
	return atan2(sqrt(1.0f - sqr(e)) * sin(Ta), e + cos(Ta));
}

Real EllipticOrbit::mean_anomaly(const Real & e, const Real & Ea)
{
	return Ea - e * sin(Ea);
}

Vector EllipticOrbit::pericentre(const Real & e, const Vector & a_)
{
	return (1.0f - e) * a_;
}

Vector EllipticOrbit::apocentre(const Real & e, const Vector & a_)
{
	return (-1.0f - e) * a_;
}

Vector EllipticOrbit::pos_from_Ea(const Real & e, const Vector & a_, const Vector & b_, const Real & Ea)
{
	return (cos(Ea) - e) * a_ + sin(Ea) * b_;
}

Vector EllipticOrbit::vel_from_Ea(const Real & e, const Vector & a_, const Vector & b_, const Real & Ea, const Real & h)
{
	Real dEa = h / (mag(a_) * mag(b_) * (1.0f - e * cos(Ea)));
	return - dEa * sin(Ea) * a_ + dEa * cos(Ea) * b_;
}

Real EllipticOrbit::Ea_from_Ma(const Real & e, const Real & Ma)
{
	Real Ea = Ma, ratio = 1.0f;
	int newton_raphson = 0;
	while(newton_raphson < 100)
	{
		Real Ea2 = Ea - e * sin(Ea) - Ma;
		Real dEa2 = 1.0f - e * cos(Ea);
		if(dEa2 <= Epsilon) break;
		ratio = Ea2 / dEa2;
		if(fabs(ratio) <= Epsilon) break;
		Ea = Ea - ratio;
		newton_raphson++;
	}
	return Ea;
}

void EllipticOrbit::predict(WorldPoint & wp, OrbitalElements & orbit)
{
	assert(cm_.time == wp.time);
	orbit.log_elements();

	Real dt = cm_.time - cm_0.time;
	Real Ma = Ma_from_Ot(orbit.gm, orbit.a, orbit.Ot + dt);
	Real Ea = Ea_from_Ma(orbit.e, Ma);
	wp.pos = pos_from_Ea(orbit.e, orbit.a_, orbit.b_, Ea);
	wp.vel = vel_from_Ea(orbit.e, orbit.a_, orbit.b_, Ea, orbit.h);
	Real r = mag(wp.pos);
	wp.acc = - orbit.gm * wp.pos / cub(r);
	wp.jerk = - 3.0f * orbit.gm * wp.vel / frth(r);
	wp = WorldPoint(wp + cm_, wp.time);
}

void EllipticOrbit::stage2(OrbitalElements & orbit)
{
	Real & gm = orbit.gm, & u = orbit.u, & e = orbit.e, &Ta = orbit.Ta;
	Vector & e_ = orbit.e_, & h_ = orbit.h_;

	Real a = semi_major_axis(gm, u); 
	Real b = semi_minor_axis(e, a); 
	Vector a_ = semi_major_axis_vector(e_, a);
	Vector b_ = semi_minor_axis_vector(e_, h_, b);
	Real Ea = eccentric_anomaly(e, Ta);
	Real Ma = mean_anomaly(e, Ea);
	Real Ot = orbital_time(gm, a, Ma);

	orbit.stage2(a_, a, b_, b, Ea, Ma, Ot);
}

void EllipticOrbit::test(OrbitalElements & orbit)
{
	Real & e = orbit.e, & h = orbit.h, &Ea = orbit.Ea;
	Vector & a_ = orbit.a_, & b_ = orbit.b_;

	Vector r_ = pos_from_Ea(e, a_, b_, Ea);
	Vector v_ = vel_from_Ea(e, a_, b_, Ea, h);

	if(detailed_logging) {
	cout << "r_       = " << r_ << "\n";
	cout << "orbit.r_ = " << orbit.r_ << "\n";
	cout << "v_       = " << v_ << "\n";
	cout << "orbit.v_ = " << orbit.v_ << "\n\n" << flush; }

	assert(eq(r_, orbit.r_, Reasonable));
	assert(eq(v_, orbit.v_, Reasonable));
}

EllipticOrbit::EllipticOrbit(const Body * body, const Particle * partner) : KeplerOrbit(body, partner)
{
	stage2(orbit1);
	stage2(orbit2);
}

bool EllipticOrbit::valid() const
{
	return true;
}

void EllipticOrbit::predict(Body * body, Integrator * integrator)
{
	assert(includes(body));
	if(logging) cout << "EllipticOrbit::predict(" << body->id << ", " << ((body->id == body_id) ? partner_id : body_id) << ")\n" << flush;
	if(cm_.time != body->time)
	{
		cm_.time = body->time;
		integrator->predict(cm_, cm_0);
	}
	if(body->id == body_id)
	{
		predict(*body, orbit1);
	}
	else
	{
		predict(*body, orbit2);
	}
}

void EllipticOrbit::correct(Body * body, PredictorCorrector * integrator)
{
	assert(includes(body));
	// FIXME: Need update_cm type mechanism
}

Real HyperbolicOrbit::semi_major_axis(const Real & gm, const Real & u)
{
	return gm / (2.0f * u);
}
	
Real HyperbolicOrbit::semi_minor_axis(const Real & e, const Real & a)
{
	return a * sqrt(sqr(e) - 1.0f);
}

Real HyperbolicOrbit::true_anomaly_max(const Real & e)
{
	return acos(-1.0f / e);
}

Real HyperbolicOrbit::true_anomaly_min(const Real & e)
{
	return -acos(-1.0f / e);
}

Real HyperbolicOrbit::eccentric_anomaly(const Real & e, const Real & Ta)
{
	return 2.0f * atanh(tan(Ta / 2.0f) * sqrt((e - 1.0f) / (e + 1.0f)));
}

Real HyperbolicOrbit::mean_anomaly(const Real & e, const Real & Ea)
{
	return e * sinh(Ea) - Ea;
}

Vector HyperbolicOrbit::pericentre(const Real & e, const Vector & a_)
{
	return (e - 1.0f) * a_;
}

Vector HyperbolicOrbit::pos_from_Ea(const Real & e, const Vector & a_, const Vector & b_, const Real & Ea)
{
	return (e - cosh(Ea)) * a_ + sinh(Ea) * b_;
}

Vector HyperbolicOrbit::vel_from_Ea(const Real & e, const Vector & a_, const Vector & b_, const Real & Ea, const Real & h)
{
	Real dEa = h / (mag(a_) * mag(b_) * (e * cosh(Ea) - 1.0f));
	return - dEa * sinh(Ea) * a_ + dEa * cosh(Ea) * b_;
}

Real HyperbolicOrbit::Ea_from_Ma(const Real & e, const Real & Ma)
{
	Real Ea = Ma, ratio = 1.0f;
//	Real Ea_min = eccentric_anomaly(e, true_anomaly_min(e) + Epsilon);
//	Real Ea_max = eccentric_anomaly(e, true_anomaly_max(e) + Epsilon);
//	Ea = max(Ea, Ea_min);
//	Ea = min(Ea, Ea_max);
	int newton_raphson = 0;
	while(newton_raphson < 100)
	{
		Real Ea2 = e * sinh(Ea) - Ea - Ma;
		Real dEa2 = e * cosh(Ea) - 1.0f;
		if(dEa2 <= Epsilon) break;
		ratio = Ea2 / dEa2;
		if(fabs(ratio) <= Epsilon) break;
		Ea = Ea - ratio;
		newton_raphson++;
	}
	cout << "Ea = " << Ea << "\n";
	return Ea;
}

bool HyperbolicOrbit::within_analytic_range(const Real & e, const Real & Ta)
{
	Real Ta_factor = 0.999f;
	return (Ta < (true_anomaly_max(e) * Ta_factor) && (Ta > true_anomaly_min(e) * Ta_factor));
}

void HyperbolicOrbit::log_range(const Real & e)
{
	if(detailed_logging) {
	cout << "Ta_max = " << true_anomaly_max(e) << "\n";
	cout << "Ta_min = " << true_anomaly_min(e) << "\n"; }
}

void HyperbolicOrbit::predict(WorldPoint & wp, OrbitalElements & orbit)
{
	assert(cm_.time == wp.time);
	orbit.log_elements();

	Real dt = cm_.time - cm_0.time;
	Real Ma = Ma_from_Ot(orbit.gm, orbit.a, orbit.Ot + dt);
	Real Ea = Ea_from_Ma(orbit.e, Ma);
	wp.pos = pos_from_Ea(orbit.e, orbit.a_, orbit.b_, Ea);
	wp.vel = vel_from_Ea(orbit.e, orbit.a_, orbit.b_, Ea, orbit.h);
	Real r = mag(wp.pos);
	wp.acc = - orbit.gm * wp.pos / cub(r);
	wp.jerk = - 3.0f * orbit.gm * wp.vel / frth(r);
	wp = WorldPoint(wp + cm_, wp.time);
}

void HyperbolicOrbit::stage2(OrbitalElements & orbit)
{
	Real & gm = orbit.gm, & u = orbit.u, & e = orbit.e, &Ta = orbit.Ta;
	Vector & e_ = orbit.e_, & h_ = orbit.h_;

	Real a = semi_major_axis(gm, u); 
	Real b = semi_minor_axis(e, a); 
	Vector a_ = semi_major_axis_vector(e_, a);
	Vector b_ = semi_minor_axis_vector(e_, h_, b);
	Real Ea = eccentric_anomaly(e, Ta);
	Real Ma = mean_anomaly(e, Ea);
	Real Ot = orbital_time(gm, a, Ma);

	orbit.stage2(a_, a, b_, b, Ea, Ma, Ot);
}

void HyperbolicOrbit::test(OrbitalElements & orbit)
{
	Real & e = orbit.e, & h = orbit.h, & Ta = orbit.Ta, &Ea = orbit.Ea;
	Vector & a_ = orbit.a_, & b_ = orbit.b_;

	Vector r_ = pos_from_Ea(e, a_, b_, Ea);
	Vector v_ = vel_from_Ea(e, a_, b_, Ea, h);

	if(detailed_logging) {
	cout << "r_       = " << r_ << "\n";
	cout << "orbit.r_ = " << orbit.r_ << "\n";
	cout << "v_       = " << v_ << "\n";
	cout << "orbit.v_ = " << orbit.v_ << "\n\n" << flush; }

	assert(eq(r_, orbit.r_, Reasonable));
	assert(eq(v_, orbit.v_, Reasonable));
}

HyperbolicOrbit::HyperbolicOrbit(const Body * body, const Particle * partner) : KeplerOrbit(body, partner)
{
	stage2(orbit1);
	stage2(orbit2);
}

bool HyperbolicOrbit::valid() const
{
	return ((orbit1.e < 10000.0f) && within_analytic_range(orbit1.e, orbit1.Ta));
}

void HyperbolicOrbit::predict(Body * body, Integrator * integrator)
{
	assert(includes(body));
	if(logging) cout << "HyperbolicOrbit::predict(" << body->id << ", " << ((body->id == body_id) ? partner_id : body_id) << ")\n" << flush;
	if(cm_.time != body->time)
	{
		cm_.time = body->time;
		integrator->predict(cm_, cm_0);
	}
	if(body->id == body_id)
	{
		predict(*body, orbit1);
	}
	else
	{
		predict(*body, orbit2);
	}
}

void HyperbolicOrbit::correct(Body * body, PredictorCorrector * integrator)
{
	assert(includes(body));
	// FIXME: Need update_cm type mechanism
}

Vector ParabolicOrbit::pos_from_Ta(const Vector & x_, const Vector & y_, const Real & Ta, const Real & hh_m)
{
	Real r = hh_m / (1.0f + cos(Ta));
	return r * cos(Ta) * x_ + r * sin(Ta) * y_;
}

Vector ParabolicOrbit::vel_from_Ta(const Vector & x_, const Vector & y_, const Real & Ta, const Real & gm_h)
{
	return - gm_h * sin(Ta) * x_ + gm_h * (1.0f + cos(Ta)) * y_;
}

ParabolicOrbit::ParabolicOrbit(const Body * body, const Particle * partner) : KeplerOrbit(body, partner)
{
	stage2(orbit1);
	stage2(orbit2);

	if(logging) cout << "Parabolic (" << body->id << ", " << partner->id << ")\n" << flush;

//	test(orbit1);
//	test(orbit2);
}

void ParabolicOrbit::stage2(OrbitalElements & orbit)
{
	Real & gm = orbit.gm, & h = orbit.h, &Ta = orbit.Ta;
	Vector & e_ = orbit.e_, & h_ = orbit.h_;

	Vector x_ = norm(e_);
	Vector y_ = norm(cross(h_, e_));
//	Real Ot = orbital_time(gm, a, Ta);
//	FIXME: need Barker's equation for Parabolic Mean Anomaly equivalent

	orbit.stage2(x_, 1.0f, y_, 1.0f, Ta, 0.0f, 0.0f);
}

void ParabolicOrbit::test(OrbitalElements & orbit)
{
	orbit.log_elements();
	Real & gm = orbit.gm, & h = orbit.h, & Ta = orbit.Ta;
	Vector & x_ = orbit.a_, & y_ = orbit.b_;

	Real hh_gm = sqr(h) / gm;
	Real gm_h = gm / h;
	Vector r_ = pos_from_Ta(x_, y_, Ta, hh_gm);
	Vector v_ = vel_from_Ta(x_, y_, Ta, gm_h);

	if(detailed_logging) {
	cout << "r_       = " << r_ << "\n";
	cout << "orbit.r_ = " << orbit.r_ << "\n";
	cout << "v_       = " << v_ << "\n";
	cout << "orbit.v_ = " << orbit.v_ << "\n\n" << flush; }

	assert(eq(r_, orbit.r_, Reasonable));
	assert(eq(v_, orbit.v_, Reasonable));
}

bool ParabolicOrbit::valid() const
{
	return false;
}

void ParabolicOrbit::predict(Body * body, Integrator * integrator)
{
	assert(includes(body));
	cout << "Not implemented\n";
}

void ParabolicOrbit::correct(Body * body, PredictorCorrector * integrator)
{
	assert(includes(body));
	cout << "Not implemented\n";
}

LinearOrbit::LinearOrbit(const Body * body, const Particle * partner) : KeplerOrbit(body, partner)
{
//	stage2(orbit1);
//	stage2(orbit2);

	if(logging) cout << "Linear (" << body->id << ", " << partner->id << ")\n" << flush;

	orbit1.log_elements();
	orbit2.log_elements();

//	test(orbit1);
//	test(orbit2);
}

bool LinearOrbit::valid() const
{
	return false;
}

void LinearOrbit::predict(Body * body, Integrator * integrator)
{
	assert(includes(body));
	cout << "Not implemented\n";
}

void LinearOrbit::correct(Body * body, PredictorCorrector * integrator)
{
	assert(includes(body));
	cout << "Not implemented\n";
}

Real Analyser::hill_sphere_radius(const Body * body, const Particle * partner) const
{
	Vector d = partner->pos - body->pos;
	Real r = mag(d);
	Vector partner_acc = (partner->mass / cub(r)) * d;
	Vector non_partner_acc = body->field.acc - partner_acc;
	return sqrt(partner->mass / mag(non_partner_acc));
}

bool Analyser::within_hill_sphere(const Body * body, const Particle * partner) const
{
	Vector d = partner->pos - body->pos;
	Real r = mag(d);
	Vector partner_acc = (partner->mass / cub(r)) * d;
	Vector non_partner_acc = body->field.acc - partner_acc;
	return ((sqr(partner_acc) * hill_sphere_factor_sq) > sqr(non_partner_acc));
}

Analyser::Analyser(const Real & hill_sphere_factor)
{
	hill_sphere_factor_sq = sqr(hill_sphere_factor);
}

Orbit * Analyser::analytic_orbit(const Body * body, const Particle * partner)
{
//	if(detailed_logging) cout << "Analyser::analytic_orbit()\n" << flush;

	if((body->mass > 0.0f) &&
	   (partner->mass > 0.0f) &&
	   (body->time == partner->time) /*&& within_hill_sphere(body, partner)*/)
	{
		return KeplerOrbit::create(body, partner);
	}
	else
	{
		return 0;
	}
}

} // namespace GravSim
