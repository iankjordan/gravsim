/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRAVSIM_PLUMMER_H
#define GRAVSIM_PLUMMER_H

#include "model.h"

class Plummer : public Model
{
private:

	Real frand(const Real & low, const Real & high);
	Vector spherical(const Real & r);
	void generate(int number);

public:

	Plummer(int number, int seed = 0);
};

#endif
