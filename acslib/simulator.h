/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRAVSIM_SIMULATOR_H
#define GRAVSIM_SIMULATOR_H

#include "model.h"
#include "analyser.h"
#include "integrator.h"
#include "divider.h"

class Simulator : public Model
{
protected:

	Analyser * analyser;
	Integrator * integrator;
	Divider * divider;
	bool constant_timesteps;
	bool variable_timesteps;
	bool shared_timesteps;
	bool individual_timesteps;
	bool block_timesteps;
	int level;
	Real constant_timestep_factor;
	Real variable_timestep_factor;
	Real constant_target_time;
	Real sqrt2;
	Real softening;
	bool started;
	Real initial_energy;
	clock_t initial_cpu;
	int logpoint;
	Real time;
	Real next_time;
	int step_size;

	void log_config();
	void log_energy();
	void log_performance();
	void log_stats_plus();

	bool same_time() const;
	bool same_next_time() const;
	void choose_constant(const Real & target_time);
	void choose_shared(const Real & target_time);
	void choose_individual(const Real & target_time);
	void begin_step(const Real & target_time);
	void end_step();
	void take_snapshot();
	Body ** active_buffer();
	Real block_or_continuous_time(const Real & target_time) const;
	void calc_gravity_brute_force(const GravArgs & args);
	void calc_gravity_brute_force_serial(const GravArgs & args);
	void calc_gravity_brute_force_parallel(const GravArgs & args);
	void calc_gravity_brute_force_remainder(const GravArgs & args);

public:

	static void (*commit_hook)(const Simulator * simulator);

	Real get_time() const { return time; }
	Real get_next_time() const { return next_time; }
	int get_step_size() const { return step_size; }
	int get_working_size() const { return (sort_order == SortIsActive) ? step_size : bodies_size; }
	Integrator * get_integrator() const { return integrator; }
	void apply_gravity(const GravArgs & args);
	const Particle * find_particle(unsigned particle_id);

	Simulator(Integrator * integrator, Divider * divider,
		bool constant_timesteps, bool shared_timesteps, bool block_timesteps,
		int level, const Real & softening, bool analyser, const Real & hill_sphere_factor);
	~Simulator();
	void run(const Real & sim_time, const Real & log_interval);
	void startup();
	void evolve(const Real & interval_time);
	void calc_gravity(const GravArgs & args);
	Real kinetic_energy() const;
	Real potential_energy() const;
	Real minimum_time() const;
	Real minimum_next_time() const;
	Real minimum_collision_time() const;
	Real root_mean_square_collision_time() const;
};

#endif
