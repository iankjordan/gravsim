/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRAVSIM_ACS_H
#define GRAVSIM_ACS_H

#include <assert.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <deque>
#include <map>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>

#ifdef FASTSIM
#define FLOAT
#endif

#ifdef GRAVSIM
#define DOUBLE
#endif

#ifdef FINESIM
#include <qd/dd_real.h>
#define DDREAL
//#include <qd/qd_real.h>
//#define QDREAL
//#include <arprec/mp_real.h>
//#define MPREAL
#endif

// OMP allows you to switch omp parellelisation on or off

//#define OMP

#ifdef OMP
#include <omp.h>
#else
inline int omp_in_parallel() { return 0; }
inline int omp_get_thread_num() { return 0; }
inline int omp_get_num_procs() { return 1; }
inline int omp_get_num_threads() { return 1; }
inline int omp_get_max_threads() { return 1; }
#endif

// PROFILER shows you roughly where the time is going in the major time-consuming functions

//#define PROFILER

#ifdef PROFILER
#include "Shiny.h"
#else
#define PROFILE_FUNC()
#define PROFILER_UPDATE()
#define PROFILER_OUTPUT()
#endif

// DETAILED_PROFILER will profile more functions but with a significant loss of performance

//#define DETAILED_PROFILER

#ifdef DETAILED_PROFILER
#define DETAILED_PROFILE_FUNC() PROFILE_FUNC()
#else
#define DETAILED_PROFILE_FUNC()
#endif

namespace GravSim
{
#	include "basic.h"
#	include "body.h"
#	include "analyser.h"
#	include "integrator.h"
#	include "model.h"
#	include "composite.h"
#	include "divider.h"
#	include "interactor.h"
#	include "simulator.h"
};

#endif
