/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRAVSIM_INTEGRATOR_H
#define GRAVSIM_INTEGRATOR_H

#include "basic.h"

#define integrator_for(INTEGRATOR, PASS) pass((void (Integrator::*)(Body *))&INTEGRATOR::PASS)

class Integrator
{
protected:

	static Real div2, div3, div4, div6, div8, div12, div24, div48;
	static Real div120, div144, div720, div5040, div40320, div362880;
	static Real x2div3, x2div9, sqrt2;

	int constant_steps_per_orbit;
	int variable_steps_per_orbit;
	Simulator * simulator;
	Body * bodies;
	int bodies_size;

	virtual void calc_gravity(bool ctsq = false);
	void pass(void (Integrator::*pass_n)(Body * body));
	void pass_0(Body * body);

public:

	static Real closest_approach_so_far;

	static Integrator * create(const char * integrator_arg);
	static void check_closest_approach(const Real & r);

	Integrator();
	Real constant_timestep_factor(int level);
	Real variable_timestep_factor(int level);
	virtual void startup(Simulator * simulator);
	virtual void step(bool ctsq) = 0;
	virtual void predict(WorldPoint & wp, const WorldPoint & p0) = 0;
	virtual void interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1) = 0;
	virtual bool one_shot() const;
	virtual const char * name() const = 0;
};

class PredictorCorrector : public Integrator
{
protected:

	void pass_1(Body * body);
	void pass_2(Body * body);

public:

	PredictorCorrector();
	virtual void step(bool ctsq);
	virtual void correct(WorldPoint & wp, const WorldPoint & p0) = 0;
	virtual bool one_shot() const;
};

class ForwardEuler : public PredictorCorrector
{
public:

	ForwardEuler();
	void predict(WorldPoint & wp, const WorldPoint & p0);
	void correct(WorldPoint & wp, const WorldPoint & p0);
	void interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1);
	const char * name() const;
};

class ForwardPlus : public PredictorCorrector
{
public:

	ForwardPlus();
	void predict(WorldPoint & wp, const WorldPoint & p0);
	void correct(WorldPoint & wp, const WorldPoint & p0);
	void interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1);
	const char * name() const;
};

class ProtoHermite : public PredictorCorrector
{
public:

	ProtoHermite();
	void predict(WorldPoint & wp, const WorldPoint & p0);
	void correct(WorldPoint & wp, const WorldPoint & p0);
	void interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1);
	const char * name() const;
};

class LeapFrog : public PredictorCorrector
{
public:

	LeapFrog();
	void predict(WorldPoint & wp, const WorldPoint & p0);
	void correct(WorldPoint & wp, const WorldPoint & p0);
	void interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1);
	const char * name() const;
};

#ifdef MULTISTEP

class MultiStep : public PredictorCorrector
{
protected:

	void make_taylor(Vector * a, const WorldLine & wl, int order);
	Vector taylor_increment(const Vector * a, const Real & dt, int number, int order);

public:

	MultiStep();
	void predict(WorldPoint & wp, const WorldPoint & p0);
	void correct(WorldLine & wl, const WorldPoint & p0);
	void interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1);
	const char * name() const;
};

#endif

class RungeKutta2n : public Integrator
{
protected:

	Real dt, dtdiv2;

	void pass_1(Body * body);
	void pass_2(Body * body);
	void pass_3(Body * body);

public:

	RungeKutta2n();
	void step(bool ctsq);
	void predict(WorldPoint & wp, const WorldPoint & p0);
	void interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1);
	const char * name() const;
};

#if (DERIVATIVE >= 3)

class RungeKutta3 : public Integrator
{
protected:

	Real dt, dtdiv2, dtsq, dtdiv6;

	void pass_1(Body * body);
	void pass_2(Body * body);
	void pass_3(Body * body);

public:

	RungeKutta3();
	void startup(Simulator * simulator);
	void step(bool ctsq);
	void estimate_jerk(WorldPoint & wp, const WorldPoint & p0);
	void predict(WorldPoint & wp, const WorldPoint & p0);
	void interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1);
	const char * name() const;
};

class RungeKutta4 : public RungeKutta3
{
protected:

	WorldPoint * k2;

	void pass_1(Body * body);
	void pass_2(Body * body);
	void pass_3(Body * body);
	void pass_4(Body * body);

public:

	RungeKutta4();
	void step(bool ctsq);
	const char * name() const;
};

class RungeKutta4n : public RungeKutta3
{
protected:

	WorldPoint * k2, * k3;

	void pass_1(Body * body);
	void pass_2(Body * body);
	void pass_3(Body * body);
	void pass_4(Body * body);
	void pass_5(Body * body);

public:

	RungeKutta4n();
	void step(bool ctsq);
	const char * name() const;
};

class ChinChen : public RungeKutta3
{
protected:

	Vector * p1, * a2;

	void calc_gforce();
	void calc_ctsq();
	void pass_1(Body * body);
	void pass_2(Body * body);
	void pass_3(Body * body);
	void pass_4(Body * body);

public:

	ChinChen();
	void step(bool ctsq);
	void predict(WorldPoint & wp, const WorldPoint & p0);
	void interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1);
	const char * name() const;
};

class ChinChenOptimised : public ChinChen
{
protected:

	void pass_1(Body * body);
	void pass_2(Body * body);
	void pass_3(Body * body);
	void pass_4(Body * body);

public:

	ChinChenOptimised();
	void step(bool ctsq);
	const char * name() const;
};

class Hermite : public PredictorCorrector
{
protected:

	void calc_gravity(bool ctsq = false);

public:

	Hermite();
	void predict(WorldPoint & wp, const WorldPoint & p0);
	void correct(WorldPoint & wp, const WorldPoint & p0);
	void interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1);
	const char * name() const;
};

#if (DERIVATIVE >= 4)

class Hermite6 : public PredictorCorrector
{
protected:

	void calc_gravity(bool ctsq = false);

public:

	Hermite6();
	void predict(WorldPoint & wp, const WorldPoint & p0);
	void correct(WorldPoint & wp, const WorldPoint & p0);
	void interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1);
	const char * name() const;
};

#if (DERIVATIVE >= 5)

class Hermite8 : public PredictorCorrector
{
protected:

	void calc_gravity(bool ctsq = false);

public:

	Hermite8();
	void predict(WorldPoint & wp, const WorldPoint & p0);
	void correct(WorldPoint & wp, const WorldPoint & p0);
	void interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1);
	const char * name() const;
};

#endif
#endif
#endif

#endif
