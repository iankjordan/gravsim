/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRAVSIM_MODEL_H
#define GRAVSIM_MODEL_H

#include "body.h"

enum SortOrder
{
	SortNone,
	SortId,
	SortTime,
	SortNextTime,
	SortIsActive,
	SortHilbert,
	SortMorton,
	SortDistance,
	SortSpeed
};

enum ParticleSign
{
	Positive,
	Negative,
	Mixture
};

class Model
{
protected:

	Body * bodies;
	int bodies_size;
	SortOrder sort_order;

	void log_stats();

	Integer hilbert(int x, int y, int z, int bits);
	Integer morton(int ix, int keybits);

	Integer hilbert_key(const Vector & pos, const Real & scale, const Real & offset, int keybits);
	Integer morton_key(const Vector & pos, const Real & scale, const Real & offset, int keybits);

	void distance_keys();
	void speed_keys();
	void hilbert_keys();
	void morton_keys();

	Body ** new_body_pointers() const;

	Real frand(const Real & low, const Real & high);
	Vector spherical(const Real & r);
	Real assign(const Real & mass, ParticleSign mass_sign);

public:

	static Model * create(const char * generate_arg, int bodies_arg, const char * mass_sign_arg);

	void sort_bodies_id();
	void sort_bodies_time();
	void sort_bodies_next_time();
	void sort_bodies_is_active();
	void sort_bodies_distance();
	void sort_bodies_speed();
	void sort_bodies_hilbert();
	void sort_bodies_morton();

	Body ** sort_body_pointers_hilbert();
	Body ** sort_body_pointers_morton();

	void adjust_center();
	void rotate(Real rate);
	int get_bodies_size() const { return bodies_size; }
	Body * get_bodies() const { return bodies; }
	SortOrder get_sort_order() const { return sort_order; }
	void render();

	Model();
	virtual ~Model();
	void clear();
	void load(istream & is);
	void load(const char * filename);
	void load_binary(const char * filename);
	void save(ostream & os);
	void save(const char * filename);
	void save_binary(const char * filename);
	Real total_mass();
	Vector centre_of_mass();
	Vector drift_velocity();
	void position_limits(Vector & minpos, Vector & maxpos);
	Real maximum_distance();
	Real half_mass_distance();
	Real root_mean_square_distance();
	Real maximum_speed();
	Real half_mass_speed();
	Real root_mean_square_speed();
	Real crossing_time();

	friend ostream & operator << (ostream & os, Model & model);
	friend istream & operator >> (istream & is, Model & model);
};

class Plummer : public Model
{
public:

	Plummer(int number, ParticleSign mass_sign, int seed = 0);
};

class Uniform : public Model
{
public:

	Uniform(int number, ParticleSign mass_sign, int seed = 0);
};

#endif
