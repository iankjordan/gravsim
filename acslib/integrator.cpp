/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler
	Copyright (C) 2007 Piet Hut
	Copyright (C) 2007 Jun Makino

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "acs.h"

namespace GravSim {

Real Integrator::div2;
Real Integrator::div3;
Real Integrator::div4;
Real Integrator::div6;
Real Integrator::div8;
Real Integrator::div12;
Real Integrator::div24;
Real Integrator::div48;
Real Integrator::div120;
Real Integrator::div144;
Real Integrator::div720;
Real Integrator::div5040;
Real Integrator::div40320;
Real Integrator::div362880;
Real Integrator::x2div3;
Real Integrator::x2div9;
Real Integrator::sqrt2;
Real Integrator::closest_approach_so_far = Infinity;

Integrator * Integrator::create(const char * integrator_arg)
{
	Integrator * integrator;
	if(strcmp(integrator_arg, "forward-euler") == 0)
	{
		integrator = new ForwardEuler();
	}
	else if(strcmp(integrator_arg, "forward-plus") == 0)
	{
		integrator = new ForwardPlus();
	}
	else if(strcmp(integrator_arg, "proto-hermite") == 0)
	{
		integrator = new ProtoHermite();
	}
	else if(strcmp(integrator_arg, "leap-frog") == 0)
	{
		integrator = new LeapFrog();
	}
#ifdef MULTISTEP
	else if(strcmp(integrator_arg, "multi-step") == 0)
	{
		integrator = new MultiStep();
	}
#endif
	else if(strcmp(integrator_arg, "runge-kutta-2n") == 0)
	{
		integrator = new RungeKutta2n();
	}
#if (DERIVATIVE >= 3)
	else if(strcmp(integrator_arg, "runge-kutta-3") == 0)
	{
		integrator = new RungeKutta3();
	}
	else if(strcmp(integrator_arg, "runge-kutta-4") == 0)
	{
		integrator = new RungeKutta4();
	}
	else if(strcmp(integrator_arg, "runge-kutta-4n") == 0)
	{
		integrator = new RungeKutta4n();
	}
	else if(strcmp(integrator_arg, "chin-chen") == 0)
	{
		integrator = new ChinChen();
	}
	else if(strcmp(integrator_arg, "chin-chen-optimised") == 0)
	{
		integrator = new ChinChenOptimised();
	}
	else if(strcmp(integrator_arg, "hermite") == 0)
	{
		integrator = new Hermite();
	}
#if (DERIVATIVE >= 4)
	else if(strcmp(integrator_arg, "hermite-6") == 0)
	{
		integrator = new Hermite6();
	}
#if (DERIVATIVE >= 5)
	else if(strcmp(integrator_arg, "hermite-8") == 0)
	{
		integrator = new Hermite8();
	}
#endif
#endif
#endif
	else
	{
		cerr << "Integrator '" << integrator_arg << "' not recognised\n";
		exit(1);
	}
	return integrator;
}

void Integrator::check_closest_approach(const Real & r)
{
	// omp critical is costly ... so this stat is approximate
	closest_approach_so_far = min(closest_approach_so_far, r);
}

Integrator::Integrator()
{
	constant_steps_per_orbit = 0;
	variable_steps_per_orbit = 0;

	div2 = (Real) 1.0f / (Real) 2.0f;
	div3 = (Real) 1.0f / (Real) 3.0f;
	div4 = (Real) 1.0f / (Real) 4.0f;
	div6 = (Real) 1.0f / (Real) 6.0f;
	div8 = (Real) 1.0f / (Real) 8.0f;
	div12 = (Real) 1.0f / (Real) 12.0f;
	div24 = (Real) 1.0f / (Real) 24.0f;
	div48 = (Real) 1.0f / (Real) 48.0f;
	div120 = (Real) 1.0f / (Real) 120.0f;
	div144 = (Real) 1.0f / (Real) 144.0f;
	div720 = (Real) 1.0f / (Real) 720.0f;
	div5040 = (Real) 1.0f / (Real) 5040.0f;
	div40320 = (Real) 1.0f / (Real) 40320.0f;
	div362880 = (Real) 1.0f / (Real) 362880.0f;
	x2div3 = (Real) 2.0f / (Real) 3.0f;
	x2div9 = (Real) 2.0f / (Real) 9.0f;
	sqrt2 = sqrt((Real) 2.0f);
}

Real Integrator::constant_timestep_factor(int level)
{
	assert(level >= 1);
	assert(level <= 15);
	Real steps = (Real) constant_steps_per_orbit;
	for(int i = 1; i < level; i++) steps *= sqrt2;
	return 1.0f / steps;
}

Real Integrator::variable_timestep_factor(int level)
{
	assert(level >= 1);
	assert(level <= 15);
	Real steps = (Real) variable_steps_per_orbit;
	for(int i = 1; i < level; i++) steps *= sqrt2;
	return 1.0f / steps;
}

void Integrator::calc_gravity(bool ctsq)
{
	DETAILED_PROFILE_FUNC();
	simulator->calc_gravity(GravArgs(true, 2, false, ctsq));
}

void Integrator::pass(void (Integrator::*pass_n)(Body * body))
{
	int working_size = simulator->get_working_size();
	if(serial || (working_size < min_integrator_parallel_size)) // Serial execution
	{
		serial_for(b, 0, working_size)
		{
			Body * body = &bodies[b];
			if(body->is_active)
			{
				(this->*pass_n)(body);
			}
		}
	}
	else // Parallel execution
	{
		int NT = parallel_threads(working_size);
#		pragma omp parallel num_threads(NT)
		parallel_for(b, 0, working_size)
		{
			Body * body = &bodies[b];
			if(body->is_active)
			{
				(this->*pass_n)(body);
			}
		}
	}
}

void Integrator::pass_0(Body * body)
{
	assert(body->is_active);
	body->startup();
}

void Integrator::startup(Simulator * a)
{
	DETAILED_PROFILE_FUNC();
	if(detailed_logging) cout << "Integrator::startup()\n" << flush;

	simulator = a;
	bodies = simulator->get_bodies();
	bodies_size = simulator->get_bodies_size();

	calc_gravity(true);

	integrator_for(Integrator, pass_0);
}

bool Integrator::one_shot() const
{
	return false;
}

PredictorCorrector::PredictorCorrector() : Integrator()
{
}

void PredictorCorrector::pass_1(Body * body)
{
	assert(body->is_active);
	body->begin();
	body->time = body->next_time;
	if(body->orbit != 0)
	{
		body->orbit->predict(body, this);
	}
	else
	{
		predict(*body, body->point_0);
	}
}

void PredictorCorrector::pass_2(Body * body)
{
	assert(body->is_active);
	if(body->orbit != 0)
	{
		body->orbit->correct(body, this);
	}
	else
	{
		correct(*body, body->point_0);
	}
	body->commit();
}

void PredictorCorrector::step(bool ctsq)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 80%)
	if(detailed_logging) cout << "\nPredictorCorrector::step()\n" << flush;

	integrator_for(PredictorCorrector, pass_1);

	calc_gravity(ctsq);

	integrator_for(PredictorCorrector, pass_2);
}

bool PredictorCorrector::one_shot() const
{
	return true;
}

ForwardEuler::ForwardEuler() : PredictorCorrector()
{
	constant_steps_per_orbit = 500000;
	variable_steps_per_orbit = 144000;
}

void ForwardEuler::predict(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time;
	wp.pos = p0.pos + p0.vel * dt;
	wp.vel = p0.vel + p0.acc * dt;
}

void ForwardEuler::correct(WorldPoint & wp, const WorldPoint & p0)
{
	predict(wp, wp);
}

void ForwardEuler::interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1)
{
	predict(wp, p1);
}

const char * ForwardEuler::name() const
{
	return "forward-euler";
}

ForwardPlus::ForwardPlus() : PredictorCorrector()
{
	constant_steps_per_orbit = 250000;
	variable_steps_per_orbit = 72000;
}

void ForwardPlus::predict(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time;
	wp.pos = p0.pos + p0.vel * dt + div2 * p0.acc * sqr(dt);
	wp.vel = p0.vel + p0.acc * dt;
}

void ForwardPlus::correct(WorldPoint & wp, const WorldPoint & p0)
{
}

void ForwardPlus::interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1)
{
	predict(wp, p1);
}

const char * ForwardPlus::name() const
{
	return "forward-plus";
}

ProtoHermite::ProtoHermite() : PredictorCorrector()
{
	constant_steps_per_orbit = 900;
	variable_steps_per_orbit = 400;
}

void ProtoHermite::predict(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time;
	Vector accdt = p0.acc * dt;
	wp.pos = p0.pos + p0.vel * dt + accdt * dt * div2;
	wp.vel = p0.vel + accdt;
}

void ProtoHermite::correct(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time, dtdiv2 = dt * div2;
	wp.vel = p0.vel + (wp.acc + p0.acc) * dtdiv2;
	wp.pos = p0.pos + (wp.vel + p0.vel) * dtdiv2;
}

void ProtoHermite::interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1)
{
	Real dt = wp.time - p1.time, dtsqdiv2 = sqr(dt) * div2;
	Vector jerk = (p0.acc - p1.acc) / (p0.time - p1.time);
	wp.pos = p1.pos + p1.vel * dt + p1.acc * dtsqdiv2;
	wp.vel = p1.vel + p1.acc * dt + jerk * dtsqdiv2;
}

const char * ProtoHermite::name() const
{
	return "proto-hermite";
}

LeapFrog::LeapFrog() : PredictorCorrector()
{
	constant_steps_per_orbit = 500;
	variable_steps_per_orbit = 143;
}

void LeapFrog::predict(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time;
	wp.pos = p0.pos + p0.vel * dt + div2 * p0.acc * sqr(dt);
	wp.vel = p0.vel + p0.acc * dt;
}

void LeapFrog::correct(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time;
	wp.pos = p0.pos + p0.vel * dt + div2 * p0.acc * sqr(dt);
	wp.vel = p0.vel + div2 * (wp.acc + p0.acc) * dt;
}

void LeapFrog::interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1)
{
	Real dt = wp.time - p1.time, dtsqdiv2 = sqr(dt) * div2;
	Vector jerk = (p0.acc - p1.acc) / (p0.time - p1.time);
	wp.pos = p1.pos + p1.vel * dt + p1.acc * dtsqdiv2;
	wp.vel = p1.vel + p1.acc * dt + jerk * dtsqdiv2;
}

const char * LeapFrog::name() const
{
	return "leap-frog";
}

#ifdef MULTISTEP

#define ORDER (DERIVATIVE - 1)

void MultiStep::make_taylor(Vector * a, const WorldLine & wl, int order)
{
	Vector ** d = new Vector *[order];
	d[0] = new Vector[order];
	for(int i = 0; i < order; i++) d[0][i] = wl.worldpoint(i).acc;
	for(int k = 1; k < order; k++)
	{
		d[k] = new Vector[order - k];
		for(int i = 0; i < order - k; i++)
		{
			d[k][i] = (d[k-1][i] - d[k-1][i+1]) / (wl.worldpoint(i).time - wl.worldpoint(i+k).time);
		}
	}
	Real ** c = new Real *[order];
	c[0] = new Real[1];
	c[0][0] = 1.0f;
	for(int k = 1; k < order; k++)
	{
		c[k] = new Real[k + 1];
		c[k][0] = 0.0f;
		for(int i = 1; i < k; i++)
		{
			c[k][i] = c[k-1][i-1] + (wl.time - wl.worldpoint(k-1).time) * c[k-1][i];
		}
		c[k][k] = 1.0f;
	}
	for(int j = 1; j < order; j++)
	{
		a[j] = 0.0f;
		for(int k = j; k < order; k++)
		{
			a[j] += c[k][j] * d[k][0];
		}
		for(int i = 1; i < j; i++)
		{
			a[j] *= (Real) (float) i;
		}
		delete [] c[j];
		delete [] d[j];
	}
	delete [] c;
	delete [] d;
}

Vector MultiStep::taylor_increment(const Vector * a, const Real & dt, int number, int order)
{
	assert(number > 0);
	assert(order <= ORDER);
	Vector result;
	if(number <= order)
	{
		result = a[number - 1];
		if(number < order)
		{
			result += (1.0f / (Real) (float) (number + 1)) * taylor_increment(a, dt, number + 1, order); 

		}
	}
	return result * dt;
}

MultiStep::MultiStep() : PredictorCorrector()
{
	constant_steps_per_orbit = 0;
	variable_steps_per_orbit = 0;
}

void MultiStep::predict(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time;
	wp.pos = p0.pos + taylor_increment(&wp.vel, dt, 1, ORDER);
	wp.vel = p0.vel + taylor_increment(&wp0.acc, dt, 1, ORDER - 1);
}

void MultiStep::correct(WorldLine & wl, const WorldPoint & p0)
{
	int order = min(wl.length(), ORDER);
	make_taylor(&wl.acc, wl, order);
	wl.vel = p0.vel - taylor_increment(&wl.acc, -dt, 1, order - 1);
	wl.pos = p0.pos - taylor_increment(&wl.vel, -dt, 1, order);
}

void MultiStep::interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1)
{
	Real dt = wp.time - p1.time;
	if((p1.time + dt < p0.time - dt) || (p1.time - dt > p0.time + dt))
	{
		predict(wp, p1, dt);
	}
	else
	{
		predict(wp, p0, dt + p1.time - p0.time);
	}
}

const char * MultiStep::name() const
{
	return "multi-step";
}

#endif

RungeKutta2n::RungeKutta2n() : Integrator()
{
	constant_steps_per_orbit = 2000;
	variable_steps_per_orbit = 287;
}

void RungeKutta2n::pass_1(Body * body)
{
	assert(body->is_active);
	body->begin();
	WorldPoint & k1 = body->point_0;
	body->pos = k1.pos + k1.vel * dt;
	body->vel = k1.vel + k1.acc * dt;
	body->time = body->next_time;
}

void RungeKutta2n::pass_2(Body * body)
{
	assert(body->is_active);
	WorldPoint & k1 = body->point_0;
	WorldPoint & k2 = *body;
	body->pos = k1.pos + (k1.vel + k2.vel) * dtdiv2;
	body->vel = k1.vel + (k1.acc + k2.acc) * dtdiv2;
}

void RungeKutta2n::pass_3(Body * body)
{
	assert(body->is_active);
	body->commit();
}

void RungeKutta2n::step(bool ctsq)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 80%)
	if(detailed_logging) cout << "\nRungeKutta2n::step()\n" << flush;

	dt = simulator->get_next_time() - simulator->get_time(), dtdiv2 = dt * div2;

	integrator_for(RungeKutta2n, pass_1);

	calc_gravity();

	integrator_for(RungeKutta2n, pass_2);

	calc_gravity(ctsq);

	integrator_for(RungeKutta2n, pass_3);
}

void RungeKutta2n::predict(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time;
	wp.pos = p0.pos + p0.vel * dt;
	wp.vel = p0.vel;
}

void RungeKutta2n::interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1)
{
	Real dt = wp.time - p1.time;
	wp.pos = p1.pos + p1.vel * dt + div2 * p1.acc * sqr(dt);
	wp.vel = p1.vel + p1.acc * dt;
}

const char * RungeKutta2n::name() const
{
	return "runge-kutta-2n";
}

#if (DERIVATIVE >= 3)

RungeKutta3::RungeKutta3() : Integrator()
{
	constant_steps_per_orbit = 666;
	variable_steps_per_orbit = 216;
}

void RungeKutta3::startup(Simulator * a)
{
	simulator = a;
	bodies = simulator->get_bodies();
	bodies_size = simulator->get_bodies_size();
	simulator->calc_gravity(GravArgs(true, 3, false, true));

   	integrator_for(RungeKutta3, pass_0);

}

void RungeKutta3::pass_1(Body * body)
{
	assert(body->is_active);
	body->begin();
	WorldPoint & k1 = body->point_0;
	body->pos = k1.pos + x2div3 * k1.vel * dt + x2div9 * k1.acc * dtsq; 
	body->time += x2div3 * dt;
}

void RungeKutta3::pass_2(Body * body)
{
	assert(body->is_active);
	WorldPoint & k1 = body->point_0;
	WorldPoint & k2 = *body;
	body->pos = k1.pos + k1.vel * dt + (k1.acc + k2.acc) * dtsq * div4;
	body->vel = k1.vel + (k1.acc + 3.0 * k2.acc) * dt * div4;
	body->time = body->next_time;
}

void RungeKutta3::pass_3(Body * body)
{
	assert(body->is_active);
	estimate_jerk(*body, body->point_0);
	body->commit();
}

void RungeKutta3::step(bool ctsq)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 80%)
	if(detailed_logging) cout << "\nRungeKutta3::step()\n" << flush;

	dt = simulator->get_next_time() - simulator->get_time(), dtsq = sqr(dt);

	integrator_for(RungeKutta3, pass_1);

	calc_gravity();

	integrator_for(RungeKutta3, pass_2);

	calc_gravity(ctsq);

	integrator_for(RungeKutta3, pass_3);

}

void RungeKutta3::estimate_jerk(WorldPoint & wp, const WorldPoint & p0)
{
    wp.jerk = (wp.acc - p0.acc) / (wp.time - p0.time);
}

void RungeKutta3::predict(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time, dtsq = sqr(dt), dtcb = dtsq * dt, dtsqdiv2 = dtsq * div2, dtcbdiv6 = dtcb * div6;
	wp.pos = p0.pos + p0.vel * dt + p0.acc * dtsqdiv2 + p0.jerk * dtcbdiv6;
	wp.vel = p0.vel + p0.acc * dt + p0.jerk * dtsqdiv2;
}

void RungeKutta3::interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1)
{
	Real dt = wp.time - p1.time;
	Vector delta = p1.vel - p0.vel;
	Real tau = p0.time - p1.time, tausq = sqr(tau), taucb = tausq * tau;
	Real dtsq = sqr(dt), dtcb = dtsq * dt, dtfr = dtsq * dtsq;
	Real dtsqdiv2 = dtsq * div2, dtcbdiv6 = dtcb * div6, dtfrdiv24 = dtfr * div24;
	Vector jerk = (-6.0f * delta - 2.0f * (2.0f * p1.acc + p0.acc) * tau) / tausq;
    Vector snap = (12.0f * delta + 6.0f * (p1.acc + p0.acc) * tau) / taucb;
	wp.pos = p1.pos + p1.vel * dt + p1.acc * dtsqdiv2 + jerk * dtcbdiv6 + snap * dtfrdiv24;
	wp.vel = p1.vel + p1.acc * dt + jerk * dtsqdiv2 + snap * dtcbdiv6;
}

const char * RungeKutta3::name() const
{
	return "runge-kutta-3";
}

RungeKutta4::RungeKutta4() : RungeKutta3()
{
	constant_steps_per_orbit = 250;
	variable_steps_per_orbit = 75;
}

void RungeKutta4::pass_1(Body * body)
{
	assert(body->is_active);
	body->begin();
	WorldPoint & k1 = body->point_0;
	body->pos = k1.pos + k1.vel * dtdiv2 + k1.acc * dtsq * div8; 
	body->time += dtdiv2;
}

void RungeKutta4::pass_2(Body * body)
{
	assert(body->is_active);
	WorldPoint & k1 = body->point_0;
	k2[body->id] = *((WorldPoint *) body);
	body->pos = k1.pos + k1.vel * dt + k2[body->id].acc * dtsq * div2;
	body->time = body->next_time;
}

void RungeKutta4::pass_3(Body * body)
{
	assert(body->is_active);
	WorldPoint & k1 = body->point_0;
	WorldPoint & k3 = *((WorldPoint *) body);
	body->pos = k1.pos + k1.vel * dt + (k1.acc + 2.0 * k2[body->id].acc) * dtsq * div6;
	body->vel = k1.vel + (k1.acc + 4.0 * k2[body->id].acc + k3.acc) * dt * div6;
}

void RungeKutta4::pass_4(Body * body)
{
	assert(body->is_active);
	estimate_jerk(*body, body->point_0);
	body->commit();
}

void RungeKutta4::step(bool ctsq)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 80%)
	if(detailed_logging) cout << "\nRungeKutta4::step()\n" << flush;

	dt = simulator->get_next_time() - simulator->get_time(), dtdiv2 = dt * div2, dtsq = sqr(dt);
	k2 = new WorldPoint[Body::next_id];

	integrator_for(RungeKutta4, pass_1);

	calc_gravity();

	integrator_for(RungeKutta4, pass_2);

	calc_gravity();

	integrator_for(RungeKutta4, pass_3);

	calc_gravity(ctsq);

	integrator_for(RungeKutta4, pass_4);

	delete k2;
}

const char * RungeKutta4::name() const
{
	return "runge-kutta-4";
}

RungeKutta4n::RungeKutta4n() : RungeKutta3()
{
	constant_steps_per_orbit = 250;
	variable_steps_per_orbit = 75;
}

void RungeKutta4n::pass_1(Body * body)
{
	assert(body->is_active);
	body->begin();
	WorldPoint & k1 = body->point_0;
	body->pos = k1.pos + k1.vel * dtdiv2;
	body->vel = k1.vel + k1.acc * dtdiv2;
	body->time += dtdiv2;
}

void RungeKutta4n::pass_2(Body * body)
{
	assert(body->is_active);
	WorldPoint & k1 = body->point_0;
	k2[body->id] = *((WorldPoint *) body);
	body->pos = k1.pos + k2[body->id].vel * dtdiv2;
	body->vel = k1.vel + k2[body->id].acc * dtdiv2;
}

void RungeKutta4n::pass_3(Body * body)
{
	assert(body->is_active);
	WorldPoint & k1 = body->point_0;
	k3[body->id] = *((WorldPoint *) body);
	body->pos = k1.pos + k3[body->id].vel * dt;
	body->vel = k1.vel + k3[body->id].acc * dt;
	body->time = body->next_time;
}

void RungeKutta4n::pass_4(Body * body)
{
	assert(body->is_active);
	WorldPoint & k1 = body->point_0;
	WorldPoint k4 = *body;
	body->pos = k1.pos + (k1.vel + 2.0f * (k2[body->id].vel + k3[body->id].vel) + k4.vel) * dtdiv6;
	body->vel = k1.vel + (k1.acc + 2.0f * (k2[body->id].acc + k3[body->id].acc) + k4.acc) * dtdiv6;
}

void RungeKutta4n::pass_5(Body * body)
{
	assert(body->is_active);
	estimate_jerk(*body, body->point_0);
	body->commit();
}

void RungeKutta4n::step(bool ctsq)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 80%)
	if(detailed_logging) cout << "\nRungeKutta4n::step()\n" << flush;

	dt = simulator->get_next_time() - simulator->get_time(), dtdiv2 = dt * div2, dtdiv6 = dt * div6;
	k2 = new WorldPoint[Body::next_id];
	k3 = new WorldPoint[Body::next_id];

	integrator_for(RungeKutta4n, pass_1);

	calc_gravity();

	integrator_for(RungeKutta4n, pass_2);

	calc_gravity();

	integrator_for(RungeKutta4n, pass_3);

	calc_gravity();

	integrator_for(RungeKutta4n, pass_4);

	calc_gravity(ctsq);

	integrator_for(RungeKutta4n, pass_5);

	delete k2;
	delete k3;
}

const char * RungeKutta4n::name() const
{
	return "runge-kutta-4n";
}

void ChinChen::calc_gforce()
{
	simulator->calc_gravity(GravArgs(false, 2, true, false));
}

void ChinChen::calc_ctsq()
{
	simulator->calc_gravity(GravArgs(false, 2, false, true));
}

ChinChen::ChinChen() : RungeKutta3()
{
	constant_steps_per_orbit = 666;
	variable_steps_per_orbit = 216;
}

void ChinChen::pass_1(Body * body)
{
	assert(body->is_active);
	body->begin();
	WorldPoint & k1 = body->point_0;
	p1[body->id] = div2 * k1.vel * dt + div12 * k1.acc * dtsq;
	body->pos = k1.pos + p1[body->id];
	body->vel = k1.vel;
	body->time += div2 * dt;
}

void ChinChen::pass_2(Body * body)
{
	assert(body->is_active);
	a2[body->id] = body->acc;
}

void ChinChen::pass_3(Body * body)
{
	assert(body->is_active);
	a2[body->id] += div48 * dtsq * body->acc;
	body->pos += p1[body->id] + div3 * a2[body->id] * dtsq;
	body->acc = a2[body->id];
	body->time = body->next_time;
}

void ChinChen::pass_4(Body * body)
{
	assert(body->is_active);
	Vector & k1 = body->point_0.acc;
	Vector & k3 = body->acc;
	body->vel += div6 * (k1 + 4.0f * a2[body->id] + k3) * dt;
	estimate_jerk(*body, body->point_0);
	body->commit();
}

void ChinChen::step(bool ctsq)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 80%)
	if(detailed_logging) cout << "\nChinChen::step()\n" << flush;

	dt = simulator->get_next_time() - simulator->get_time(), dtsq = sqr(dt);
	p1 = new Vector[Body::next_id]; 
	a2 = new Vector[Body::next_id];

	integrator_for(ChinChen, pass_1);

	calc_gravity();

	integrator_for(ChinChen, pass_2);

	calc_gforce();

	integrator_for(ChinChen, pass_3);

	calc_gravity();

	integrator_for(ChinChen, pass_4);

	if(ctsq) calc_ctsq();

	delete p1;
	delete a2;
}

void ChinChen::predict(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time, dtsq = sqr(dt), dtcb = dtsq * dt, dtsqdiv2 = dtsq * div2, dtcbdiv6 = dtcb * div6;
	wp.pos = p0.pos + p0.vel * dt + p0.acc * dtsqdiv2 + p0.jerk * dtcbdiv6;
	wp.vel = p0.vel + p0.acc * dt + p0.jerk * dtsqdiv2;
	wp.acc = p0.acc + p0.jerk * dt;
}

void ChinChen::interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1)
{
	Real dt = wp.time - p1.time;
	Vector delta = p1.vel - p0.vel;
	Real tau = p0.time - p1.time, tausq = sqr(tau), taucb = tausq * tau;
	Real dtsq = sqr(dt), dtcb = dtsq * dt, dtfr = dtsq * dtsq;
	Real dtsqdiv2 = dtsq * div2, dtcbdiv6 = dtcb * div6, dtfrdiv24 = dtfr * div24;
	Vector jerk = (-6.0f * delta - 2.0f * (2.0f * p1.acc + p0.acc) * tau) / tausq;
    Vector snap = (12.0f * delta + 6.0f * (p1.acc + p0.acc) * tau) / taucb;
	wp.pos = p1.pos + p1.vel * dt + p1.acc * dtsqdiv2 + jerk * dtcbdiv6 + snap * dtfrdiv24;
	wp.vel = p1.vel + p1.acc * dt + jerk * dtsqdiv2 + snap * dtcbdiv6;
	wp.acc = p1.acc + jerk * dt + snap * dtsqdiv2;
}

const char * ChinChen::name() const
{
	return "chin-chen";
}

ChinChenOptimised::ChinChenOptimised() : ChinChen()
{
	constant_steps_per_orbit = 666;
	variable_steps_per_orbit = 216;
}

void ChinChenOptimised::pass_1(Body * body)
{
	assert(body->is_active);
	body->begin();
	WorldPoint & k1 = body->point_0;
	body->vel = k1.vel + div6 * k1.acc * dt;
	body->pos = k1.pos + div2 * body->vel * dt;
	body->time += div2 * dt;
}

void ChinChenOptimised::pass_2(Body * body)
{
	assert(body->is_active);
	a2[body->id] = body->acc;
}

void ChinChenOptimised::pass_3(Body * body)
{
	assert(body->is_active);
	a2[body->id] += div48 * sqr(dt) * body->acc;
	body->acc = a2[body->id];
	body->vel += x2div3 * a2[body->id] * dt;
	body->pos += div2 * body->vel * dt;
	body->time = body->next_time;
}

void ChinChenOptimised::pass_4(Body * body)
{
	assert(body->is_active);
	body->vel += div6 * body->acc * dt;
	estimate_jerk(*body, body->point_0);
	body->commit();
}

void ChinChenOptimised::step(bool ctsq)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 80%)
	if(detailed_logging) cout << "\nChinChenOptimised::step()\n" << flush;

	dt = simulator->get_next_time() - simulator->get_time();
	a2 = new Vector[Body::next_id];

	integrator_for(ChinChenOptimised, pass_1);

	calc_gravity();

	integrator_for(ChinChenOptimised, pass_2);

	calc_gforce();

	integrator_for(ChinChenOptimised, pass_3);

	calc_gravity();

	integrator_for(ChinChenOptimised, pass_4);

	if(ctsq) calc_ctsq();

	delete a2;
}

const char * ChinChenOptimised::name() const
{
	return "chin-chen-optimised";
}

Hermite::Hermite() : PredictorCorrector()
{
	constant_steps_per_orbit = 250;
	variable_steps_per_orbit = 75;
}

void Hermite::calc_gravity(bool ctsq)
{
	simulator->calc_gravity(GravArgs(true, 3, false, ctsq));
}

void Hermite::predict(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time, dtsq = sqr(dt), dtcb = dtsq * dt, dtsqdiv2 = dtsq * div2, dtcbdiv6 = dtcb * div6;
	wp.pos = p0.pos + p0.vel * dt + p0.acc * dtsqdiv2 + p0.jerk * dtcbdiv6;
	wp.vel = p0.vel + p0.acc * dt + p0.jerk * dtsqdiv2;
}

void Hermite::correct(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time, dtsq = sqr(dt), dtdiv2 = dt * div2, dtsqdiv12 = dtsq * div12;
    wp.vel = p0.vel + (p0.acc + wp.acc) * dtdiv2 + (p0.jerk - wp.jerk) * dtsqdiv12;
    wp.pos = p0.pos + (p0.vel + wp.vel) * dtdiv2 + (p0.acc - wp.acc) * dtsqdiv12;
}

void Hermite::interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1)
{
	Real dt = wp.time - p1.time;
	Vector delta = p1.acc - p0.acc;
	Real tau = p0.time - p1.time, tausq = sqr(tau), taucb = tausq * tau;
	Real dtsq = sqr(dt), dtcb = dtsq * dt, dtfr = sqr(dtsq), dtff = dtcb * dtsq;
	Real dtsqdiv2 = dtsq * div2, dtcbdiv6 = dtcb * div6, dtfrdiv24 = dtfr * div24, dtffdiv144 = dtff * div144;
	Vector snap = (-6.0f * delta - 2.0f * (2.0f * p1.jerk + p0.jerk) * tau) / tausq;
	Vector crackle = (12.0f * delta + 6.0f * (p1.jerk + p0.jerk) * tau) / taucb;
	wp.pos = p1.pos + p1.vel * dt + p1.acc * dtsqdiv2 + p1.jerk * dtcbdiv6 + snap * dtfrdiv24 + crackle * dtffdiv144;
    wp.vel = p1.vel + p1.acc * dt + p1.jerk * dtsqdiv2 + snap * dtcbdiv6 + crackle * dtfrdiv24;
}

const char * Hermite::name() const
{
	return "hermite";
}

#if (DERIVATIVE >= 4)

Hermite6::Hermite6() : PredictorCorrector()
{
	constant_steps_per_orbit = 193;
	variable_steps_per_orbit = 58;
}

void Hermite6::calc_gravity(bool ctsq)
{
	simulator->calc_gravity(GravArgs(true, 4, false, ctsq));
}

void Hermite6::predict(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time, dtsq = sqr(dt), dtcb = dtsq * dt, dtfr = sqr(dtsq), dtsqdiv2 = dtsq * div2, dtcbdiv6 = dtcb * div6, dtfrdiv24 = dtfr * div24;
	wp.pos = p0.pos + p0.vel * dt + p0.acc * dtsqdiv2 + p0.jerk * dtcbdiv6 + p0.snap * dtfrdiv24;
	wp.vel = p0.vel + p0.acc * dt + p0.jerk * dtsqdiv2 + p0.snap * dtcbdiv6;
	wp.acc = p0.acc + p0.jerk * dt + p0.snap * dtsqdiv2;
}

void Hermite6::correct(WorldPoint & wp, const WorldPoint & p0)
{
	WorldPoint wp2(wp.time);
	interpolate(wp2, wp, p0);
	wp.pos = wp2.pos;
	wp.vel = wp2.vel;
	wp.acc = wp2.acc;
}

void Hermite6::interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1)
{
	Real h = 0.5f * (p0.time - p1.time), hsq = sqr(h), hcb = hsq * h, hfr = sqr(hsq), hff = hsq * hcb;
	Real dt = wp.time - p1.time, dtsq = sqr(dt), dtsqdiv2 = dtsq * div2, dtcb = dtsq * dt, dtcbdiv6 = dtcb * div6, dtfr = sqr(dtsq), dtfrdiv24 = dtfr * div24;
	Real dtff = dtcb * dtsq, dtffdiv120 = dtff * div120, dtsx = sqr(dtcb), dtsxdiv720 = dtsx * div720, dtsv = dtfr * dtcb, dtsvdiv5040 = dtsv * div5040;
	Vector aplus = p0.acc + p1.acc;
	Vector amin = p0.acc - p1.acc;
	Vector jplus = h * (p0.jerk + p1.jerk);
	Vector jmin = h * (p0.jerk - p1.jerk);
	Vector splus = hsq * (p0.snap + p1.snap);
	Vector smin = hsq * (p0.snap - p1.snap);
	Vector a3half = 3.0f * div4 * (-5.0f * amin + 5.0f * jplus - smin) / hcb;
	Vector a4half = 3.0f * div2 * (splus - jmin) / hfr;
	Vector a5half = 15.0f * div2 * (3.0f * amin - 3.0f * jplus + smin) / hff;
	Vector crackle = a3half - h * a4half + div2 * hsq * a5half;
	Vector pop = a4half - h * a5half;
	Vector nanda = a5half;
	wp.pos = p1.pos + p1.vel * dt + p1.acc * dtsqdiv2 + p1.jerk * dtcbdiv6 + p1.snap * dtfrdiv24 + crackle * dtffdiv120 + pop * dtsxdiv720 + nanda * dtsvdiv5040;
    wp.vel = p1.vel + p1.acc * dt + p1.jerk * dtsqdiv2 + p1.snap * dtcbdiv6 + crackle * dtfrdiv24 + pop * dtffdiv120 + nanda * dtsxdiv720;
    wp.acc = p1.acc + p1.jerk * dt + p1.snap * dtsqdiv2 + crackle * dtcbdiv6 + pop * dtfrdiv24 + nanda * dtffdiv120;
}

const char * Hermite6::name() const
{
	return "hermite-6";
}

#if (DERIVATIVE >= 5)

Hermite8::Hermite8() : PredictorCorrector()
{
	constant_steps_per_orbit = 148;
	variable_steps_per_orbit = 45;
}

void Hermite8::calc_gravity(bool ctsq)
{
	simulator->calc_gravity(GravArgs(true, 5, false, ctsq));
}

void Hermite8::predict(WorldPoint & wp, const WorldPoint & p0)
{
	Real dt = wp.time - p0.time, dtsq = sqr(dt), dtcb = dtsq * dt, dtfr = sqr(dtsq), dtff = dtcb * dtsq;
	Real dtsqdiv2 = dtsq * div2, dtcbdiv6 = dtcb * div6, dtfrdiv24 = dtfr * div24, dtffdiv120 = dtff * div120;
	wp.pos = p0.pos + p0.vel * dt + p0.acc * dtsqdiv2 + p0.jerk * dtcbdiv6 + p0.snap * dtfrdiv24 + p0.crackle * dtffdiv120;
	wp.vel = p0.vel + p0.acc * dt + p0.jerk * dtsqdiv2 + p0.snap * dtcbdiv6 + p0.crackle * dtfrdiv24;
	wp.acc = p0.acc + p0.jerk * dt + p0.snap * dtsqdiv2 + p0.crackle * dtcbdiv6;
	wp.jerk = p0.jerk + p0.snap * dt + p0.crackle * dtsqdiv2;
}

void Hermite8::correct(WorldPoint & wp, const WorldPoint & p0)
{
	WorldPoint wp2(wp.time);
	interpolate(wp2, wp, p0);
	wp.pos = wp2.pos;
	wp.vel = wp2.vel;
	wp.acc = wp2.acc;
	wp.jerk = wp2.jerk;
}

void Hermite8::interpolate(WorldPoint & wp, const WorldPoint & p0, const WorldPoint & p1)
{
	Real h = 0.5f * (p0.time - p1.time), hsq = sqr(h), hcb = hsq * h, hfr = sqr(hsq), hff = hsq * hcb, hsx = sqr(hcb), hsv = hfr * hcb, hsqdiv2 = hsq * div2;
	Real dt = wp.time - p1.time, dtsq = sqr(dt), dtcb = dtsq * dt, dtfr = sqr(dtsq), dtff = dtcb * dtsq, dtsx = sqr(dtcb), dtsv = dtfr * dtcb, dtei = sqr(dtfr), dtnn = dtff * dtfr;
	Real dtsqdiv2 = dtsq * div2, dtcbdiv6 = dtcb * div6, dtfrdiv24 = dtfr * div24, dtffdiv120 = dtff * div120, dtsxdiv720 = dtsx * div720, dtsvdiv5040 = dtsv * div5040, dteidiv40320 = dtei * div40320, dtnndiv362880 = dtnn * div362880;
	Vector aplus = p0.acc + p1.acc;
	Vector amin = p0.acc - p1.acc;
	Vector jplus = h * (p0.jerk + p1.jerk);
	Vector jmin = h * (p0.jerk - p1.jerk);
	Vector splus = hsq * (p0.snap + p1.snap);
	Vector smin = hsq * (p0.snap - p1.snap);
	Vector cplus = hcb * (p0.crackle + p1.crackle);
	Vector cmin = hcb * (p0.crackle - p1.crackle);
	Vector a4half = 3.0f * div4 * (-5.0f * jmin + 5.0f * splus - cmin) / hfr;
	Vector a5half = 15.0f * div4 * (21.0f * amin - 21.0f * jplus + 8.0f * smin - cplus) / hff;
	Vector a6half = 45.0f * div2 * (jmin - splus + div3 * cmin) / hsx;
	Vector a7half = 315.0f * div2 * (-5.0f * amin + 5.0f * jplus -2.0f * smin + div3 * cplus) / hsv;
	Vector a4zero = a4half - h * a5half + hsqdiv2 * a6half - hcb * div6 * a7half;
	Vector a5zero = a5half - h * a6half + hsqdiv2 * a7half;
	Vector a6zero = a6half - h * a7half;
	Vector a7zero = a7half;
	wp.pos = p1.pos + p1.vel * dt + p1.acc * dtsqdiv2 + p1.jerk * dtcbdiv6 + p1.snap * dtfrdiv24 + p1.crackle * dtffdiv120 + a4zero * dtsxdiv720 + a5zero * dtsvdiv5040 + a6zero * dteidiv40320 + a7zero * dtnndiv362880;
    wp.vel = p1.vel + p1.acc * dt + p1.jerk * dtsqdiv2 + p1.snap * dtcbdiv6 + p1.crackle * dtfrdiv24 + a4zero * dtffdiv120 + a5zero * dtsxdiv720 + a6zero * dtsvdiv5040 + a7zero * dteidiv40320;
    wp.acc = p1.acc + p1.jerk * dt + p1.snap * dtsqdiv2 + p1.crackle * dtcbdiv6 + a4zero * dtfrdiv24 + a5zero * dtffdiv120 + a6zero * dtsxdiv720 + a7zero * dtsvdiv5040;
    wp.jerk = p1.jerk + p1.snap * dt + p1.crackle * dtsqdiv2 + a4zero * dtcbdiv6 + a5zero * dtfrdiv24 + a6zero * dtffdiv120 + a7zero * dtsxdiv720;
}

const char * Hermite8::name() const
{
	return "hermite-8";
}

#endif
#endif
#endif

} // namespace GravSim
