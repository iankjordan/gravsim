/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRAVSIM_COMPOSITE_H
#define GRAVSIM_COMPOSITE_H

#include "body.h"

class Quadrupole
{
private:

	Vector quad[3];

public:

	Quadrupole() { * this = 0.0f; }
	Quadrupole operator = (const Real & r) { quad[0] = r; quad[1] = r; quad[2] = r; return *this; }
	void operator += (const Quadrupole & q) { quad[0] += q.quad[0]; quad[1] += q.quad[1]; quad[2] += q.quad[2]; }
	Vector operator [] (int i) const { return quad[i]; }
	Vector & operator () (int i) { return quad[i]; }
};

struct Expansion
{
	Vector coeff1;
#if (EXPANSION >= 2)
	Vector coeff2;
#if (EXPANSION >= 3)
	Vector coeff3;
#endif
#endif

	Expansion();
};

struct Composite : public Particle
{
	Particle * components[MAX_BRANCH_FACTOR];
	int components_size;
	Quadrupole quadrupole;
	Expansion expansion;
	Real rr_critical;

	Composite();
	int depth(int d = 0) const;
	void apply_gravity(const GravArgs & args);
	void add(Particle * component);
	void calc_monopole();
	void calc_multipole(const Real & theta);
	bool approximation_is_ok(const Real & rr) const { return rr > rr_critical; }
	bool approximation_is_ok(const Real & rr, const Composite * composite) const { return rr > (rr_critical + composite->rr_critical); }

	// Methods used by Composite

	void add_multipole_to(Composite * composite) const;
	void translate_gravity_from(const Composite * composite);

	// Methods used by direct_ and pairwise_ grav<> and ctsq<> templates

	Real multipole_potential(const Vector & d, const Real & r, const Real & rr) const;
	void evaluate_expansion(const AccEtc & grav, const Real & r);

	// Methods used by DirectInteractor

	void calc_gravity_direct(Body ** batch, int batch_size, const GravArgs & args) const;

	// Methods used by PairwiseInteractor

	void calc_gravity_pairwise(Particle * particle, const GravArgs & args);
	void calc_gravity_pairwise(Body * body, const GravArgs & args);
	void calc_gravity_pairwise(Composite * composite, const GravArgs & args);
};

#endif
