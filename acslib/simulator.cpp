/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "acs.h"

namespace GravSim {

void (*Simulator::commit_hook)(const Simulator * simulator) = 0;

void Simulator::log_config()
{
	if(logging) {
	cout.precision(REAL_DIGITS);
	cout << "--------Machine Configuration-----------\n";
	cout << "Real Type = " << REAL_TYPE << "\n";
	cout << "Precision = " << REAL_DIGITS << "\n";
	cout << "Processors = " << num_procs << "\n";
	cout << "Max Threads = " << max_threads << "\n";
	cout << "Max Batch Size = " <<  max_batch_size << "\n\n";

	cout << "--------Integrator Configuration--------\n";
	cout << "Integrator  = " << integrator->name() << "\n";
	cout << "Min Integrator Parallel Size = " <<  min_integrator_parallel_size << "\n";
	cout << "Steps Per Collision Timescale = " << to_int(1.0f / (constant_timesteps ? constant_timestep_factor : variable_timestep_factor)) << "\n\n";

	cout << "--------Simulator Configuration-------\n";
	if(divider == 0)
	{
		cout << "Accelerator = brute-force\n";
	}
	else
	{
		cout << "Accelerator = " << divider->get_accelerator_name() << "\n";
	}
	cout << "Timesteps = " << (constant_timesteps ? "Constant" : shared_timesteps ? "Shared" : "Individual") << "\n";
	cout << "Level = " << level << "\n";
	cout << "Softening = " << softening << "\n\n" << flush;

	if(divider != 0) divider->log_config(); }
}

void Simulator::log_energy()
{
	if(logging) {
	cout << "----Energy------------------------------\n";
	Real ke = kinetic_energy();
	cout << "Kinetic Energy = " << ke << "\n";
	Real pe = potential_energy();
	cout << "Potential Energy = " << pe << "\n";
	Real energy = ke + pe;
	cout << "Total Energy = " << energy << "\n";
	if(initial_energy == 0.0f)
	{
		initial_energy = energy;
	}
	else
	{
		Real error = fabs(energy - initial_energy) / max(fabs(initial_energy), (Real) Epsilon);
		cout << "Energy Error = " << error * 100.0f << " %\n";
	}
	cout << "\n" << flush; }
}

void Simulator::log_performance()
{
	if(logging) {
	cout << "----Performance-------------------------\n";
	clock_t cpu = clock() - initial_cpu;
	cout << "CPU Time = " << cpu << "\n\n" << flush; }
}

void Simulator::log_stats_plus()
{
	if(logging) {
	cout << "----Stats-Plus--------------------------\n";
	cout << "Minimum Collision Time = " << minimum_collision_time() << "\n";
	cout << "Root Mean Square Collision Time = " << root_mean_square_collision_time() << "\n";
	if(lt(integrator->closest_approach_so_far, Infinity))
	{
		cout << "Closest Approach So Far = " << integrator->closest_approach_so_far << "\n";
	}
	cout << "\n" << flush; }
}

bool Simulator::same_time() const
{
	bool result = true;
	for(int b = 0; b < bodies_size; b++)
	{
		result &= eq(bodies[b].time, time, 2.0f);
	}
	return result;
}

bool Simulator::same_next_time() const
{
	bool result = true;
	for(int b = 0; b < bodies_size; b++)
	{
		result &= eq(bodies[b].next_time, next_time, 2.0f);
	}
	return result;
}

void Simulator::choose_constant(const Real & target_time)
{
	assert(constant_timesteps);
	Real step_time = block_or_continuous_time(constant_target_time);
	time = bodies[0].time;
	next_time = min(time + step_time, target_time);
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		assert(body->is_active);
		assert(body->time == time);
		body->next_time = next_time;
	}
	step_size = bodies_size;
	sort_order = SortIsActive;
}

void Simulator::choose_shared(const Real & target_time)
{
	assert(shared_timesteps);
	time = bodies[0].time;
	next_time = target_time;
	for(int b1 = 0; b1 < bodies_size; b1++)
	{
		Body * body = &bodies[b1];
		if(analyser != 0) body->begin_regularisation(analyser);
		Real variable_target_time = body->target_time(variable_timestep_factor);
		Real step_time = block_or_continuous_time(variable_target_time);
		Real body_next_time = body->time + step_time;
		next_time = min(next_time, body_next_time);
	}
	for(int b2 = 0; b2 < bodies_size; b2++)
	{
		Body * body = &bodies[b2];
		assert(body->is_active);
		assert(body->time == time);
		body->next_time = next_time;
		body->reset_neighbours();
	}
	step_size = bodies_size;
	sort_order = SortIsActive;
}

void Simulator::choose_individual(const Real & target_time)
{
	assert(individual_timesteps);
	time = Infinity;
	next_time = Infinity;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		if(analyser != 0) body->begin_regularisation(analyser);
		if(body->is_active)
		{
			Real variable_target_time = body->target_time(variable_timestep_factor);
			Real step_time = block_or_continuous_time(variable_target_time);
			body->next_time = body->time + step_time;
		}
		if(lt(body->time, target_time) && gt(body->next_time, target_time))
		{
			body->next_time = target_time;
		}
		if(lte(body->next_time, next_time))
		{
			if(lt(body->next_time, next_time))
			{
				next_time = body->next_time;
				time = body->time;
			}
			else if(lt(body->time, time))
			{
				time = body->time;
			}
		}
	}
	bool one_shot = integrator->one_shot();
	int step_count = 0;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		if(eq(body->next_time, next_time) && (one_shot || eq(body->time, time)))
		{
			body->is_active = true;
			step_count++;
		}
		else
		{
			body->is_active = false;
		}
		body->reset_neighbours();
	}
	step_size = step_count;
	if(step_size < bodies_size)
	{
		sort_order = SortNone;
		sort_bodies_is_active();
	}
	else
	{
		sort_order = SortIsActive;
	}
}

const Particle * Simulator::find_particle(unsigned particle_id)
{
	const Particle * result = 0;
	for(int b = 0; b < bodies_size; b++)
	{
		if(bodies[b].id == particle_id)
		{
			result = &bodies[b];
			break;
		}
	}
	return result;
}

void Simulator::begin_step(const Real & target_time)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 4%)
	if(detailed_logging) cout << "Simulator::choose_step()\n" << flush;

	if(constant_timesteps)
	{
		choose_constant(target_time);
	}
	else if(shared_timesteps)
	{
		choose_shared(target_time);
	}
	else // if(individual_timesteps)
	{
		choose_individual(target_time);
	}

	if(divider != 0) divider->startup(this);

	if (detailed_logging)
	{
		cout << setw(REAL_DIGITS + 5) << time << ", \t";
		cout << setw(REAL_DIGITS + 5) << next_time << ", \t";
		cout << setw(10) << step_size << ", \t";
		cout << setw(10) << calc_batch_size(step_size) << "\n" << flush; 
	}
}

void Simulator::end_step()
{
	if((analyser != 0) && variable_timesteps)
	{
		for(int b = 0; b < bodies_size; b++)
		{
			Body * body = &bodies[b];
			body->end_regularisation();
		}
	}
}

void Simulator::take_snapshot()
{
	if(detailed_logging) cout << "Simulator::take_snapshot()\n" << flush;

	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		if(body->is_active)
		{
			body->reset_gravity();
		}
		else
		{
			body->take_snapshot(*body, integrator);
		}
	}
}

Body ** Simulator::active_buffer()
{
	assert(sort_order == SortIsActive);
	Body ** buffer = new Body *[step_size];
	for(int b = 0; b < step_size; b++)
	{
		Body * body = &bodies[b];
		assert(body->is_active);
		buffer[b] = body;
	}
	return buffer;
}

void Simulator::apply_gravity(const GravArgs & args)
{
	int working_size = get_working_size();
	for(int b = 0; b < working_size; b++)
	{
		Body * body = &bodies[b];
		if(body->is_active)
		{
			body->apply_gravity(args);
		}
	}
}

Real Simulator::block_or_continuous_time(const Real & target_time) const
{
	DETAILED_PROFILE_FUNC();

	Real time;
	if(block_timesteps)
	{
		time = 1.0f;
		while(time < target_time) time *= 2.0f;
		while(time > target_time) time *= 0.5f;
		if(time * sqrt2 < target_time) time *= sqrt2;
	}
	else // continuous_timesteps
	{
		time = target_time;
	}
	return max(time, (Real) Epsilon);
}

Simulator::Simulator(Integrator * i, Divider * d, bool ct, bool st, bool bt, int lv, const Real & sf, bool an, const Real & hsf) : Model()
{
	if(serial) max_threads = 1;
	if(an) analyser = new Analyser(hsf); else analyser = 0;
	integrator = i;
	divider = d;
	constant_timesteps = ct;
	variable_timesteps = !ct;
	shared_timesteps = st;
	individual_timesteps = (!ct && !st);
	block_timesteps = bt;
	// continuous_timesteps = !block_timesteps
	level = lv;
	constant_timestep_factor = integrator->constant_timestep_factor(level) * CONSTANT_TUNING_FACTOR;
	variable_timestep_factor = integrator->variable_timestep_factor(level) * VARIABLE_TUNING_FACTOR;
	constant_target_time = constant_timestep_factor;
	sqrt2 = sqrt((Real) 2.0f);
	softening = fabs(sf);
	started = false;
	initial_energy = 0.0f;
	initial_cpu = clock();
	logpoint = 0;
	time = 0.0f;
	next_time = 0.0f;
	step_size = 0;
}

Simulator::~Simulator()
{
	delete analyser;
}

void Simulator::run(const Real & sim_time, const Real & log_interval)
{
	DETAILED_PROFILE_FUNC();

	startup();
	while(lt((time = minimum_time()), sim_time))
	{
		Real interval_time = min(time + log_interval, sim_time);
		evolve(interval_time);
	}

	log_stats();
	log_stats_plus();
}

void Simulator::startup()
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 1%)

	time = bodies[0].time;
	next_time = bodies[0].next_time;
	assert(same_time());
	assert(same_next_time());

	log_config();
	log_stats();
	if(logging) cout <<"------------------------------Starting-----------------------------------------\n\n" << flush;

	Particle::simulator = this;

	step_size = bodies_size;
	if(divider != 0) divider->startup(this);
	integrator->startup(this);
	constant_target_time = minimum_collision_time() * constant_timestep_factor;
	started = true;

	log_stats_plus();
	log_energy();
	log_performance();
}

void Simulator::evolve(const Real & interval_time)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 99%)
	assert(started);

	if(detailed_logging) 
	{
		cout << "------------------------------Evolving-----------------------------------------\n\n";
		cout << setw(REAL_DIGITS + 5) << "Time" << ", \t";
		cout << setw(REAL_DIGITS + 5) << "Next-Time" << ", \t";
		cout << setw(10) << "Step-Size" << ", \t";
		cout << setw(10) << "Batch-Size" << "\n" << flush; }

	while(lt((time = minimum_time()), interval_time))
	{
		begin_step(interval_time);
		integrator->step(variable_timesteps);
		if(commit_hook != 0) commit_hook(this);
	}
	end_step();

	logpoint ++;

	if(detailed_logging) cout << "\nLog Point: " << logpoint << "\n\n" << flush;

	assert(same_time());

	log_energy();
	log_performance();
}

void Simulator::calc_gravity(const GravArgs & args)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 47%)
	if(detailed_logging) cout << "Simulator::calc_gravity()\n" << flush;

	take_snapshot();
	if((divider != 0) && divider->worth_using())
	{
		divider->calc_gravity(GravArgs(args, analyser, softening));
	}
	else
	{
		calc_gravity_brute_force(GravArgs(args, analyser, softening));
	}
}

void Simulator::calc_gravity_brute_force(const GravArgs & args)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 27%)
	if(detailed_logging) cout << "Simulator::calc_gravity_brute_force()\n" << flush;

	sort_bodies_is_active();

	if(serial || (step_size < min_brute_force_parallel_size))
	{
		calc_gravity_brute_force_serial(args);
	}
	else
	{
		calc_gravity_brute_force_parallel(args);
	}
}

void Simulator::calc_gravity_brute_force_serial(const GravArgs & args)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 27%)
	if(detailed_logging) cout << "Simulator::calc_gravity_brute_force_serial()\n" << flush;

	serial_for(b1, 0, step_size)
	{
		Body * body1 = &bodies[b1];
		assert(body1->is_active);
		serial_for(b2, b1 + 1, step_size)
		{
			Body * body2 = &bodies[b2];
			assert(body2->is_active);
			body1->calc_gravity_pairwise(body2, args);
		}
	}

	if(step_size < bodies_size)
	{
		calc_gravity_brute_force_remainder(args);
	}
	else
	{
		apply_gravity(args);
	}
}

void Simulator::calc_gravity_brute_force_parallel(const GravArgs & args)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 27%)
	if(detailed_logging) cout << "Simulator::calc_gravity_brute_force_parallel()\n" << flush;

	serial_for(n1, 1, step_size)
	{
		int PS = calc_pairwise_size(n1, step_size);
		int NT = pairwise_threads(step_size - n1, PS);
#		pragma omp parallel num_threads(NT)
		parallel_pairwise_for(n2, 0, step_size - n1, PS)
		{
			int b1 = n2;
			int b2 = n1 + n2;
//#pragma omp critical
//			cout << "TN = " << TN << " (" << b1 << ", " << b2 << ")\n";
			Body * body1 = &bodies[b1];
			Body * body2 = &bodies[b2];
			assert(body1->is_active && body2->is_active);
			body1->calc_gravity_pairwise(body2, args);
		}
//		cout << "parallel pass 1 end\n";
		if(n1 <= (step_size / 2))
		{
#			pragma omp parallel num_threads(NT)
			parallel_pairwise_for2(n2, 0, step_size - n1, PS)
			{
				int b1 = n2;
				int b2 = n1 + n2;
//#pragma omp critical
//			cout << "TN = " << TN << " (" << b1 << ", " << b2 << ")\n";
				Body * body1 = &bodies[b1];
				Body * body2 = &bodies[b2];
				assert(body1->is_active && body2->is_active);
				body1->calc_gravity_pairwise(body2, args);
			}
//		cout << "parallel pass 2 end\n";
		}
	}
//		cout << "serial end\n";
	if(step_size < bodies_size)
	{
		calc_gravity_brute_force_remainder(args);
	}
	else
	{
		apply_gravity(args);
	}
}

void Simulator::calc_gravity_brute_force_remainder(const GravArgs & args)
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 27%)
	if(detailed_logging) {
	cout << "Simulator::calc_gravity_brute_force_remainder()\n";
	cout << setw(10) << "Batch" << ", \t" << setw(10) << "Batch-Size" << "\n" << flush; }

	Body ** buffer = active_buffer();
	int BS = calc_batch_size(step_size);
	int NT = batch_threads(step_size, BS);
#	pragma omp parallel num_threads(NT)
	parallel_batch_for(b3, 0, step_size, BS)
	{
		int batch_size = min(step_size - b3, BS);
		Body ** batch = buffer + b3;

		if(detailed_logging) {
#		pragma omp critical
		cout << setw(10) << b3 << ", \t" << setw(10) << batch_size << "\n" << flush; }

		for(int b4 = step_size + 1; b4 < bodies_size; b4++)
		{
			Body * body4 = &bodies[b4];
			assert(!body4->is_active);
			body4->calc_gravity_direct(batch, batch_size, args);
		}

		for(int b5 = 0; b5 < batch_size; b5++)
		{
			Body * body5 = batch[b5];
			assert(body5->is_active);
			body5->apply_gravity(args);
		}
	}
	delete buffer;
}

Real Simulator::kinetic_energy() const
{
	DETAILED_PROFILE_FUNC();
	assert(same_time());

	Real result = 0.0f;
	for(int b = 0; b < bodies_size; b++)
	{
		const Body * body = &bodies[b];
		result += body->kinetic_energy();
	}
	return result;
}

Real Simulator::potential_energy() const
{
	DETAILED_PROFILE_FUNC();
	assert(same_time());

	Real result = 0.0f;
	for(int b = 0; b < bodies_size; b++)
	{
		const Body * body = &bodies[b];
		result += body->potential_energy();
	}
	return result / 2.0f;
}

Real Simulator::minimum_time() const
{
	DETAILED_PROFILE_FUNC();

	Real min_time = Infinity;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		min_time = min(min_time, body->time);
	}
	return min_time;
}

Real Simulator::minimum_next_time() const
{
	DETAILED_PROFILE_FUNC();

	Real min_next_time = Infinity;
	for(int b = 0; b < bodies_size; b++)
	{
		Body * body = &bodies[b];
		min_next_time = min(min_next_time, body->next_time);
	}
	return min_next_time;
}

Real Simulator::minimum_collision_time() const
{
	DETAILED_PROFILE_FUNC();
	assert(same_time());

	Real min_collision_time_sq = Infinity;
	for(int b = 0; b < bodies_size; b++)
	{
		const Body * body = &bodies[b];
		min_collision_time_sq = min(min_collision_time_sq, body->neighbours[0].collision_time_sq);
	}
	return sqrt(min_collision_time_sq);
}

Real Simulator::root_mean_square_collision_time() const
{
	DETAILED_PROFILE_FUNC();
	assert(same_time());

	Real total_collision_time_sq = 0.0f;
	for(int b = 0; b < bodies_size; b++)
	{
		const Body * body = &bodies[b];
		total_collision_time_sq += body->neighbours[0].collision_time_sq;
	}
	return sqrt(total_collision_time_sq / bodies_size);
}

} // namespace GravSim
