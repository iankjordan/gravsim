/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "acs.h"

namespace GravSim {

Expansion::Expansion()
{
	coeff1 = 0.0f;
#if (EXPANSION >= 2)
	coeff2 = 0.0f;
#if (EXPANSION >= 3)
	coeff3 = 0.0f;
#endif
#endif
}

Composite::Composite() : Particle(), quadrupole(), expansion()
{
	components_size = 0;
}

int Composite::depth(int d) const
{
	int depth_max = d + 1;
	for(int n = 0; n < components_size; n++)
	{
		Particle * component = components[n];
		int depth = component->depth(d + 1);
		if(depth > depth_max) depth_max = depth;
	}
	return depth_max;
}

void Composite::apply_gravity(const GravArgs & args)
{
	assert(is_active);

	for(int n = 0; n < components_size; n++)
	{
		Particle * component = components[n];
		if(component->is_active)
		{
			component->apply_gravity(args);
		}
	}
}

void Composite::add(Particle * component)
{
	assert(components_size < MAX_BRANCH_FACTOR);
	components[components_size] = component;
	components_size++;
	is_active |= component->is_active;
}

void Composite::calc_monopole()
{
	mass = 0.0f;
	pos = 0.0f;
	vel = 0.0f;
	for(int n1 = 0; n1 < components_size; n1++)
	{
		Particle * component = components[n1];
		mass += component->mass;
		pos += (component->pos * component->mass);
		vel += (component->vel * component->mass);

		// FUTURE: Include acc and jerk terms
	}
	pos /= mass;
	vel /= mass;
	radius = 0.0f;
	for(int n2 = 0; n2 < components_size; n2++)
	{
		Particle * component = components[n2];
		Vector d = component->pos - pos;
		Real r = sqrt(sqr(d)) + component->radius;
		if(r > radius) radius = r;
	}
}

void Composite::calc_multipole(const Real & theta) 
{
	calc_monopole();
	rr_critical = (components_size > 1) ? sqr(radius / theta) : Infinity;
	for(int n = 0; n < components_size; n++)
	{
		Particle * component = components[n];
		Vector d = component->pos - pos;
		Real rr = sqr(d);
		for(int j = 0; j < 3; j++)
		{
			for(int k = 0; k < 3; k++)
			{
				Real q = 3.0f * d[j] * d[k];
				if(j == k) q -= rr;
				quadrupole(j)(k) += component->mass * q;
			}
		}
		component->add_multipole_to(this);
	}

	// FUTURE: octopole and hexadecapole terms
}

void Composite::add_multipole_to(Composite * composite) const // Passing up the tree
{
	composite->quadrupole += quadrupole;
}

void Composite::translate_gravity_from(const Composite * composite) // Passing down the tree
{
	Vector d = pos - composite->pos;
	const Vector & c1 = composite->expansion.coeff1;
	Real m1 = max(mag(c1), Epsilon), d1 = dot(c1, d) / m1, d1sqdiv2 = sqr(d1) / 2.0f;

	potential += composite->potential - dot(composite->field.acc, d) - m1 * d1sqdiv2;
	field.acc += composite->field.acc + c1 * d1;
	expansion.coeff1 += c1;

#if (EXPANSION >= 2)
	Vector & c2 = composite->expansion.coeff2;
	Real m2 = max(mag(c2), Epsilon), d2 = dot(c2, d) / m2, d2sqdiv2 = sqr(d2) / 2.0f, d2cbdiv6 = cub(d2) / 6.0f;

	potential -= m2 * d2cbdiv6;
	field.acc += c2 * d2sqdiv2;
	expansion.coeff1 += c2 * d2;
	expansion.coeff2 = c2;

#if (EXPANSION >= 3)
	Vector & c3 = composite->expansion.coeff3;
	Real m3 = max(mag(c3), Epsilon), d3 = dot(c3, d) / m3, d3sqdiv2 = sqr(d3) / 2.0f, d3cbdiv6 = cub(d3) / 6.0f, d3, d3frdiv24 = frth(d3) / 24.0f;

	potential -= m3 * d3frdiv24;
	field.acc += c3 * d3cbdiv6;
	expansion.coeff1 += c3 * d3sqdiv2;
	expansion.coeff2 = c3 * d3;
	expansion.coeff3 = c3;

#endif
#endif

	// FUTURE: Include jerk etc terms
}

Real Composite::multipole_potential(const Vector & d, const Real & r, const Real & rr) const
{
	Real qq = 0.0f;
	for(int j = 0; j < 3; j++)
	{
		for(int k = 0; k < 3; k++)
		{
			Real q = 3.0f * d[j] * d[k];
			if(j == k) q -= rr;
			qq += quadrupole[j][k] * q;
		}
	}

	// FUTURE: Include octopole and hexadecapole terms

	return - (qq / (6.0f * sqr(rr) * r));
}

void Composite::evaluate_expansion(const AccEtc & grav, const Real & r)
{
	Vector coeff1 = -2.0f * grav.acc / r;
	expansion.coeff1 += coeff1;
#if (EXPANSION >= 2)
	Vector coeff2 = -3.0f * coeff1 / r;
	expansion.coeff2 += coeff2;
#if (EXPANSION >= 3)
	Vector coeff3 = -4.0f * coeff2 / r;
	expansion.coeff3 += coeff3;
#endif
#endif

	// FUTURE: Include multipole terms
}

void Composite::calc_gravity_direct(Body ** batch, int batch_size, const GravArgs & args) const
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 25%)
	assert(batch_size > 0);

	Body ** batch2 = new Body*[batch_size];
	int batch2_size = 0;
	for(int b = 0; b < batch_size; b++)
	{
		Body * body = batch[b];
		assert(body != 0);
		assert(body->is_active);

		Vector d = pos - body->pos;
		Real rr = sqr(d);
		if(approximation_is_ok(rr)) // Multipole approximation
		{
			direct_grav<Body, Composite>(body, this, args, d, sqrt(rr), rr);
		}
		else
		{
			batch2[batch2_size++] = body;
		}
	}
	if(batch2_size > 0)
	{
		for(int n = 0; n < components_size; n++) // Recursive
		{
			components[n]->calc_gravity_direct(batch2, batch2_size, args);
		}
	}
	delete batch2;
}

void Composite::calc_gravity_pairwise(Particle * particle, const GravArgs & args)
{
	assert(is_active || particle->is_active);

	if(particle != 0) // Non-self-interaction
	{
		particle->calc_gravity_pairwise(this, args);
	}
	else // Self-interaction
	{
		for(int n1 = 0; n1 < components_size; n1++)
		{
			Particle * component1 = components[n1];
			for(int n2 = n1 + 1; n2 < components_size; n2++)
			{
				Particle * component2 = components[n2];
				if(component1->is_active || component2->is_active) // Near field
				{
					component2->calc_gravity_pairwise(component1, args);
				}
			}
			if(component1->is_active) // Far field + Self-interaction
			{
				component1->translate_gravity_from(this);
				component1->calc_gravity_pairwise((Particle *) 0, args);
			}
		}
	}
}
/* candidate parallel implementation:
//		cout << "\n";
	serial_batch_for(n1, 0, step_size, 2)
	{
		serial_for(i, 0, 2)
		{
			int NT = batch_threads(step_size - n1, 2);
#			pragma omp parallel num_threads(NT)
			parallel_batch_for(n2, 0, step_size - n1, 2)
			{
				int b1 = n2 + i;
				int b2 = n1 + n2 - i + 1;
				if((b2 - b1 > 0) && (b1 < step_size) && (b2 < step_size))
				{
					Body * body1 = &bodies[b1];
					Body * body2 = &bodies[b2];
					assert(body1->is_active && body2->is_active);
					body1->calc_gravity_pairwise(body2, args);
//					cout << "(" << c1 << ", " << c2 << ")\n";
				}
			}
			cout << "parallel_end\n";
		}
	}
	cout << "serial_end\n";

	serial_batch_for(n1, 0, step_size, 2)
	{
		serial_batch_for(n2, n1, step_size, 2)
		{
			int NT = parallel_threads(2);
#			pragma omp parallel num_threads(NT)
			parallel_for(i, 0, 2)
			{
				int b1 = n1 + i;
				int b2 = n1 + n2 + i;
				if((b2 - b1 > 0) && (b1 < step_size) && (b2 < step_size))
				{
					Body * body1 = &bodies[b1];
					Body * body2 = &bodies[b2];
					assert(body1->is_active && body2->is_active);
					body1->calc_gravity_pairwise(body2, args);
//					cout << "(" << c1 << ", " << c2 << ")\n";
				}
			}
//			cout << "parallel_end\n";
		}
	}
//	cout << "serial_end\n";
//	exit(1);*/

void Composite::calc_gravity_pairwise(Body * body, const GravArgs & args)
{
	assert(body != 0);
	assert(is_active || body->is_active);

	Vector d = pos - body->pos;
	Real rr = sqr(d);
	if(approximation_is_ok(rr))
	{
		if(body->is_active)
		{
			if(is_active) // Multipole, Expansion approximations
			{
				pairwise_grav<Body, Composite>(body, this, args, d, sqrt(rr), rr);
			}
			else // Multipole approximation
			{
				direct_grav<Body, Composite>(body, this, args, d, sqrt(rr), rr);
			}
		}
		else // Expansion approximation
		{
			direct_grav<Composite, Body>(this, body, args, -d, sqrt(rr), rr);
		}
	}
	else
	{
		for(int n = 0; n < components_size; n++)
		{
			Particle * component = components[n];
			if(component->is_active || body->is_active) // Recursive
			{
				component->calc_gravity_pairwise(body, args);
			}
		}
	}
}

void Composite::calc_gravity_pairwise(Composite * composite, const GravArgs & args)
{
	assert(composite != 0);
	assert(is_active || composite->is_active);

	Vector d = pos - composite->pos;
	Real rr = sqr(d);
	if(approximation_is_ok(rr, composite))
	{
		if(composite->is_active)
		{
			if(is_active)// Multipole + Expansion, Multipole + Expansion approximations
			{
				pairwise_grav<Composite, Composite>(composite, this, args, d, sqrt(rr), rr); 
			}
			else // Multipole + Expansion approximation
			{
				direct_grav<Composite, Composite>(composite, this, args, d, sqrt(rr), rr);
			}
		}
		else // Multipole + Expansion approximation
		{
			direct_grav<Composite, Composite>(this, composite, args, -d, sqrt(rr), rr);
		}
	}
	else
	{
		for(int n = 0; n < components_size; n++)
		{
			Particle * component = components[n];
			if(component->is_active || composite->is_active) // Recursive
			{
				component->calc_gravity_pairwise(composite, args);
			}
		}
	}
}

} // namespace GravSim
