/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRAVSIM_BODY_H
#define GRAVSIM_BODY_H

#include "analyser.h"

struct WorldLine
{
public:

	WorldPoint point_0;
	WorldPoint point_1;
	int transactions;

#ifdef MULTISTEP
	deque<WorldPoint> point_2_etc; // Known bug with MULTISTEP and sort_hilbert etc
#endif

	WorldLine();
	const WorldPoint & worldpoint(int index) const;
	int length() const;
	Real last_time() const;
	Real second_last_time() const;
	void begin();
	void commit(const WorldPoint & point);
	void abort();
	void take_snapshot(WorldPoint & point, Integrator * integrator);
	void discard_snapshot(WorldPoint & point);
};

struct GravArgs
{
	bool grav;
	int derivative;
	bool gforce;
	bool ctsq;
	Real softening;
	Analyser * analyser;

	GravArgs(bool gg, int d, bool gf, bool ct, const Real & s = 0.0f)
	{
		grav = gg;
		derivative = d;
		gforce = gf;
		ctsq = ct;
		analyser = 0;
		softening = s;
	}
	GravArgs(const GravArgs & args, Analyser * a, const Real & s)
	{
		grav = args.grav;
		derivative = args.derivative;
		gforce = args.gforce;
		ctsq = args.ctsq;
		analyser = a;
		softening = s;
	}
	void add_softening(Real & r, Real & rr) const
	{
		if(softening > 0.0f)
		{
			r += softening;
			rr = sqr(r);
		}
	}
};

struct Neighbour
{
	const Particle * particle;
	Real collision_time_sq;

	Neighbour();
	void reset();
	void assign(const Particle * p, const Real & ctsq);
	void swap(const Particle *& p, Real & ctsq);	
};

struct Particle : public WorldPoint
{
	static Simulator * simulator;

	Real mass;
	Real potential;
	AccEtc field;
	Real radius;
	Integer key;
	unsigned id;
	bool is_active;
	Neighbour neighbours[MAX_NEIGHBOURS];
	Orbit * orbit;

	Particle(unsigned id = 0x7fffffff);
	Particle(unsigned id, const WorldPoint & wp, const Real & m);
	void reset_neighbours();
	const Particle * remove_neighbour(unsigned particle_id);
	void check_if_neighbour(const Particle * particle, Real ctsq);
	void update_neighbour(unsigned particle_id, Real ctsq);
	void log_neighbours() const;
	virtual int depth(int d = 0) const = 0;
	virtual void apply_gravity(const GravArgs & args) = 0;

	// Methods used by Composite

	virtual void add_multipole_to(Composite * composite) const = 0;
	virtual void translate_gravity_from(const Composite * composite) = 0;

	// Methods used by DirectInteractor

	virtual void calc_gravity_direct(Body ** batch, int batch_size, const GravArgs & args) const = 0;

	// Methods used by PairwiseInteractor

	virtual void calc_gravity_pairwise(Particle * particle, const GravArgs & args) = 0;
	virtual void calc_gravity_pairwise(Body * body, const GravArgs & args) = 0;
	virtual void calc_gravity_pairwise(Composite * composite, const GravArgs & args) = 0;
};

struct Body : public Particle, public WorldLine
{
	static unsigned next_id;

	static void (*commit_hook)(const Body * body);
	static void (*error_handler)(const char * error, const char * context);

	Real next_time;

	void draw_line() const;
	void draw_point() const;

	Body();
	Body(const WorldPoint & wp, const Real & m);
	int depth(int d = 0) const { return d; }
	void begin_regularisation(Analyser * analyser);
	void end_regularisation();
	Real target_time(const Real & factor);
	void apply_gravity(const GravArgs & args);
	void reset_gravity();
	void reset_timescale();
	void startup();
	void commit();
	void validate();
	Real kinetic_energy() const { return mass * sqr(vel) / 2.0f; }
	Real potential_energy() const { return mass * potential; }

	// Methods used by Composite

	void add_multipole_to(Composite * composite) const;
	void translate_gravity_from(const Composite * composite);

	// Methods used by direct_ and pairwise_ grav<> and ctsq<> templates

	Real multipole_potential(const Vector & d, const Real & r, const Real & rr) const { return 0.0f; }
	void evaluate_expansion(const AccEtc & grav, const Real & r) { }

	// Methods used by DirectInteractor

	void calc_gravity_direct(Body ** batch, int batch_size, const GravArgs & args) const;

	// Methods used by PairwiseInteractor

	void calc_gravity_pairwise(Particle * particle, const GravArgs & args);
	void calc_gravity_pairwise(Body * body, const GravArgs & args);
	void calc_gravity_pairwise(Composite * composite, const GravArgs & args);
};

template <class PARTICLE1, class PARTICLE2>
Real direct_ctsq(PARTICLE1 * particle1, const PARTICLE2 * particle2, const Vector & d, const Real & rr)
{
	Vector v = particle2->vel - particle1->vel;
	Real cr = particle1->radius + particle2->radius;
	Real ss;
	if(cr > 0.0f)
	{
		Real s = sqrt(rr) - cr;
		s = max(s, 0.0f);
		ss = sqr(s);
	}
	else
	{
		ss = rr;
	}
	Real hyperbolic_cvsq = sqr(v);
	Real elliptical_cvsq = particle1->mass + particle2->mass; // MR: I find that ss gives better results than sss
	Real ctsq = ss / max(max(hyperbolic_cvsq, elliptical_cvsq), 1e-5f);
	particle1->check_if_neighbour(particle2, ctsq);
	return ctsq;
}

template <class PARTICLE1, class PARTICLE2>
void pairwise_ctsq(PARTICLE1 * particle1, PARTICLE2 * particle2, const Vector & d, const Real & rr)
{
	Real ctsq = direct_ctsq<PARTICLE1, PARTICLE2>(particle1, particle2, d, rr);
	particle2->check_if_neighbour(particle1, ctsq);
}

template <class PARTICLE1, class PARTICLE2>
void direct_grav(PARTICLE1 * particle1, const PARTICLE2 * particle2, const GravArgs & args, const Vector & d, const Real & r, const Real & rr)
{
	// Evaluate potential

	Real pot = -particle2->mass / r + particle2->multipole_potential(d, r, rr);
	particle1->potential += pot;
	Real c0 = 1.0f / rr;
	Real m = -pot * c0;

	// Evaluate acc, jerk etc

	AccEtc grav1;
	if(args.gforce)
	{
		Vector a = -particle1->acc; // FIXME: Needs shared timesteps?
		grav1.acc = 2.0f * m * (a - 3.0f * (dot(d, a) * c0) * d);
	}
	else
	{
		grav1.acc = m * d;
#		if (DERIVATIVE >= 3)
		if(args.derivative >= 3)
		{
			Vector v = particle2->vel - particle1->vel;
			Real c1 = dot(d, v) * c0;
			grav1.jerk = m * v - 3.0f * c1 * grav1.acc;
#			if (DERIVATIVE >= 4)
			if(args.derivative >= 4)
			{
				Vector a = particle2->acc - particle1->acc;
				Real c1sq = sqr(c1);
				Real c2 = (sqr(v) + dot(d, a)) * c0 + c1sq;
				grav1.snap = m * a - 6.0f * c1 * grav1.jerk - 3.0f * c2 * grav1.acc;
#				if (DERIVATIVE >= 5)
				if(args.derivative >= 5)
				{
					Vector j = particle2->jerk - particle1->jerk;
					Real c3 = (3.0f * dot(v, a) + dot(d, j)) * c0 + c1 * (3.0f * c2 - 4.0f * c1sq);
					grav1.crackle = m * j - 9.0f * c1 * grav1.snap - 9.0f * c2 * grav1.jerk - 3.0f * c3 * grav1.acc;
				}
#				endif
			}
#			endif
		}
#		endif
	}
	particle1->field.add(grav1);

	// Evaluate expansion coefficients
	particle1->evaluate_expansion(grav1, r);

	// Evaluate collision timescale
	if(args.ctsq)
	{
		direct_ctsq<PARTICLE1, PARTICLE2>(particle1, particle2, d, rr);
	}
}

template <class PARTICLE1, class PARTICLE2>
void pairwise_grav(PARTICLE1 * particle1, PARTICLE2 * particle2, const GravArgs & args, const Vector & d, const Real & r, const Real & rr)
{
	// Evaluate potential

	Real pot1 = -particle2->mass / r + particle2->multipole_potential(d, r, rr);
	Real pot2 = -particle1->mass / r + particle1->multipole_potential(d, r, rr);
	particle1->potential += pot1;
	particle2->potential += pot2;

	// Evaluate acc, jerk etc

	Real c0 = 1.0f / rr;
	Real m1 = -pot1 * c0;
	Real m2 = -pot2 * c0;
	AccEtc grav1;
	AccEtc grav2;
	if(args.gforce)
	{
		Vector a1 = -particle1->acc; 
		Vector a2 = -particle2->acc; 
		grav1.acc = 2.0f * m1 * (a1 - 3.0f * (dot(d, a1) * c0) * d);
		grav2.acc = 2.0f * m2 * (a2 - 3.0f * (dot(d, a2) * c0) * d);
	}
	else
	{
		grav1.acc =  m1 * d;
		grav2.acc = -m2 * d;
#		if (DERIVATIVE >= 3)
		if(args.derivative >= 3)
		{
			Vector v = particle2->vel - particle1->vel;
			Real c1 = dot(d, v) * c0;
			grav1.jerk =  m1 * v - 3.0f * c1 * grav1.acc;
			grav2.jerk = -m2 * v - 3.0f * c1 * grav2.acc;
#			if (DERIVATIVE >= 4)
			if(args.derivative >= 4)
			{
				Vector a = particle2->acc - particle1->acc;
				Real c1sq = sqr(c1);
				Real c2 = (sqr(v) + dot(d, a)) * c0 + c1sq;
				grav1.snap =  m1 * a - 6.0f * c1 * grav1.jerk - 3.0f * c2 * grav1.acc;
				grav2.snap = -m2 * a - 6.0f * c1 * grav2.jerk - 3.0f * c2 * grav2.acc;
#				if (DERIVATIVE >= 5)
				if(args.derivative >= 5)
				{
					Vector j = particle2->jerk - particle1->jerk;
					Real c3 = (3.0f * dot(v, a) + dot(d, j)) * c0 + c1 * (3.0f * c2 - 4.0f * c1sq);
					grav1.crackle =  m1 * j - 9.0f * c1 * grav1.snap - 9.0f * c2 * grav1.jerk - 3.0f * c3 * grav1.acc;
					grav2.crackle = -m2 * j - 9.0f * c1 * grav2.snap - 9.0f * c2 * grav2.jerk - 3.0f * c3 * grav2.acc;
				}
#				endif
			}
#			endif
		}
#		endif
	}
	particle1->field.add(grav1);
	particle2->field.add(grav2);

	// Evaluate expansion coefficients

	particle1->evaluate_expansion(grav1, r);
	particle2->evaluate_expansion(grav2, r);

	// Evaluate collision timescale
	if(args.ctsq)
	{
		pairwise_ctsq<PARTICLE1, PARTICLE2>(particle1, particle2, d, rr);
	}
}

#endif
