include ../../../Makefile.inc

DEFINES=$(DEF)$(SIMULATOR)

../../acs.h :	../../analyser.h ../../basic.h ../../body.h ../../composite.h ../../divider.h ../../integrator.h ../../interactor.h ../../model.h ../../simulator.h 
	-touch ../../acs.h

analyser.o : ../../analyser.cpp ../../acs.h	    
basic.o :	../../basic.cpp ../../acs.h	    
body.o :	../../body.cpp ../../acs.h	    
composite.o :	../../composite.cpp ../../acs.h	    
divider.o :	../../divider.cpp ../../acs.h	    
integrator.o :	../../integrator.cpp ../../acs.h	    
interactor.o :	../../interactor.cpp ../../acs.h	    
model.o :	../../model.cpp ../../acs.h	    
simulator.o : ../../simulator.cpp ../../acs.h	    

%.o : ../../%.cpp
	$(CC) $(CCFLAGS) $(DEFINES) $(INCLUDES) $(CCOUT)$@ $< 

acslib.a :	analyser.o basic.o body.o composite.o divider.o integrator.o interactor.o model.o simulator.o
	$(AR) $(ARFLAGS) $(AROUT)$@ analyser.o basic.o body.o composite.o divider.o integrator.o interactor.o model.o simulator.o

clean:
	-rm -f *.o
	-rm *.a
