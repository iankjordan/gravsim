/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef INTERACTOR_H
#define INTERACTOR_H

#include "divider.h"

class Interactor
{
protected:

	Simulator * simulator;
	Divider * divider;
	Body * bodies;
	int bodies_size;
	int step_size;
	Composite * root;

public:

	static int multipole_count;
	static Real multipole_error_sq;

	static Interactor * create(const char * accelerator_arg, const char * interactor_arg);

	Interactor();
	virtual ~Interactor();
	void startup(Simulator * simulator, Divider * divider);
	virtual void calc_gravity(const GravArgs & args) = 0;
	virtual const char * name() const = 0;
};

class DirectInteractor : public Interactor
{
private:

	Body ** active_buffer();

public:

	DirectInteractor();
	void calc_gravity(const GravArgs & args);
	const char * name() const;
};

class PairwiseInteractor : public Interactor
{
private:

	void apply_gravity(const GravArgs & args);

public:

	PairwiseInteractor();
	void calc_gravity(const GravArgs & args);
	const char * name() const;
};

#endif
