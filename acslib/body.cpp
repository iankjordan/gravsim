/*  Grav-Sim : http://www.grav-sim.com
    Copyright (C) 2009 Mark Ridler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "acs.h"

namespace GravSim {

Simulator * Particle::simulator = 0;
unsigned Body::next_id = 0;
void (*Body::commit_hook)(const Body * body) = 0;
void (*Body::error_handler)(const char * error, const char * context) = 0;

WorldLine::WorldLine()
#ifdef MULTISTEP
, point_2_etc()
#endif
{
	point_0.time = Infinity;
	point_1.time = Infinity;
	transactions = 0;
}

const WorldPoint & WorldLine::worldpoint(int index) const
{
	assert(index >= 0);
	assert(index < length());
	if(index == 0) return point_0;
	if(index == 1) return point_1;
#ifdef MULTISTEP
	return point_2_etc[index - 2];
#else
	return point_1;
#endif
}

int WorldLine::length() const
{
	if(point_1.time == Infinity) return 1;
#ifdef MULTISTEP
	return (int) point_2_etc.size() + 2;
#else
	return 2;
#endif
}

Real WorldLine::last_time() const
{
#ifdef MULTISTEP
	int size = point_2_etc.size();
	if(size > 0) return point_2_etc[size - 1].time;
#endif
	if(point_1.time != Infinity) return point_1.time;
	return point_0.time;
}

Real WorldLine::second_last_time() const
{
#ifdef MULTISTEP
	int size = point_2_etc.size();
	if(size > 0) return point_1.time;
#endif
	return point_0.time;
}

void WorldLine::begin()
{
	assert(transactions >= 0);
	transactions++;
}

void WorldLine::commit(const WorldPoint & point)
{
	assert(transactions >= 1);
	transactions--;
	if(transactions == 0)
	{
#ifdef MULTISTEP
		if(point_1.time != Infinity)
		{
			point_2_etc.push_front(point_1);
		}
#endif
		point_1 = point_0;
		point_0 = point;
	}
	assert(transactions >= 0);
}

void WorldLine::abort()
{
	assert(transactions >= 1);
	transactions--;
}

void WorldLine::take_snapshot(WorldPoint & point, Integrator * integrator)
{
	DETAILED_PROFILE_FUNC();

	assert(point_0.time != Infinity);
	WorldPoint * p0 = &point_0;
	WorldPoint * p1 = ((point_1.time == Infinity) ? 0 : &point_1);
#ifdef MULTISTEP
	int index = 0;
#endif
	if(eq(p0->time, point.time))
	{
		point = *p0;
		return;
	}
	else if(gt(point.time, p0->time))
	{
		integrator->predict(point, *p0);
	}
	else
	{
		assert(gte(point.time, last_time()));
		while(p1 != 0)
		{
			assert(p1->time != Infinity);
			if(eq(point.time, p1->time))
			{
				point = *p1;
				return;
			}
			else if(gt(point.time, p1->time))
			{
				integrator->interpolate(point, *p0, *p1);
				return;
			}
			assert(lt(point.time, p1->time));
			p0 = p1;
			p1 = 0;
#ifdef MULTISTEP
			if(index < point_2_etc.size())
			{
				p1 = &point_2_etc[index];
				index--;
			}
#endif
		}
	}
}

void WorldLine::discard_snapshot(WorldPoint & point)
{
	DETAILED_PROFILE_FUNC();

	point = point_0;
}

Neighbour::Neighbour()
{
	reset();
}

void Neighbour::reset()
{
	particle = 0;
	collision_time_sq = Infinity;
}

void Neighbour::assign(const Particle * p, const Real & ctsq)
{
	particle = p;
	collision_time_sq = ctsq;
}

void Neighbour::swap(const Particle *& p, Real & ctsq)
{
	Neighbour swap;
	swap.particle = particle; particle = p; p = swap.particle;
	swap.collision_time_sq = collision_time_sq;	collision_time_sq = ctsq; ctsq = swap.collision_time_sq;
}

Particle::Particle(unsigned i) : WorldPoint(), field()
{
	mass = 0.0f;
	potential = 0.0f;
	radius = 0.0f;
	id = i;
	is_active = true;
	orbit = 0;
	reset_neighbours();
}

Particle::Particle(unsigned i, const WorldPoint & wp, const Real & m) : WorldPoint(wp), field()
{
	mass = m;
	potential = 0.0f;
	radius = 0.0f;
	id = i;
	is_active = true;
	orbit = 0;
	reset_neighbours();
}

void Particle::reset_neighbours()
{
	for(int n = 0; n < MAX_NEIGHBOURS; n++)
	{
		Neighbour * neighbour = &neighbours[n];
		neighbour->reset();
	}
}

const Particle * Particle::remove_neighbour(unsigned particle_id)
{
	const Particle * result = 0;
	for(int n1 = 0; n1 < MAX_NEIGHBOURS; n1++)
	{
		Neighbour * neighbour1 = &neighbours[n1];
		const Particle * particle = neighbour1->particle;
		if((particle != 0) && (particle->id == particle_id))
		{
			result = particle;
			for(int n2 = n1; n2 < MAX_NEIGHBOURS - 1; n2++)
			{
				neighbours[n2].particle = neighbours[n2 + 1].particle;
				neighbours[n2].collision_time_sq = neighbours[n2 + 1].collision_time_sq;
			}
			neighbours[MAX_NEIGHBOURS - 1].reset();
			break;
		}
	}
	return result;
}

void Particle::check_if_neighbour(const Particle * particle, Real ctsq)
{
	for(int n = 0; n < MAX_NEIGHBOURS; n++)
	{
		Neighbour * neighbour = &neighbours[n];
		if(neighbour->particle == 0)
		{
			neighbour->assign(particle, ctsq);
			break;
		}
		else if(ctsq < neighbour->collision_time_sq)
		{
			neighbour->swap(particle, ctsq);
		}
	}
}

void Particle::update_neighbour(unsigned particle_id, Real ctsq)
{
	const Particle * particle = remove_neighbour(particle_id);
	if(particle == 0)
	{
		particle = simulator->find_particle(particle_id);
	}
	check_if_neighbour(particle, ctsq);
}

void Particle::log_neighbours() const
{
	if(detailed_logging) {
	cout << "id = " << id << "\n";
	for(int n = 0; n < MAX_NEIGHBOURS; n++)
	{
		const Neighbour * neighbour = &neighbours[n];
		const Particle * particle = neighbour->particle;
		if(particle == 0)
		{
			cout << "particle = NULL\n";
		}
		else
		{
			cout << "particle = " << particle->id << "\n";
		}
		cout << "ctsq = " << neighbour->collision_time_sq << "\n";
	} }
}

Body::Body() : Particle(next_id++), WorldLine()
{
	next_time = 0.0f;
}

Body::Body(const WorldPoint & wp, const Real & m) : Particle(next_id++, wp, m), WorldLine()
{
	next_time = 0.0f;
}

void Body::begin_regularisation(Analyser * analyser)
{
	assert(MAX_NEIGHBOURS == 2);
//	if(detailed_logging) cout << "Body::begin_regularisation()\n" << flush;

	if(orbit == 0) {
	const Particle * particle = neighbours[0].particle;
	if((particle != 0) && (particle->neighbours[0].particle == this))
	{
		if(particle->orbit == 0)
		{
			orbit = analyser->analytic_orbit(this, particle);
		}
		else if(particle->orbit->includes(this))
		{
			orbit = particle->orbit;
		}
		if(orbit != 0)
		{
			orbit->expiry_time_sq = min(orbit->expiry_time_sq, neighbours[1].collision_time_sq);
		}
	} }
}

void Body::end_regularisation()
{
	assert(MAX_NEIGHBOURS == 2);
//	if(detailed_logging) cout << "Body::end_regularisation()\n" << flush;

	if(orbit != 0)
	{
		orbit->free();
		orbit = 0;
	}
}

Real Body::target_time(const Real & factor)
{
	if(orbit != 0)
	{
		return sqrt(orbit->expiry_time_sq) * factor;
	}
	else
	{
		return sqrt(neighbours[0].collision_time_sq) * factor;
	}
}

void Body::apply_gravity(const GravArgs & args)
{
	assert(is_active);
	apply(field);
	if(orbit != 0) orbit->update(this);
}

void Body::reset_gravity()
{
	assert(is_active);

	potential = 0.0f;
	field.zero();
}

void Body::startup()
{
	WorldLine::begin();
	assert(is_active);
	WorldLine::commit(*this);
}

void Body::commit()
{
	WorldLine::commit(*this);
	if((transactions == 0) && (commit_hook != 0))
	{
		commit_hook(this);
	}
}

void Body::validate()
{
	if(id >= next_id) next_id = id + 1;
//	reset_neighbours();
}

void Body::add_multipole_to(Composite * composite) const // Passing up the tree
{
}

void Body::translate_gravity_from(const Composite * composite) // Passing down the tree
{
	Vector d = pos - composite->pos;
	const Vector & c1 = composite->expansion.coeff1;
	Real m1 = max(mag(c1), Epsilon), d1 = dot(c1, d) / m1, d1sqdiv2 = sqr(d1) / 2.0f;

	potential += composite->potential - dot(composite->field.acc, d) - m1 * d1sqdiv2;
	field.acc += composite->field.acc + c1 * d1;

#if (EXPANSION >= 2)
	Vector & c2 = composite->expansion.coeff2;
	Real m2 = max(mag(c2), Epsilon), d2 = dot(c2, d) / m2, d2sqdiv2 = sqr(d2) / 2.0f, d2cbdiv6 = cub(d2) / 6.0f;

	potential -= m2 * d2cbdiv6;
	field.acc += c2 * d2sqdiv2;

#if (EXPANSION >= 3)
	Vector & c3 = composite->expansion.coeff3;
	Real m3 = max(mag(c3), Epsilon), d3 = dot(c3, d) / m3, d3cbdiv6 = cub(d3) / 6.0f, d3, d3frdiv24 = frth(d3) / 24.0f;

	potential -= m3 * d3frdiv24;
	field.acc += c3 * d3cbdiv6;

#endif
#endif

	// FIXME: What about field.jerk etc?
}

void Body::calc_gravity_direct(Body ** batch, int batch_size, const GravArgs & args) const
{
	PROFILE_FUNC(); // Time-consuming function (e.g. 6%)

	for(int b = 0; b < batch_size; b++)
	{
		Body * body = batch[b];
		assert(body != 0);
	    if(body->id != id)
		{
			assert(body->is_active);

			Vector d = pos - body->pos;
			Real rr = sqr(d);
			Real r = sqrt(rr);
			Integrator::check_closest_approach(r);
			args.add_softening(r, rr);
			direct_grav<Body, Body>(body, this, args, d, r, rr);
		}
	}
}

void Body::calc_gravity_pairwise(Particle * particle, const GravArgs & args)
{
	assert(is_active || particle->is_active);

	if(particle != 0) // Non-self-interaction
	{
		particle->calc_gravity_pairwise(this, args);
	}
}

void Body::calc_gravity_pairwise(Body * body, const GravArgs & args)
{
	assert(body != 0);
	assert(body->id != id);
	assert(is_active || body->is_active);

	Vector d = pos - body->pos;
	Real rr = sqr(d);
	Real r = sqrt(rr);
	Integrator::check_closest_approach(r);
	args.add_softening(r, rr);
	if(body->is_active)
	{
		if(is_active)
		{
			pairwise_grav<Body, Body>(body, this, args, d, r, rr);
		}
		else
		{
			direct_grav<Body, Body>(body, this, args, d, r, rr);
		}
	}
	else
	{
		direct_grav<Body, Body>(this, body, args, -d, r, rr);
	}
}

void Body::calc_gravity_pairwise(Composite * composite, const GravArgs & args)
{
	assert(composite != 0);
	assert(is_active || composite->is_active);

	composite->calc_gravity_pairwise(this, args);
}

} // namespace GravSim
